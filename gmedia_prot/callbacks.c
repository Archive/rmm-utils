/*
 * Copyright 2002 Sun Microsystems Inc.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Street #330, Boston, MA 02111-1307, USA.
 *
 */

#ifdef HAVE_CONFIG_H
#  include <config.h>
#endif

#include <gnome.h>

#include "callbacks.h"
#include "interface.h"
#include "support.h"
#include "protection.h"
#include "rmedia_utils.h"

extern GtkWidget *passwd_dlg;
extern GtkWidget *passwd_field;
extern GtkWidget *verify_field;
extern GtkWidget *verify_label;
extern GtkWidget *enter_passwd_label;
extern GtkWidget *wp_passwd_button;
extern GtkWidget *rw_passwd_button;
extern GtkWidget *current_protection_label;
extern GtkWidget *wp_nopasswd_button;

void
on_disable_button_toggled              (GtkToggleButton *togglebutton,
                                        gpointer         user_data)
{
  new_protection = RM_WRITE_PROTECT_DISABLE;
}


void
on_wp_nopasswd_button_toggled          (GtkToggleButton *togglebutton,
                                        gpointer         user_data)
{
  new_protection = RM_WRITE_PROTECT_NOPASSWD;
}


void
on_wp_passwd_button_toggled            (GtkToggleButton *togglebutton,
                                        gpointer         user_data)
{
  new_protection = RM_WRITE_PROTECT_PASSWD;
}


void
on_rw_passwd_button_toggled            (GtkToggleButton *togglebutton,
                                        gpointer         user_data)
{
  new_protection = RM_READ_WRITE_PROTECT;
}


void
on_prot_ok_clicked                     (GtkButton       *button,
                                        gpointer         user_data)
{
    int status;
    
    status = process_new_protection();

    /* a passwd is involved somewhere, we need to prompt the user */
    if (status == ERR_NEED_PASSWD_ONLY)
    {
        gtk_widget_set_sensitive(GTK_WIDGET(verify_field), FALSE);
    }
    else
    {
        gtk_widget_set_sensitive(GTK_WIDGET(verify_field), TRUE);
    }
    
    gtk_widget_show(passwd_dlg);
}


void
on_prot_cancel_clicked                 (GtkButton       *button,
                                        gpointer         user_data)
{
    exit(0);
}


void
on_prot_help_clicked                   (GtkButton       *button,
                                        gpointer         user_data)
{
	GError *error = NULL;
	gnome_help_display_desktop (NULL,
				    "user-guide",
				    "wgosnautilus.xml", "gosnautilus-453",
				    &error);

	if (error) {
		GtkWidget *dialog;
		dialog = gtk_message_dialog_new (GTK_WINDOW (user_data),
						 GTK_DIALOG_MODAL|GTK_DIALOG_DESTROY_WITH_PARENT,
						 GTK_MESSAGE_ERROR,
						 GTK_BUTTONS_CLOSE,
						 _("There was an error displaying help: %s"),
						 error->message);

		g_signal_connect (G_OBJECT (dialog),
				  "response", G_CALLBACK (gtk_widget_destroy),
				  NULL);
		gtk_window_set_resizable (GTK_WINDOW (dialog), FALSE);
		gtk_widget_show (dialog);
		g_error_free (error);
	}
	return;
}

void
on_passwd_dlg_show                     (GtkWidget       *widget,
                                        gpointer         user_data)
{
    const char *label_string;
 
    /* This determines whether we display the verification textfield
     * when displaying the dialog, Note the Verification field is only
     * displayed when we are entering a new passwd, and need to verify
     * the entry. Otherwise we are entering an existing passwd. */

    if((new_protection == RM_WRITE_PROTECT_PASSWD) ||
       (new_protection == RM_READ_WRITE_PROTECTED))
    {
        gtk_widget_show(verify_field);
        gtk_widget_show(verify_label);
	label_string = _("Please enter password to enable protection:");
    }
    else
    {
        gtk_widget_hide(verify_field);
        gtk_widget_hide(verify_label);
        label_string = _("Please enter password to disable protection:");
    }

    gtk_label_set_text(GTK_LABEL(enter_passwd_label), label_string);
	
    gtk_widget_show (passwd_dlg);
}


void
on_passwd_apply_clicked                (GtkButton       *button,
                                        gpointer         user_data)
{
    GtkWidget *error_msgbox;
    const char*	label_string;
    int status;

    password = (char *) gtk_entry_get_text(GTK_ENTRY(passwd_field));
    verify =  (char *) gtk_entry_get_text(GTK_ENTRY(verify_field));
    
    status = set_protection_with_password();
    
    if (status == ERR_PASSWD_FAIL) /* passwd not verified */
    {
	error_msgbox = gtk_message_dialog_new (GTK_WINDOW (user_data),
                                         GTK_DIALOG_MODAL | GTK_DIALOG_DESTROY_WITH_PARENT,
                                         GTK_MESSAGE_ERROR,
                                         GTK_BUTTONS_CLOSE,
                                         _("Password verification failed. Please re-enter and verify the password."));

        g_signal_connect (G_OBJECT (error_msgbox), "response",
                         G_CALLBACK (gtk_widget_destroy), NULL);

        gtk_window_set_resizable (GTK_WINDOW (error_msgbox), TRUE);
        gtk_widget_show(error_msgbox);

    }
    else    /* error setting the protection */
    {
        error_msgbox = gtk_message_dialog_new (GTK_WINDOW (user_data),
                                         GTK_DIALOG_MODAL | GTK_DIALOG_DESTROY_WITH_PARENT,
                                         GTK_MESSAGE_ERROR,
                                         GTK_BUTTONS_CLOSE,
                                         _("Password verification failed. Some problem check out errno."));

        g_signal_connect (G_OBJECT (error_msgbox), "response",
                         G_CALLBACK (gtk_widget_destroy), NULL);
        gtk_window_set_resizable (GTK_WINDOW (error_msgbox), TRUE);
        gtk_widget_show(error_msgbox);

    }
}

static void
on_passwd_cancel_cleanup               (GtkWidget      *protection_dlg)
{

    gtk_widget_hide(passwd_dlg);
    gtk_entry_set_text(GTK_ENTRY(passwd_field), "");
    gtk_entry_set_text(GTK_ENTRY(verify_field), "");

    /* Make the main window sensitive if it is insensitive */
    if (!GTK_WIDGET_IS_SENSITIVE (protection_dlg) && new_protection == RM_WRITE_PROTECT_DISABLE)
    {
	gchar* label_string;
	gtk_widget_set_sensitive(GTK_WIDGET(protection_dlg), TRUE);
	gtk_widget_set_sensitive(GTK_WIDGET(wp_passwd_button), FALSE);
	gtk_widget_set_sensitive(GTK_WIDGET(rw_passwd_button), FALSE);
	gtk_widget_set_sensitive(GTK_WIDGET(wp_nopasswd_button), FALSE);

	if (actual_protection == RM_WRITE_PROTECTED_WP)
	    label_string = g_strdup(_("Protection for this medium is Write Protect with Password."));
	else if (actual_protection == RM_READ_WRITE_PROTECTED)
	    label_string = g_strdup(_("Protection for this medium is Read-Write Protect with Password."));
	else
	    label_string = g_strdup(_("Protection for this medium unknown."));

	gtk_label_set_text(GTK_LABEL(current_protection_label), label_string);
	g_free (label_string);
    }
}

void
on_passwd_cancel_clicked               (GtkButton       *button,
                                        gpointer         user_data)
{
    GtkWidget *protection_dlg = GTK_WIDGET (user_data);

    on_passwd_cancel_cleanup (protection_dlg);
}

gboolean
on_passwd_dialog_esc                   (GtkWidget        *passd_dialog,
                                        GdkEventAny      *event,
                                        gpointer          user_data)
{

   GtkWidget *protection_dlg = GTK_WIDGET (user_data);

   on_passwd_cancel_cleanup (protection_dlg);

   return TRUE;
}

gboolean
on_prot_dialog_esc                     (GtkWidget      *prot_dlg,
                                        GdkEventAny    *event,
                                        gpointer        user_data)
{
  exit (0);
  
  /* To avoid a compilation warning */
  return TRUE;
}
