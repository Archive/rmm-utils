/*
 * Copyright 2002 Sun Microsystems Inc.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Street #330, Boston, MA 02111-1307, USA.
 *
 */

#include <string.h>
#include <stdlib.h>
#include "protection.h"
#include "rmedia_utils.h"

int32_t actual_protection, new_protection;
char *media_pathname;
int32_t version;

int 
parse_args(
    int argc,
    char **argv
)
{
    int count, y, fp1;
    char *device = NULL;
    int medium_handle;
    MediumInfoHandle medium_info;
    int status, sys_status;
    int32_t device_type;

    count = 1;
    actual_protection = RM_STATUS_UNKNOWN;
    new_protection = RM_STATUS_UNKNOWN;

    if (argc < 2 || argc > 3)
        return(ERR_USAGE);

    while(count < argc) {

        /* need to change -d -D as -d is reserved for display */
        if(strcmp(argv[count], "-d") == 0)
        {
            /* set new_protection to disable passwd */
            if (new_protection == RM_STATUS_UNKNOWN)
            {
                new_protection = RM_WRITE_PROTECT_DISABLE;
                argv[count][1] = 'D';
            }
            else
                return(ERR_USAGE);
        }
        else if(strcmp(argv[count], "-w") == 0)
        {
            /* set new_protection to write protect w/o passwd */
            if (new_protection == RM_STATUS_UNKNOWN)
                new_protection = RM_WRITE_PROTECT_NOPASSWD;
            else
                return(ERR_USAGE);
        }
        else
        {
            if(device != NULL) /* cannot have more than 1 device path */
                return(ERR_USAGE);
            else
            {
                /* set device path */
                device = (char *)malloc(strlen(argv[count]) + 1);
                strcpy(device, argv[count]);
                media_pathname = rmedia_findname(device);
            }
        }
        count++;
    } 

    if (media_pathname == NULL)
        return(ERR_USAGE);

    /* commenting out the below lines since rmedia_device_open_medium_handle
     * returns (void *)fd, where fd is the file descriptor of media_pathname
     * even if the file descriptor in 0, we are getting NULL. This has to be
     * fixed in sdtrmedia libraries, once this is fixed we can use this call
     */

#if 0
    medium_handle = rmedia_device_open_medium_handle(media_pathname);

    if (medium_handle == NULL)
        return(ERR_OPENDEV);
#endif 

   medium_handle = open (media_pathname, O_RDONLY);

   if (medium_handle < 0)
	return (ERR_OPENDEV);

    actual_protection = rmedia_device_get_protection_status((void *)medium_handle); 
    medium_info = rmedia_device_get_medium_property((void *)medium_handle);
    rmedia_device_close_medium_handle((void *)medium_handle);

    if (medium_info == NULL)
        return (ERR_BADDEV);

    version = rmedia_medium_get_version(medium_info);
    device_type = rmedia_medium_get_media_type(medium_info);

    /* determine if the device supports s/w protection */
    switch(device_type)
    {
        case DK_ZIP :	
        case DK_JAZ :
        case DK_CDRW :
            break;
        case DK_MO_ERASABLE :
        case DK_MO_WRITEONCE :
        case DK_AS_MO :
        case DK_CDROM :
        case DK_CDR :
        case DK_DVDROM :
        case DK_DVDR :
        case DK_DVDRAM :
        case DK_FIXED_DISK :
        default :
            return(ERR_BADDEV);
    }   

    /* Was the new protection specified on the command line?
     * If it was and a password isn't required apply new protection
     * Otherwise just return
     */

    /* disable protection from cmd line */
    if (new_protection == RM_WRITE_PROTECT_DISABLE)
    {
        if (actual_protection == RM_WRITE_PROTECTED)
        {
            sys_status = rmedia_device_set_protection_status(media_pathname,
                    RM_WRITE_PROTECT_DISABLE, NULL, 0, version);
            exit(sys_status);
        }
        else if (actual_protection == RM_UNPROTECTED)
        {
            exit(0); /* protection already disabled */
        }
    }

    /* write protect without password from the command line */
    if (new_protection == RM_WRITE_PROTECT_NOPASSWD)
    {
        if(actual_protection == RM_WRITE_PROTECTED)
        {
            exit(0); /* Already write protect without passwd */
        }
        else if (actual_protection == RM_UNPROTECTED)
        {
            sys_status = rmedia_device_set_protection_status(media_pathname,
                    RM_WRITE_PROTECT_NOPASSWD, NULL, 0, version);
            exit(sys_status);
        }
    }

    return(ERR_OK);
}

int
process_new_protection()
{
    int status, sys_status;

    /* if actual protection has no passwd, and the new protection has no passwd
       we simply just apply it, followed by an unmount and mount*/
    if (((new_protection == RM_WRITE_PROTECT_DISABLE) || 
	 (new_protection == RM_WRITE_PROTECT_NOPASSWD)) && 
        ((actual_protection == RM_UNPROTECTED) || 
         (actual_protection == RM_WRITE_PROTECTED)))
    {
        sys_status = rmedia_device_set_protection_status(media_pathname,
                new_protection, NULL, 0, version);
        exit(sys_status);
    }
    else if ((new_protection == RM_WRITE_PROTECT_PASSWD) ||
	     (new_protection == RM_READ_WRITE_PROTECT))
    {
        /* if the new protection uses a passwd, then the current
         * protection cannot posess a passwd so the user is prompted
         * for the new passwd and verification */
        return (ERR_NEED_PASSWD_AND_VERIFY);
    }
    else
    {
        return (ERR_NEED_PASSWD_ONLY);
    }
}

int
set_protection_with_password()
{
    int status, sys_status;

    /* if we're setting a new passwd, verify the passwd */
    if((new_protection == RM_WRITE_PROTECT_PASSWD) ||
       (new_protection == RM_READ_WRITE_PROTECT))
    {
    	if((strcmp(password, verify) != 0) && (strlen(password) > 0) )
    	    return (ERR_PASSWD_FAIL);
    }

    sys_status = rmedia_device_set_protection_status(media_pathname,
            new_protection, password, strlen(password), version);
    if (sys_status != 0)
        return (sys_status);
    else
        exit (0);
}
