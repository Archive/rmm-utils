/*
 * Copyright 2002 Sun Microsystems Inc.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Street #330, Boston, MA 02111-1307, USA.
 *
 */

#include <gnome.h>


void
on_disable_button_toggled              (GtkToggleButton *togglebutton,
                                        gpointer         user_data);

void
on_wp_nopasswd_button_toggled          (GtkToggleButton *togglebutton,
                                        gpointer         user_data);

void
on_wp_passwd_button_toggled            (GtkToggleButton *togglebutton,
                                        gpointer         user_data);

void
on_rw_passwd_button_toggled            (GtkToggleButton *togglebutton,
                                        gpointer         user_data);

void
on_prot_ok_clicked                     (GtkButton       *button,
                                        gpointer         user_data);

void
on_prot_cancel_clicked                 (GtkButton       *button,
                                        gpointer         user_data);

void
on_prot_help_clicked                   (GtkButton       *button,
                                        gpointer         user_data);

void
on_passwd_dlg_show                     (GtkWidget       *widget,
                                        gpointer         user_data);

void
on_passwd_activated                    (GtkEditable     *editable,
                                        gpointer         user_data);

void
on_verify_activated                    (GtkEditable     *editable,
                                        gpointer         user_data);

void
on_passwd_verify_changed               (GtkEditable     *editable,
                                        gpointer         user_data);

void
on_passwd_apply_clicked                (GtkButton       *button,
                                        gpointer         user_data);


void
on_passwd_cancel_clicked               (GtkButton       *button,
                                        gpointer         user_data);

gboolean
on_passwd_dialog_esc                   (GtkWidget        *passd_dialog,
                                        GdkEventAny      *event,
                                        gpointer          user_data);

gboolean
on_prot_dialog_esc                     (GtkWidget        *prot_dlg,
                                        GdkEventAny      *event,
                                        gpointer         user_data);
