/*
 * Copyright 2002 Sun Microsystems Inc.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Street #330, Boston, MA 02111-1307, USA.
 *
 */

#include <dlfcn.h>
#include <dirent.h>
#include <sys/types.h>
#include <errno.h>

#define MAX_PASS_LEN 50

/* Error codes returned by sdtmedia_prot_utils.c */
#define ERR_OK					0
#define ERR_OPENDEV				1
#define ERR_BADDEV				2
#define ERR_FORK				6
#define ERR_EJECT_EXEC				7
#define ERR_EJECT_WAIT				8
#define ERR_NEED_PASSWD_ONLY			9
#define ERR_NEED_PASSWD_AND_VERIFY		10
#define ERR_PASSWD_FAIL				11
#define ERR_USAGE				100

/* the password and verify strings are used to store
 * the entries from the password entry dialog */
char *password, *verify;

/* actual_protection is used to store the current
 * protection settings for the removable media */
extern int32_t actual_protection;

/* new_protection is used to is used to store the
 * protection settings the user has selected to
 * apply to the removable media */
extern int32_t new_protection;

/* the full pathname of the media being protected */
extern char *media_pathname;

extern int parse_args(int argc, char **argv);
extern int process_new_protection();
extern int set_protection_with_password();
