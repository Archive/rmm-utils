/*
 * Copyright 2002 Sun Microsystems Inc.
 *
 *  This Library is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This Library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU Public Public License
 *  along with this Library; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Street #330, Boston, MA 02111-1307, USA.
 *
 */

#pragma ident	"@(#)action_gnome.c	0.1	2002/04/04 SMI"

/*
 * To handle removable media for gnome session
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#else
#define PACKAGE_BIN_DIR "/usr/gnome/bin"
#endif

#include	<malloc.h>
#include	<sys/types.h>
#include	<sys/param.h>
#include	<sys/systeminfo.h>
#include	<sys/stat.h>
#include	<rpc/types.h>
#include	<pwd.h>
#include	<errno.h>
#include	<stdio.h>
#include	<stdlib.h>
#include	<fcntl.h>
#include	<string.h>
#include	<unistd.h>
#include	<rmmount.h>
#include 	<syslog.h>

static void	setup_user(void);
static void	set_user_env(int, char **);


int
action (struct action_arg **aa, int argc, char **argv)
{
	char			*atype = getenv ("VOLUME_ACTION");
	char 			command[256];

	/* we don't care about removes, just inserts */
	if (strcmp (atype, "insert") != 0) {
		return FALSE;
	}

	/* can't do anything without a path */
	if (aa[0]->aa_rawpath == NULL) {
		openlog ("ACTION_GNOME", LOG_PID, LOG_USER);
		syslog (LOG_ERR, "no rawpath\n");
		closelog ();
		return FALSE;
	}

#ifdef DEBUG
	openlog ("ACTION_GNOME : DEBUG :",LOG_PID,LOG_USER);
	syslog (LOG_USER, "using path %s", aa[0]->aa_rawpath);
	closelog ();
#endif

	if (fork () == 0) { /*child*/
		int		fd;

#ifdef	DEBUG
	openlog ("ACTION_GNOME : DEBUG", LOG_PID, LOG_USER);
	syslog(LOG_USER, " child is running (pid = %d)\n", getpid ());
	closelog ();
#endif
		/* child */

		/* stick their error messages out on the console */
		if ((fd = open ("/dev/console", O_RDWR)) >= 0) {
			(void) dup2 (fd, fileno (stdin));
			(void) dup2 (fd, fileno (stdout));
			(void) dup2 (fd, fileno (stderr));
		}


		set_user_env (argc, argv);
#ifdef	DEBUG
		openlog ("ACTION_GNOME : DEBUG", LOG_PID, LOG_USER);
		syslog (LOG_USER, " child: after setting up env\n");
		closelog ();
#endif
		if (aa[0]->aa_mountpoint != NULL) {
			setup_user ();
			sprintf(command, "%snautilus --no-desktop %s", PACKAGE_BIN_DIR, aa[0]->aa_mountpoint);
			popen (command, "r");
			openlog ("ACTION_GNOME", LOG_PID, LOG_USER);
                	syslog (LOG_ERR, "nautilus exec failed : %s %s\n", strerror (errno), command);
                	closelog ();
			exit (1);
		}

		if(!strcmp(aa[0]->aa_type, "audio_only")) {
			FILE *fp;
			uid_t uid;
			int status = FALSE;
			char cmd[64];

			sprintf (command, "%sgnome-cd", PACKAGE_BIN_DIR);
			setup_user ();

			/* Get the real user id */
			uid = getuid ();

			sprintf (cmd, "pgrep -u %ld gnome-cd", uid);

			fp = popen (cmd, "r");
			if (fp)
			{
				if (fgets(cmd, sizeof (cmd), fp) != NULL)
					if (cmd[0] != '\n')
						status = TRUE;
				pclose (fp);
			}

			if (status == FALSE)
			{
				(void) execl (command, command, NULL);
 				openlog ("ACTION_GNOME", LOG_PID, LOG_USER);
                        	syslog (LOG_ERR, " exec of %s failed : %s\n", command, strerror (errno));
                        	closelog ();
                        	exit (1);
			}
			exit (0);
		}
		
		if((!strcmp(aa[0]->aa_type,"unformatted_media"))
			|| (!strcmp(aa[0]->aa_type,"unknown"))) {
			sprintf (command, "%sgmedia_format", PACKAGE_BIN_DIR);
			setup_user ();
			(void) execl (command, command, "-d", aa[0]->aa_rawpath, NULL);
			openlog ("ACTION_GNOME", LOG_PID, LOG_USER);
                        syslog (LOG_ERR, "gmedia_format exec failed : %s\n" ,strerror (errno));
                        closelog ();
                        exit (1);
		}
		
		if (!strcmp(aa[0]->aa_type, "password_protected")) {
			sprintf (command, "%sgmedia_prot", PACKAGE_BIN_DIR);
			setup_user ();
			(void) execl (command, command, "-d", aa[0]->aa_rawpath, NULL);
			openlog ("ACTION_GNOME", LOG_PID, LOG_USER);
                        syslog (LOG_ERR, "gmedia_prot exec failed : %s\n", strerror (errno));
                        closelog ();
                        exit (1);
		}
 
	}

#ifdef	DEBUG
	openlog ("ACTION_GNOME : DEBUG", LOG_PID, LOG_USER);
	syslog (LOG_USER, "sleeping a while -- just wait\n");
	closelog ();
	(void) sleep (15);
#endif

	return (FALSE);
}


void
setup_user(void)
{
	struct stat	sb;
	uid_t		uid = 1;	/* daemon */
	gid_t		gid = 1;	/* other */
	struct passwd	*pw;
	char		namebuf[MAXNAMELEN];


	if (stat ("/dev/console", &sb) == 0) {
		if (sb.st_uid != 0) {
			uid = sb.st_uid;
		}
	}
	/* set HOME env. var. */
	if ((pw = getpwuid (uid)) != NULL) {
		gid = pw->pw_gid;
		(void) sprintf (namebuf, "HOME=%s", pw->pw_dir);
		(void) putenv (strdup(namebuf));
	} else {
		(void) putenv ("HOME=/tmp");
	}

	if (setgid (gid) < 0) {
		perror ("Failed to set gid:");
	}

	if (setuid (uid) < 0) {
		perror ("Failed to set uid:");
	}

}


/*
 * set_user_env -- set up user environment
 */
static void set_user_env(int ac, char **av)
{
	int			i;
	bool_t			display_specified = FALSE;
	static char		*display;
	static char		ld_lib_path[MAXNAMELEN+1];



#ifdef	DEBUG
openlog ("ACTION_GNOME : DEBUG", LOG_PID, LOG_USER);
        syslog (LOG_CONS, " set_user_env(): entering\n");
        closelog ();
#endif

	/* only set display if it wasn't passed in */
	for (i = 0; i < ac; i++) {
		if ((strcmp (av[i], "-display") == 0) &&
		    (ac > (i+1))) {
			if (strlen (av[i+1]) <= MAXNAMELEN) {
				display_specified = TRUE;
				display = (char *) malloc (sizeof (av[i+1]) + 1);
				(void) strcpy (display, av[i+1]);
			}
			break;
		}
	}
#ifdef	DEBUG
openlog ("ACTION_GNOME : DEBUG", LOG_PID, LOG_USER);
        syslog(LOG_USER, " set_user_env(): display found = %s\n", display_specified ? "TRUE" : "FALSE");
        closelog ();
#endif
	if (!display_specified) {
		display = (char *)malloc (strlen ("DISPLAY=:0") + 2);
		(void) sprintf (display, "DISPLAY=:0");
	}
		(void) putenv (display);

	(void) sprintf(ld_lib_path, "LD_LIBRARY_PATH=%s:%s:%s",
	    "/usr/lib",
	    "/usr/gnome/lib",
	    "/usr/dt/lib");
	(void) putenv(ld_lib_path);
	

#ifdef	DEBUG
openlog("ACTION_GNOME : DEBUG", LOG_PID, LOG_USER);
        syslog(LOG_USER,"set_user_env(): leaving  %s\n", getenv("DISPLAY"));
        closelog ();
#endif

}

