/*
 * Copyright 2002 Sun Microsystems Inc.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Street #330, Boston, MA 02111-1307, USA.
 *
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif
#include <locale.h>

#include <stdio.h>
#include <string.h>
#include <gnome.h>
#include <gtk/gtk.h>
#include <errno.h>
#include "properties.h"

typedef struct {

	GtkWidget *dlg;
	GtkWidget *vbox;
	GtkWidget *hbox,*hbox2;
	GtkWidget *separator1;
	GtkWidget *separator2;
	GtkWidget *closeButton;
	GtkWidget *helpButton;
	GtkWidget *categoryLabel;
	GtkWidget *aliasLabel;
	GtkWidget *aliasValue;
	GtkWidget *idLabel;
	GtkWidget *idValue;
	GtkWidget *writeLabel;
	GtkWidget *writeValue;	
	GtkWidget *passwordLabel;
	GtkWidget *passwordValue;
	GtkWidget *fsLabel;
	GtkWidget *fsValue;
	GtkWidget *mountLabel;
	GtkWidget *mountValue;
	GtkWidget *capacityLabel;
	GtkWidget *capacityValue;
	GtkWidget *radioInfoButton;
	GtkWidget *radioFloatButton;
	GtkWidget *usedLabel;
	GtkWidget *usedValue;
	GtkWidget *availLabel;
	GtkWidget *availValue;
	GtkWidget *labelTable;
	GtkWidget *alignmentC1;
	GtkWidget *alignmentC2;
} MainApp;

MainApp *mp;
MediumInfoHandle	device_prop;

static void set_relation(GtkWidget *widget, GtkLabel *label);

static void delete_event( GtkWidget *widget,
	gpointer   data )
{
	gtk_main_quit();
}

static void destroy( GtkWidget *widget,
	gpointer   data )
{
	gtk_main_quit();
}

static void close_cb( GtkWidget *widget,
	gpointer   data )
{
	gtk_main_quit();
}

static void help_cb( GtkWidget *widget,
	gpointer   data )
{
	GError *error = NULL;
	gnome_help_display_desktop (NULL,
				    "user-guide",
				    "wgosnautilus.xml", "gosnautilus-455",
				    &error);

	if (error) {
		GtkWidget *dialog;
		dialog = gtk_message_dialog_new (GTK_WINDOW (data),
						 GTK_DIALOG_MODAL | GTK_DIALOG_DESTROY_WITH_PARENT,
						 GTK_MESSAGE_ERROR,
						 GTK_BUTTONS_CLOSE,
						 _("There was an error displaying help: %s"),
						 error->message);

		g_signal_connect (G_OBJECT (dialog),
				  "response", G_CALLBACK (gtk_widget_destroy),
				  NULL);
		gtk_window_set_resizable (GTK_WINDOW (dialog), FALSE);
		gtk_widget_show (dialog);
		g_error_free (error);
	}
	return;
}	


static void showCapacity( GtkWidget *widget,
		gpointer data) 
{

	if(GTK_TOGGLE_BUTTON(widget)->active){
		gtk_widget_hide(mp->aliasLabel);
		gtk_widget_hide(mp->aliasValue);
		gtk_widget_hide(mp->idLabel);
		gtk_widget_hide(mp->idValue);
		gtk_widget_hide(mp->writeLabel);
		gtk_widget_hide(mp->writeValue);
		gtk_widget_hide(mp->passwordLabel);
		gtk_widget_hide(mp->passwordValue);
		gtk_widget_hide(mp->fsLabel);
		gtk_widget_hide(mp->fsValue);
		gtk_widget_hide(mp->mountLabel);
		gtk_widget_hide(mp->mountValue);
		gtk_widget_hide(mp->capacityLabel);
		gtk_widget_hide(mp->capacityValue);
		
		gtk_widget_show(mp->usedLabel);
		gtk_widget_show(mp->usedValue);
		gtk_widget_show(mp->availLabel);
		gtk_widget_show(mp->availValue);

	}
	else {
		gtk_widget_hide(mp->usedLabel);
		gtk_widget_hide(mp->usedValue);
		gtk_widget_hide(mp->availLabel);
		gtk_widget_hide(mp->availValue);

		gtk_widget_show(mp->aliasLabel);
		gtk_widget_show(mp->aliasValue);
		gtk_widget_show(mp->idLabel);
		gtk_widget_show(mp->idValue);
		gtk_widget_show(mp->writeLabel);
		gtk_widget_show(mp->writeValue);
		gtk_widget_show(mp->passwordLabel);
		gtk_widget_show(mp->passwordValue);
		gtk_widget_show(mp->fsLabel);
		gtk_widget_show(mp->fsValue);
		gtk_widget_show(mp->mountLabel);
		gtk_widget_show(mp->mountValue);
		gtk_widget_show(mp->capacityLabel);
		gtk_widget_show(mp->capacityValue);

	}
}

void gui_initialise(gchar *device)
{
	mp = (MainApp *) malloc(sizeof(MainApp));

	mp->dlg =  gtk_dialog_new_with_buttons (_("Gnome Media Properties"),NULL,GTK_DIALOG_MODAL | GTK_DIALOG_DESTROY_WITH_PARENT, NULL);


	g_signal_connect (G_OBJECT (mp->dlg), "delete_event",
				 G_CALLBACK (delete_event),
				 NULL);

	g_signal_connect (G_OBJECT (mp->dlg), "destroy",
				 G_CALLBACK (destroy),
				 NULL);

	gtk_widget_set_size_request(GTK_WIDGET(mp->dlg), 500,275);

	/*
	mp->vbox = gtk_vbox_new(FALSE,5);
	*/
	 /* Create the radio buttons */
	mp->hbox = gtk_hbox_new (FALSE, 5 );
	mp->radioInfoButton = gtk_radio_button_new_with_mnemonic(NULL, _("_Information"));
	mp->radioFloatButton =  gtk_radio_button_new_with_mnemonic(gtk_radio_button_get_group(
		GTK_RADIO_BUTTON(mp->radioInfoButton )) , _("_Floating Capacities"));
	gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(mp->radioInfoButton) , TRUE);

	g_signal_connect (G_OBJECT (mp->radioFloatButton), "toggled",
			 G_CALLBACK (showCapacity),
			 NULL);


	mp->categoryLabel = gtk_label_new(_("Category:"));
	
	set_relation(mp->radioInfoButton, GTK_LABEL(mp->categoryLabel));
	set_relation(mp->radioFloatButton, GTK_LABEL(mp->categoryLabel));
	
	gtk_box_pack_start(GTK_BOX(mp->hbox), mp->categoryLabel, TRUE, TRUE, 0);

	gtk_box_pack_start(GTK_BOX(mp->hbox), mp->radioInfoButton, TRUE, TRUE, 0);
	gtk_box_pack_start(GTK_BOX(mp->hbox), mp->radioFloatButton, TRUE, TRUE, 0);

	gtk_box_pack_start(GTK_BOX(GTK_DIALOG(mp->dlg)->vbox), mp->hbox, TRUE,TRUE,0);

	/* Separator */
	mp->separator1 = gtk_hseparator_new();

	gtk_box_pack_start(GTK_BOX(GTK_DIALOG(mp->dlg)->vbox), mp->separator1, TRUE, TRUE, 0);


	/* Information Labels */
	mp->labelTable = gtk_table_new(7,1,TRUE);
	
	mp->alignmentC1 = gtk_alignment_new(1.0, 0.5, 0.0, 0.0); 
	mp->aliasLabel = gtk_label_new(_("Alias:"));
	gtk_container_add(GTK_CONTAINER(mp->alignmentC1), mp->aliasLabel);
	gtk_table_attach(GTK_TABLE(mp->labelTable), mp->alignmentC1, 0, 1, 0 , 1, GTK_EXPAND|GTK_FILL|GTK_SHRINK, GTK_EXPAND|GTK_FILL|GTK_SHRINK, 30, 0);
	
	mp->alignmentC2 = gtk_alignment_new(0.0, 0.5, 0.0, 0.0); 
	mp->aliasValue = gtk_label_new("");
	gtk_container_add(GTK_CONTAINER(mp->alignmentC2), mp->aliasValue);
	gtk_table_attach(GTK_TABLE(mp->labelTable), mp->alignmentC2, 1, 2, 0 , 1, GTK_EXPAND|GTK_FILL|GTK_SHRINK, GTK_EXPAND|GTK_FILL|GTK_SHRINK, 0, 0);
	
	set_relation(mp->aliasValue, GTK_LABEL(mp->aliasLabel));
	
	mp->alignmentC1 = gtk_alignment_new(1.0, 0.5, 0.0, 0.0); 
	mp->idLabel = gtk_label_new(_("Product Id:"));
	gtk_container_add(GTK_CONTAINER(mp->alignmentC1), mp->idLabel);
	gtk_table_attach(GTK_TABLE(mp->labelTable), mp->alignmentC1, 0, 1, 1, 2, GTK_EXPAND|GTK_FILL|GTK_SHRINK, GTK_EXPAND|GTK_FILL|GTK_SHRINK, 30, 0);
	
	mp->alignmentC2 = gtk_alignment_new(0.0, 0.5, 0.0, 0.0); 
	mp->idValue = gtk_label_new("");
	gtk_container_add(GTK_CONTAINER(mp->alignmentC2), mp->idValue);
	gtk_table_attach(GTK_TABLE(mp->labelTable), mp->alignmentC2, 1, 2, 1, 2, GTK_EXPAND|GTK_FILL|GTK_SHRINK, GTK_EXPAND|GTK_FILL|GTK_SHRINK, 0, 0);
	
	set_relation(mp->idValue, GTK_LABEL(mp->idLabel));
	
	mp->alignmentC1 = gtk_alignment_new(1.0, 0.5, 0.0, 0.0); 
	mp->writeLabel = gtk_label_new(_("Write Protect:"));
	gtk_container_add(GTK_CONTAINER(mp->alignmentC1), mp->writeLabel);
	gtk_table_attach(GTK_TABLE(mp->labelTable), mp->alignmentC1, 0, 1, 2, 3, GTK_EXPAND|GTK_FILL|GTK_SHRINK, GTK_EXPAND|GTK_FILL|GTK_SHRINK, 30, 0);
	
	mp->alignmentC2 = gtk_alignment_new(0.0, 0.5, 0.0, 0.0); 
	mp->writeValue = gtk_label_new("");
	gtk_container_add(GTK_CONTAINER(mp->alignmentC2), mp->writeValue);
	gtk_table_attach(GTK_TABLE(mp->labelTable), mp->alignmentC2, 1, 2, 2, 3, GTK_EXPAND|GTK_FILL|GTK_SHRINK, GTK_EXPAND|GTK_FILL|GTK_SHRINK, 0, 0);
	
	set_relation(mp->writeValue, GTK_LABEL(mp->writeLabel));
	
	mp->alignmentC1 = gtk_alignment_new(1.0, 0.5, 0.0, 0.0); 
	mp->passwordLabel = gtk_label_new(_("Password Protect:"));
	gtk_container_add(GTK_CONTAINER(mp->alignmentC1), mp->passwordLabel);
	gtk_table_attach(GTK_TABLE(mp->labelTable), mp->alignmentC1, 0, 1, 3, 4, GTK_EXPAND|GTK_FILL|GTK_SHRINK, GTK_EXPAND|GTK_FILL|GTK_SHRINK, 30, 0);
	
	mp->alignmentC2 = gtk_alignment_new(0.0, 0.5, 0.0, 0.0); 
	mp->passwordValue = gtk_label_new("");
	gtk_container_add(GTK_CONTAINER(mp->alignmentC2), mp->passwordValue);
	gtk_table_attach(GTK_TABLE(mp->labelTable), mp->alignmentC2, 1, 2, 3 , 4, GTK_EXPAND|GTK_FILL|GTK_SHRINK, GTK_EXPAND|GTK_FILL|GTK_SHRINK, 0, 0);
	
	set_relation(mp->passwordValue, GTK_LABEL(mp->passwordLabel));
	
	mp->alignmentC1 = gtk_alignment_new(1.0, 0.5, 0.0, 0.0); 
	mp->fsLabel = gtk_label_new(_("File System:"));
	gtk_container_add(GTK_CONTAINER(mp->alignmentC1), mp->fsLabel);
	gtk_table_attach(GTK_TABLE(mp->labelTable), mp->alignmentC1, 0, 1, 4 , 5, GTK_EXPAND|GTK_FILL|GTK_SHRINK, GTK_EXPAND|GTK_FILL|GTK_SHRINK, 30, 0);
	
	mp->alignmentC2 = gtk_alignment_new(0.0, 0.5, 0.0, 0.0); 
	mp->fsValue = gtk_label_new("");
	gtk_container_add(GTK_CONTAINER(mp->alignmentC2), mp->fsValue);
	gtk_table_attach(GTK_TABLE(mp->labelTable), mp->alignmentC2, 1, 2, 4 , 5, GTK_EXPAND|GTK_FILL|GTK_SHRINK, GTK_EXPAND|GTK_FILL|GTK_SHRINK, 0, 0);
	
	set_relation(mp->fsValue, GTK_LABEL(mp->fsLabel));
	
	mp->alignmentC1 = gtk_alignment_new(1.0, 0.5, 0.0, 0.0); 
	mp->mountLabel = gtk_label_new(_("Mount Point:"));
	gtk_container_add(GTK_CONTAINER(mp->alignmentC1), mp->mountLabel);
	gtk_table_attach(GTK_TABLE(mp->labelTable), mp->alignmentC1, 0, 1, 5 , 6, GTK_EXPAND|GTK_FILL|GTK_SHRINK, GTK_EXPAND|GTK_FILL|GTK_SHRINK, 30, 0);
	
	mp->alignmentC2 = gtk_alignment_new(0.0, 0.5, 0.0, 0.0); 
	mp->mountValue = gtk_label_new("");
	gtk_container_add(GTK_CONTAINER(mp->alignmentC2), mp->mountValue);
	gtk_table_attach(GTK_TABLE(mp->labelTable), mp->alignmentC2, 1, 2, 5 , 6, GTK_EXPAND|GTK_FILL|GTK_SHRINK, GTK_EXPAND|GTK_FILL|GTK_SHRINK, 0, 0);
	
	set_relation(mp->mountValue, GTK_LABEL(mp->mountLabel));
	
	mp->alignmentC1 = gtk_alignment_new(1.0, 0.5, 0.0, 0.0); 
	mp->capacityLabel = gtk_label_new(_("Total Capacity:"));
	gtk_container_add(GTK_CONTAINER(mp->alignmentC1), mp->capacityLabel);
	gtk_table_attach(GTK_TABLE(mp->labelTable), mp->alignmentC1, 0, 1, 6 , 7, GTK_EXPAND|GTK_FILL|GTK_SHRINK, GTK_EXPAND|GTK_FILL|GTK_SHRINK, 30, 0);
	
	mp->alignmentC2 = gtk_alignment_new(0.0, 0.5, 0.0, 0.0); 
	mp->capacityValue = gtk_label_new("");
	gtk_container_add(GTK_CONTAINER(mp->alignmentC2), mp->capacityValue);
	gtk_table_attach(GTK_TABLE(mp->labelTable), mp->alignmentC2, 1, 2,  6, 7, GTK_EXPAND|GTK_FILL|GTK_SHRINK, GTK_EXPAND|GTK_FILL|GTK_SHRINK, 0, 0);
	
	set_relation(mp->capacityValue, GTK_LABEL(mp->capacityLabel));
	
	gtk_box_pack_start( GTK_BOX(GTK_DIALOG(mp->dlg)->vbox),mp->labelTable, TRUE,TRUE,0);
	

	/* Close and Help buttons */
	mp->helpButton = gtk_dialog_add_button(GTK_DIALOG (mp->dlg), GTK_STOCK_HELP, GTK_RESPONSE_HELP);
	gtk_widget_ref (mp->helpButton);
	g_object_set_data_full (G_OBJECT(mp->dlg),"mp->helpButton",
	mp->helpButton, (GtkDestroyNotify) gtk_widget_unref);
	g_signal_connect(GTK_BUTTON(mp->helpButton),"clicked",GTK_SIGNAL_FUNC(help_cb),mp->dlg);

	mp->closeButton = gtk_dialog_add_button(GTK_DIALOG (mp->dlg), GTK_STOCK_CLOSE, GTK_RESPONSE_CLOSE);
	gtk_widget_ref (mp->closeButton);
	g_object_set_data_full (G_OBJECT(mp->dlg),"mp->closeButton",
	mp->closeButton, (GtkDestroyNotify) gtk_widget_unref);
	g_signal_connect(GTK_BUTTON(mp->closeButton),"clicked",GTK_SIGNAL_FUNC(close_cb),NULL);


	
	/*  Show the dialog box */ 
	gtk_widget_show_all(mp->dlg);


	/* Add two label to table that will remain hidden until radio button toggled */
	mp->alignmentC1 = gtk_alignment_new(1.0, 0.5, 0.0, 0.0); 
	mp->usedLabel = gtk_label_new(_("Used Capacity:"));
	gtk_container_add(GTK_CONTAINER(mp->alignmentC1), mp->usedLabel);
	gtk_table_attach(GTK_TABLE(mp->labelTable), mp->alignmentC1, 0, 1, 0,1, GTK_EXPAND|GTK_FILL|GTK_SHRINK, GTK_EXPAND|GTK_FILL|GTK_SHRINK, 30, 0);
	gtk_widget_show(mp->alignmentC1);
	
	mp->alignmentC2 = gtk_alignment_new(0.0, 0.5, 0.0, 0.0); 
	mp->usedValue = gtk_label_new("");
	gtk_container_add(GTK_CONTAINER(mp->alignmentC2), mp->usedValue);
	gtk_table_attach(GTK_TABLE(mp->labelTable), mp->alignmentC2, 1, 2,  0, 1, GTK_EXPAND|GTK_FILL|GTK_SHRINK, GTK_EXPAND|GTK_FILL|GTK_SHRINK, 0, 0);
	gtk_widget_show(mp->alignmentC2);
	
	set_relation(mp->usedValue, GTK_LABEL(mp->usedLabel));
	
	mp->alignmentC1 = gtk_alignment_new(1.0, 0.5, 0.0, 0.0); 
	mp->availLabel = gtk_label_new(_("Available Capacity:"));
	gtk_container_add(GTK_CONTAINER(mp->alignmentC1), mp->availLabel);
	gtk_table_attach(GTK_TABLE(mp->labelTable), mp->alignmentC1, 0, 1,  1, 2, GTK_EXPAND|GTK_FILL|GTK_SHRINK, GTK_EXPAND|GTK_FILL|GTK_SHRINK, 30, 0);
	gtk_widget_show(mp->alignmentC1);
	
	mp->alignmentC2 = gtk_alignment_new(0.0, 0.5, 0.0, 0.0); 
	mp->availValue = gtk_label_new("");
	gtk_container_add(GTK_CONTAINER(mp->alignmentC2), mp->availValue);
	gtk_table_attach(GTK_TABLE(mp->labelTable), mp->alignmentC2, 1, 2,  1, 2, GTK_EXPAND|GTK_FILL|GTK_SHRINK, GTK_EXPAND|GTK_FILL|GTK_SHRINK, 0, 0);
	gtk_widget_show(mp->alignmentC2);
	
	set_relation(mp->availValue, GTK_LABEL(mp->availLabel));
	
	gtk_dialog_set_default_response(GTK_DIALOG(mp->dlg), GTK_RESPONSE_HELP);

	gtk_window_set_position (GTK_WINDOW(mp->dlg), GTK_WIN_POS_CENTER);
}

void setup_labels_error(char *error_string)
{
	gtk_label_set_text(GTK_LABEL(mp->capacityValue), error_string);
	gtk_label_set_text(GTK_LABEL(mp->usedValue), error_string);
	gtk_label_set_text(GTK_LABEL(mp->availValue), error_string);
	
}

void setup_labels(media_info_ptr dinfo)
{
	gtk_label_set_text(GTK_LABEL(mp->aliasValue), dinfo->alias_name);
	
	gtk_label_set_text(GTK_LABEL(mp->idValue), dinfo->dev_type);
	
	gtk_label_set_text(GTK_LABEL(mp->writeValue), dinfo->write_protect);
	
	gtk_label_set_text(GTK_LABEL(mp->passwordValue), dinfo->password_protect);
	
	gtk_label_set_text(GTK_LABEL(mp->fsValue), dinfo->file_sys);
	
	gtk_label_set_text(GTK_LABEL(mp->mountValue), dinfo->mnt_pnt);
	
	gtk_label_set_text(GTK_LABEL(mp->capacityValue), dinfo->total_capacity);
	
	gtk_label_set_text(GTK_LABEL(mp->usedValue), dinfo->used_size);
	
	gtk_label_set_text(GTK_LABEL(mp->availValue), dinfo->free_size);

	
}


void set_catalogue_error(int err_no){

	if (err_no > 0) {

		switch (err_no) {	
		case 1:
			fprintf(stderr, "%s", _("You have not supplied any parameters\n\n"));
			fprintf(stderr, "%s", _("Usage: sdtmedia_prop [-u units] raw_device_path\n\n"));
			break;
		case 2:
			fprintf(stderr, "%s", _("You have supplied too many parameters\n\n"));
			fprintf(stderr, "%s", _("Usage: sdtmedia_prop [-u units] raw_device_path\n\n"));
			break;
		case 3:
			fprintf(stderr, "%s", _("The PATH you have supplied is not a valid path to a raw device \n\n"));
			fprintf(stderr, "%s", _("Usage: sdtmedia_prop [-u units] raw_device_path\n\n"));
			break;
		default:
			fprintf(stderr, "%s", _("Usage: sdtmedia_prop [-u units] raw_device_path\n\n"));
			break;
		}
		exit(1);
	}

}


void set_unit_str()
{

	if (units == 1)
		unit_str = g_strdup(_("bytes"));
	else if (units == 1024)
		unit_str = g_strdup(_("kilobytes"));
	else if (units == 1048576)
		unit_str = g_strdup(_("megabytes"));
	else
		unit_str = g_strdup(_("gigabytes"));


}

void set_catalogue_protection_status(int protect_stat, media_info_ptr dinfo){
	
	if (strcmp(dinfo->dev_type, _("cdrom")) == 0) {
		dinfo->write_protect = g_strdup(_("True"));
		dinfo->password_protect = g_strdup(_("False"));
	}
	else{
		if (protect_stat < 0){
			dinfo->write_protect = g_strdup(_("Unable to determine protection"));
			dinfo->password_protect = g_strdup(_("Unable to determine password"));

		}
		else {
			switch (protect_stat) {
			case RM_UNPROTECTED:
				dinfo->write_protect = g_strdup(_("False"));
				dinfo->password_protect = g_strdup(_("False"));
				break;
			case RM_WRITE_PROTECTED:
				dinfo->write_protect = g_strdup(_("True"));
				dinfo->password_protect = g_strdup(_("False"));
				break;
			case RM_WRITE_PROTECTED_WP:
				dinfo->write_protect = g_strdup(_("True"));
				dinfo->password_protect = g_strdup(_("True"));
				break;
			case RM_READ_WRITE_PROTECTED:
				dinfo->write_protect = g_strdup(_("True"));
				dinfo->password_protect = g_strdup(_("True"));
				break;
			default:
				dinfo->write_protect = g_strdup(_("Unable to determine protection"));
				dinfo->password_protect = g_strdup(_("Unable to determine password"));
				break;
			}
		}
	}

}

int
main(int argc, char *argv[])
{
	/*Down further
	int error_code;
*/
/*This is the only place err_string is used - comment out.	
	char *err_string;
*/
/*poptContext needed for init with popt table*/

	poptContext		ctx;
	
	/*bytes, megabytes or gigabytes*/
	static char		*bmg = NULL;

	struct poptOption options[] = {
		{NULL,
		'u',
		POPT_ARG_STRING,
		&bmg,
		0,
		"Units to be displayed",
		"UNITS"
		},
		{NULL,
		'\0',
		0,
		NULL,
		0,
	 	NULL,
		NULL
		}
	};

	
	int			error_code;

	media_info_ptr		dinfo;
	
	int			protection_status;


	setlocale (LC_ALL, "");

#ifdef ENABLE_NLS
	bindtextdomain (GETTEXT_PACKAGE, GETTEXT_LOCALEDIR);
	bind_textdomain_codeset (GETTEXT_PACKAGE, "UTF-8");
	textdomain (GETTEXT_PACKAGE);
#endif
	gnome_program_init ("gnomeproperties", "1.0",
				LIBGNOMEUI_MODULE,
				argc, argv,
				GNOME_PARAM_POPT_TABLE, options,
				0, &ctx,
				NULL);

	error_code = parse_args(argc, argv);

	set_catalogue_error(error_code);

	set_unit_str();

	dinfo = initialise_media_struct();

	gui_initialise(usable_file_name);

	device_prop = get_device_prop(usable_file_name);

	setup_total_capacity(device_prop, dinfo);
	
	get_device_type(device_prop, dinfo);
	
	setup_alias_stuff(dinfo);
	
	setup_capacities(dinfo);

	protection_status = get_protection(dinfo);
	
	set_catalogue_protection_status(protection_status, dinfo);

	setup_labels(dinfo);

	gtk_main();


	return 0;

}

/**
 * set_relation
 * @widget : The Gtk widget which is labelled by @label
 * @label : The label for the @widget.
 * Description : This function establishes atk relation
 * between a gtk widget and a label.
 */
 
static void
set_relation(GtkWidget *widget, GtkLabel *label)
{
	AtkObject *aobject;
	AtkRelationSet *relation_set;
	AtkRelation *relation;
	AtkObject *targets[1];
 
	g_return_if_fail(GTK_IS_WIDGET(widget));
	g_return_if_fail(GTK_IS_LABEL(label));
 
	aobject = gtk_widget_get_accessible(widget);

	/* Return if GAIL is not loaded */
	if (!GTK_IS_ACCESSIBLE(aobject))
		return;
	/* Set the ATK_RELATION_LABEL_FOR relation */
	gtk_label_set_mnemonic_widget(label, widget);
 
	targets[0] = gtk_widget_get_accessible(GTK_WIDGET(label));
 
	relation_set = atk_object_ref_relation_set(aobject);
 
	relation = atk_relation_new(targets, 1, ATK_RELATION_LABELLED_BY);
	atk_relation_set_add(relation_set, relation);
	g_object_unref(G_OBJECT(relation));
}
