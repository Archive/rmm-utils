/*
 * Copyright 2002 Sun Microsystems Inc.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Street #330, Boston, MA 02111-1307, USA.
 *
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <strings.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/smedia.h>
#include <dlfcn.h>
#include <volmgt.h>
#include <dirent.h>
#include <sys/types.h>
#include <errno.h>
#include <sys/statvfs.h>
#include "properties.h"

void 
setup_alias_stuff(media_info_ptr dinfo)
{

	char           *filep, info[256], *last, *first, *media_name;
	struct dirent  *dp;
	DIR            *dfd;
	int             c;
	FILE           *ofp;
	char		*mounted_at = NULL;
	char *devroot, *devpath=NULL;

	c = 0;
	filep = NULL;
	media_name = malloc(MAXPATHLEN);
	
	if (strcmp(usable_file_name, vol_file_name) != 0) {
		dinfo->alias_name = strdup(vol_file_name);
	}
	else 
	{
		/* Fixes 4422664. Take into considertaion the value of DTDEVROOT */
		if ((devroot = getenv("DTDEVROOT")) != NULL) {
			devpath = malloc(strlen(devroot)+strlen("/tmp/.removable/")+1);
			sprintf(devpath, "%s/tmp/.removable/", devroot);
		}
		else {
			devpath = malloc(strlen("/tmp/.removable/")+1);
			strcpy(devpath, "/tmp/.removable/");
		}
		dfd = opendir(devpath);

		while (((dp = readdir(dfd)) != NULL) && (c == 0)) {
			strcpy(media_name, dp->d_name);
			filep = malloc(strlen(devpath) + strlen(media_name) + 1);
			strcpy(filep, devpath);
			strcat(filep, media_name);

			/* A check is added to avoid opening a pipe file for reading. As
			** vold puts a notifynnn file which blocks until it is updated.
			*/
			if (strstr(media_name, "notify") != NULL) 
				continue;
			
			if ((ofp = fopen(filep, "r")) == NULL) {
				fprintf(stderr, _("Error: Cannot open %s"), filep);
				exit(1);
			}
			fgets(info, 256, ofp);

			if (strstr(info, usable_file_name) != NULL) {
				c = 1;
				dinfo->alias_name = strdup(media_name);

				if (strcmp(dinfo->dev_type, _("Unknown"))==0 &&
				    strstr(media_name, "cdrom") != NULL) 
				{
					free(dinfo->dev_type);
					dinfo->dev_type = strdup(_("cdrom"));
				}

				fgets(info, 256, ofp);
				last = strrchr(info, ' ');
				*last = '\0';
				last++;
				first = strchr(info, ' ');

				if(first != NULL) {
					*first = '\0';
					first++;
					dinfo->mnt_pnt = strdup(info);
					dinfo->file_sys = strdup(last);
					mounted_at = malloc(strlen(info) + 1);
					strcpy(mounted_at, info);
				} else {
					mounted_at = malloc(strlen(dinfo->alias_name) + 10);
					strcpy(mounted_at, "/rmdisk/");
					strcat(mounted_at, dinfo->alias_name);
					dinfo->mnt_pnt =  strdup(mounted_at);
					dinfo->file_sys = strdup(info);
				}
			}
			fclose(ofp);
		}

		if (c == 0) {
			dinfo->alias_name = strdup(_("Unknown"));
		}
	}

	if (strcmp(dinfo->dev_type, _("Unknown")) == 0) {
		dinfo->file_sys = strdup(_("Unknown"));
		dinfo->mnt_pnt = strdup(_("Unknown"));
	}

	free(mounted_at); 
	free(devpath); 
	if(filep != NULL)
	    free(filep);
	closedir(dfd);
}
