/*
 * Copyright 2002 Sun Microsystems Inc.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Street #330, Boston, MA 02111-1307, USA.
 *
 */
#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <dlfcn.h>
#include <dirent.h>
#include <errno.h>
#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "properties.h"



int parse_args(int argc, char **argv){
	
	int i, perror;
	int error_code;
	
	i = 1;
	perror = 0;
	units = 1024;
	error_code = 0;
	
	if (argc < 2)
		perror = 1;

	if (argc > 4)
		perror = 2;

	if (perror == 0) {
		while (i < argc) {
			if (argv[i][0] == '-')  {
				if(argv[i][1] != 'u')
				{
					perror = 4;
					argv[i][1] = 'X';
				}
				i++;
				if (strcmp(argv[i], "b") == 0) {
					units = 1;
				}
				else if (strcmp(argv[i], "m") == 0) {
					units = 1048576;
				}
				else if (strcmp(argv[i], "g") == 0) {
					units = 1073741824;
				}
				else {
					units = 1024;

				}


			}
			else {
				vol_file_name = malloc(strlen(argv[i]) + 1);
				strcpy(vol_file_name, argv[i]);
				usable_file_name = rmedia_findname(vol_file_name);
			}
			i++;
		}


	}
	if (usable_file_name == NULL) {
		perror = 3;
	}
	return perror;
/*	
	if (perror > 0) {
		switch (perror) {	
		case 1:
			fprintf(stderr, "%s", _("You have not supplied any parameters\n\n"));
			fprintf(stderr, "%s", _("Usage: sdtmedia_prop [-u units] raw_device_path\n\n"));
			break;
		case 2:
			fprintf(stderr, "%s", _("You have supplied too many parameters\n\n"));
			fprintf(stderr, "%s", _("Usage: sdtmedia_prop [-u units] raw_device_path\n\n"));
			break;
		case 3:
			fprintf(stderr, "%s", _("The PATH you have supplied is not a valid path to a raw device \n\n"));
			fprintf(stderr, "%s", _("Usage: sdtmedia_prop [-u units] raw_device_path\n\n"));
			break;
		default:
			fprintf(stderr, "%s", _("Usage: sdtmedia_prop [-u units] raw_device_path\n\n"));
			break;
		}
		exit(1);
	}
*/	
}

char *setup_media_props_error(int errorno)
{

	char		*error_string;


	switch (errorno){
	case ENXIO:
		error_string = "ENXIO No such device";
		break;
			
	case EIO:
		error_string = "EIO Input / Output Error";
		break;

	case ENOTSUP:
		error_string = "ENOTSUP Operation not Supported";
		break;
	default:
		error_string = "Unknown Error occured";		
		break;
	}

	full_capacity = 0;
	return error_string;
}	


void setup_total_capacity(void *dprop, media_info_ptr dinfo)
{

	if (dprop == NULL){
		dinfo->total_capacity = strdup(setup_media_props_error(errno));
		dinfo->used_size = strdup(setup_media_props_error(errno));
		dinfo->free_size = strdup(setup_media_props_error(errno));
	}
	else{
		dinfo->total_capacity = strdup(setup_media_props(dprop));
		
	}

}
/*	
void set_unit_str()
{

	if (units == 1)
		unit_str = strdup(_("bytes"));
	else if (units == 1024)
		unit_str = strdup(_("kilobytes"));
	else if (units == 1048576)
		unit_str = strdup(_("megabytes"));
	else
		unit_str = strdup(_("gigabytes"));


}
*/
char *
setup_media_props(void *dprop)
{
	int		unit_size;
	
	char		*buff;

	float		cap_num;
	int32_t		block_sz;

	char 	*output_string;


	unit_size = strlen(unit_str);
	buff = malloc(unit_size +21);

	/* Fixes 4616568 by doing a division first before multiplying by
	** block size */
	cap_num = rmedia_medium_get_capacity(dprop);
	block_sz = rmedia_medium_get_block_size(dprop);
	cap_num = cap_num / units;
	cap_num = cap_num * block_sz;

	if (units == 1)
		sprintf(buff, "%.0f %s", cap_num, unit_str);
	else if (units == 1024)
		sprintf(buff, "%.2f %s", cap_num, unit_str);
	else
		sprintf(buff, "%.4f %s", cap_num, unit_str);

	full_capacity = rmedia_medium_get_capacity(dprop);
	return buff;
}


media_info_ptr initialise_media_struct()
{
	media_info_ptr dinfo;

	dinfo = malloc(sizeof(struct media_info));
	dinfo->dev_type = strdup("");
	dinfo->alias_name = strdup("");
	dinfo->file_sys = strdup("");
	dinfo->mnt_pnt = strdup("");
	dinfo->used_size = strdup("");
	dinfo->free_size = strdup("");
	dinfo->write_protect = strdup("");
	dinfo->password_protect = strdup("");
	dinfo->total_capacity = strdup("");

	return dinfo;
}
/*
void *get_device_prop(char *usable_file_name)
{
        MediumHandle fp1;
        smmedium_prop_t *device_prop;

        fp1 = rmedia_device_open_medium_handle(usable_file_name);
        device_prop = rmedia_device_get_medium_property(fp1);
        return(device_prop);
}
*/

MediumInfoHandle get_device_prop(char *usable_file_name)
{
        MediumHandle fp1;
        MediumInfoHandle device_prop;

        fp1 = rmedia_device_open_medium_handle(usable_file_name);
        device_prop = rmedia_device_get_medium_property(fp1);
        return device_prop;

}


void get_device_type(void *dprop, media_info_ptr dinfo)
{

	int mtype;

	if (dprop == NULL){
		dinfo->dev_type = strdup(_("Unknown"));
		return;
	}
	else{
		mtype = rmedia_medium_get_media_type(dprop);
	
		switch (mtype) {
			case RM_DK_MO_ERASABLE:
			case RM_DK_MO_WRITEONCE:
			case RM_DK_AS_MO:
			case RM_DK_CDROM:
			case RM_DK_CDR:
			case RM_DK_CDRW:
				dinfo->dev_type = strdup(_("cdrom"));
				break;
			case RM_DK_DVDROM:
			case RM_DK_DVDR:
			case RM_DK_DVDRAM:
				dinfo->dev_type = strdup(_("dvd"));
				break;
			case RM_DK_FIXED_DISK:
				exit(1);
			case RM_FLOPPY:
				dinfo->dev_type = strdup(_("floppy"));
				break;
			case RM_DK_ZIP:
				dinfo->dev_type = strdup(_("zip"));
				break;
			case RM_DK_JAZ:
				dinfo->dev_type = strdup(_("jaz"));
				break;
			default:
				dinfo->dev_type = strdup(_("Unknown"));
				break;
		}
	}

}

/*
void get_protection(media_info_ptr dinfo)
{
	MediumHandle fp1;
	int x;

	fp1 = rmedia_device_open_medium_handle(usable_file_name);
	x = rmedia_device_get_protection_status(fp1);
	if (strcmp(dinfo->dev_type, _("cdrom")) == 0) {
		dinfo->write_protect = strdup(_("True"));
		dinfo->password_protect = strdup(_("False"));
	}
	else{
		if (x < 0){
			dinfo->write_protect = strdup(_("Unable to determine Protection"));

			dinfo->password_protect = strdup(_("Unable to determine Password"));

		}
		else {
			switch (x) {
			case RM_UNPROTECTED:
				dinfo->write_protect = strdup(_("False"));
				dinfo->password_protect = strdup(_("False"));
				break;
			case RM_WRITE_PROTECTED:
				dinfo->write_protect = strdup(_("True"));
				dinfo->password_protect = strdup(_("False"));
				break;
			case RM_WRITE_PROTECTED_WP:
				dinfo->write_protect = strdup(_("True"));
				dinfo->password_protect = strdup(_("True"));
				break;
			case RM_READ_WRITE_PROTECTED:
				dinfo->write_protect = strdup(_("True"));
				dinfo->password_protect = strdup(_("True"));
				break;
			default:
				dinfo->write_protect = strdup(_("Unable to determine Protection"));
				dinfo->password_protect = strdup(_("Unable to determine Password"));
				break;
			}
		}
	}
}
*/

int get_protection(media_info_ptr dinfo)

{
	MediumHandle fp1;
	int x;

	fp1 = rmedia_device_open_medium_handle(usable_file_name);
	x = rmedia_device_get_protection_status(fp1);
	return x;
/*	
	if (strcmp(dinfo->dev_type, _("cdrom")) == 0) {
		dinfo->write_protect = strdup(_("True"));
		dinfo->password_protect = strdup(_("False"));
	}
	else{
		if (x < 0){
			dinfo->write_protect = strdup(_("Unable to determine Protection"));
			dinfo->password_protect = strdup(_("Unable to determine Password"));

		}
		else {
			switch (x) {
			case RM_UNPROTECTED:
				dinfo->write_protect = strdup(_("False"));
				dinfo->password_protect = strdup(_("False"));
				break;
			case RM_WRITE_PROTECTED:
				dinfo->write_protect = strdup(_("True"));
				dinfo->password_protect = strdup(_("False"));
				break;
			case RM_WRITE_PROTECTED_WP:
				dinfo->write_protect = strdup(_("True"));
				dinfo->password_protect = strdup(_("True"));
				break;
			case RM_READ_WRITE_PROTECTED:
				dinfo->write_protect = strdup(_("True"));
				dinfo->password_protect = strdup(_("True"));
				break;
			default:
				dinfo->write_protect = strdup(_("Unable to determine Protection"));
				dinfo->password_protect = strdup(_("Unable to determine Password"));
				break;
			}
		}
	}
*/
}



void 
setup_capacities(media_info_ptr dinfo)
{

	int             x, i, y;

	long            a, b;

	double          freeblk, usedblk;

	char           *endp, *star;

	/* if full_capacity is zero then these values have already been set */
	if (full_capacity != 0) {
		
		int unit_size;
		double used, free;

		x = rmedia_device_get_block_info(dinfo->mnt_pnt, &free, &used);
		if (x < 0){
	 		
			unit_size  = strlen(_("Cannot determine"));
			dinfo->free_size = malloc(unit_size + 1);
			dinfo->free_size = strdup(_("Cannot determine"));
			dinfo->used_size = malloc(unit_size + 1);
			dinfo->used_size = strdup(_("Cannot determine"));
			fprintf(stderr, _("Error %d: Cannot execute a statvfs on %s"), errno, dinfo->mnt_pnt);
		}
		else {
			/* free is returned by the rmedia call above */
			
			freeblk = free / units; 
			unit_size = strlen(unit_str);
			dinfo->free_size = malloc(unit_size + 21);
			dinfo->used_size = malloc(unit_size + 21);
			if(units == 1)
				sprintf(dinfo->free_size, "%.0f %s", freeblk, unit_str);
			else if(units == 1024)
				sprintf(dinfo->free_size, "%.2f %s", freeblk, unit_str);
			else
				sprintf(dinfo->free_size, "%.4f %s", freeblk, unit_str);


			/* used is returned by the rmedia call above */
			usedblk = used / units;
			if(units == 1)
				sprintf(dinfo->used_size, "%.0f %s", usedblk, unit_str);
			if(units == 1024)
				sprintf(dinfo->used_size, "%.2f %s", usedblk, unit_str);
			else
				sprintf(dinfo->used_size, "%.4f %s", usedblk, unit_str);


		}
	}


}

