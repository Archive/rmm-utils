/*
 * Copyright 2002 Sun Microsystems Inc.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Street #330, Boston, MA 02111-1307, USA.
 *
 */

#include <dlfcn.h>
#include <dirent.h>
#include <sys/types.h>
#include <errno.h>
#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <sys/param.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <nl_types.h>
#include <gnome.h>

#include "SDtRmedia.h"



/* Error codes returned by sdtmedia_prot_utils.c */
/*No parameters*/
#define ERR_NO_PRAM					1
/*Too many parameters*/
#define ERR_TOO_PRAM					2
/*Wrong path*/
#define ERR_PATH					3
/*Default*/
#define ERR_DEF						4



typedef struct media_info *media_info_ptr;

struct media_info {
  char *dev_type;
  char *alias_name;
  char *file_sys;
  char *mnt_pnt;
  char *free_size;
  char *used_size;
  char *write_protect;
  char *password_protect;
  char *total_capacity;
};

char			*vol_file_name, *usable_file_name;

char			*unit_str;

char			*lbl_xmstring;

int32_t			units, x;

int32_t 		full_capacity;

extern char		*mounted_at;



extern int parse_args(int argc, char **argv);
/*
extern void set_unit_str();
*/
extern media_info_ptr initialise_media_struct();

/*extern void *get_device_prop(char *usable_file_name);*/
extern MediumInfoHandle get_device_prop(char *usable_file_name);

extern void setup_total_capacity(void *dprop, media_info_ptr dinfo);

extern char *setup_media_props_error(int errorno);

extern char *setup_media_props(void *dprop);

extern void get_device_type(void *dprop, media_info_ptr dinfo);

extern void setup_alias_stuff(media_info_ptr dinfo);

extern void setup_capacities(media_info_ptr dinfo);

/*extern void get_protection(media_info_ptr dinfo);*/
extern int get_protection(media_info_ptr dinfo);









