/*
 * Copyright 2002 Sun Microsystems Inc.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Street #330, Boston, MA 02111-1307, USA.
 *
 */

/* This header file along with format_utils.c is to be shared with CDE-Motif and Gnome
   workspaces as it contains code common to both versions of format utility */

#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <sys/param.h>
#include <sys/types.h>
#include <sys/stat.h>
#include "gutils_fmt.h"
#include "SDtRmedia.h"

/* Errors */
#define ERR_USAGE			1
#define ERR_OPEN_DEVICE 	2
#define ERR_OPEN_FILE		3
#define ERR_REMOUNT			4
#define ERR_DURING_FORMAT	5
#define ERR_PARSING_FILE	6
#define ERR_OK				7


/* Modal messages */

#define MSG_NOT_DETER_SIZE	1
#define MSG_CMD_FAIL		2
#define MSG_OPEN_SLICE_FAIL	3
#define MSG_NOT_PARSE_SLICE	4
#define MSG_FORMAT_DONE		5
#define MSG_OPEN_DEVICE_FAIL	6
#define MSG_NOT_GET_PROPS	7

/* the character between elements of a directory path */
#define PATH_SEP '/'

#ifdef DEBUG
#define DEBUGPRINT(str) printf(str)
#else
#define DEBUGPRINT(str) 
#endif

#define MAXPATHLEN  1024

#define False	0
#define True	1

/* file-system types supported by this format program */
#define FS_PCFS		"pcfs"
#define FS_UFS		"ufs"
#define FS_UDFS		"udfs"
#define FS_NECDOS	"nec"


typedef int	FormatBool;

typedef struct filesys_rec {
	char filesys_name[6];
	struct filesys_rec *nxt;
}    sys_rec, *sys_ptr;  

typedef struct _slice_rec {
	int slice_num;
	char slice_tag[15];
	char slice_flag[5];
	double slice_start, size;
	char slice_filesys[6];
	struct _slice_rec *nextr;
}    slice_rec, *slice_ptr;
	

extern char *vol_label;			/* volume label */
extern char *slice_file;		/* file where slice info is saved */
extern int media_block_size; /* stores the block size of the media */
extern FormatBool path_is_slice;
extern char usable_device[MAXPATHLEN];	/* path to removable device */
extern char usable_device_alias[MAXPATHLEN];	/* alias to removable device */
extern MediumInfoHandle medium_info;	/* data struct returned from XXXXX */
extern slice_ptr start, curr, unused;	/* pointers to slice data structure */
extern FileSystem fstype;
extern char media_size_str[20];	/* (ARG) # blocks on media */
extern char u_dev[MAXPATHLEN];	/* path to physical device if usable_device is a slice*/
extern char u_dev_alias[MAXPATHLEN];	/* alias to device if usable_device is a slice*/
extern char pc_device[MAXPATHLEN], pcfs_device[MAXPATHLEN];	/* Args must have global scope */
extern int PARTITIONED;			/* has the slice editor been called */
extern MediumHandle media_fd;			/* file descripter for open media */
extern int low_level_format_percent;/* current percentage complete for low level format */
extern int old_low_level_format_percent;/* previous percentage complete for low level format */
extern int format_mode;				/* whether status bar will be used */

extern char progress_warn[MAXPATHLEN];



char *get_media_type_from_id(int mid);
extern unsigned long calculate_media_size();/* determine the size of the device */
extern sys_ptr get_file_sys(char sys[256]);	/* read in the partition filesystem info from slice file */
extern slice_ptr slice_create ();			/* create a data struct to store partition info */
extern unsigned long calculate_media_size();		/* determine the size of the device */
extern int recalc_devicename();				/* determine the new path to the device */
extern int read_slice_info();				/* read slice info from slice file */
extern void init_ui_values();
extern void report_system_error(char *);
extern void setup_slice_names();

/* Functions implemented in GUI code to switch on/off UI elements */
extern void enable_ui_ok_cancel(int ok_state, int cancel_state);
extern void toggle_ui_long_format(int enabled);
extern void enable_ui_low_format(int enabled);
extern void enable_ui_filesystem(int enabled);
extern void enable_ui_vol_name_entry (int enabled);
extern void init_fs_buttons(FileSystem *fs_list, int ntypes);
extern gboolean button_delete_cb(GtkWidget *widget, gpointer data);

void run_slice(void);
void create_progress_warn_file(void);
int determine_media_type(void);
int parse_args(int argc, char** argv);
void start_making_slice_filesystems(void);
int get_cylinder_info(int *a, unsigned long *b);

void show_modal_message(int message_code); 
void enable_ui_slice_button(int enabled);

void format_get_focused_widget();
void format_set_focused_widget();

void create_progress_bar ();
