/*
 * Copyright 2002 Sun Microsystems Inc.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Street #330, Boston, MA 02111-1307, USA.
 *
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <string.h>
#include <gnome.h>
#include <signal.h>
#include "gmedia_format.h"
#include "gutils_fmt.h"

/* Callbacks */

void            show_modal_message(int message_code);

void fs_button_cb(GtkWidget * widget, gpointer data)
{

	int             x, status;

	fstype = *(FileSystem *) data;
	status = determine_media_type();
	if (status != ERR_OK) {
		show_modal_message(status);
		remove_files(FALSE);
		exit(-1);
	}
	if (fstype.sliceable == 1 && (media_type == RM_DK_ZIP
				      || media_type == RM_DK_JAZ))
		gtk_widget_set_sensitive(GTK_WIDGET(fApp->slice_button), TRUE);
	else
		gtk_widget_set_sensitive(GTK_WIDGET(fApp->slice_button), FALSE);

	if( !strcmp(fstype.fs_type, FS_UDFS) ||
  	    !strcmp(fstype.fs_type, FS_UFS)) {
		gtk_entry_set_max_length (GTK_ENTRY (fApp->entry), 8);
	} else {
		gtk_entry_set_max_length (GTK_ENTRY (fApp->entry), 11);
	}

	vol_label = (char *)gtk_entry_get_text(GTK_ENTRY(fApp->entry));

	x = fstype.max_volume_label;
	if (strlen(vol_label) > x) {
		vol_label[x] = '\0';
		gtk_entry_set_text(GTK_ENTRY(fApp->entry), vol_label);
		gtk_editable_set_position(GTK_EDITABLE(fApp->entry), x);
	}
}

void quick_radio_cb(GtkWidget * widget, gpointer data)
{

	format_mode = RM_FORMAT_QUICK;

}

void long_radio_cb(GtkWidget * widget, gpointer data)
{

	format_mode = RM_FORMAT_FORCE;

}

void slice_cb(GtkWidget * widget, gpointer data)
{

	format_get_focused_widget();
	run_slice();
	PARTITIONED = 1;

}
void format_cb(GtkWidget * widget, gpointer data)
{

	int             status;

	vol_label = (char *)gtk_entry_get_text(GTK_ENTRY(data));
	format_in_progress = TRUE;
	enable_ui_low_format(FALSE);
	enable_ui_filesystem(FALSE);
	enable_ui_slice_button(FALSE);
	enable_ui_options(FALSE);
	enable_ui_vol_name_entry(FALSE);

	g_signal_handlers_block_by_func(G_OBJECT(fApp->cancel_button),
					button_delete_cb,fApp->dlg);

	create_progress_warn_file();
	status = determine_media_type();

	if (status != ERR_OK) {
		show_modal_message(status);
		remove_files(FALSE);
		exit(-1);
	}

	enable_ui_ok_cancel(FALSE, TRUE);
	if (media_type == RM_FLOPPY) {
		do_floppy();
	} else {
	
		/* User might change his mind and selected a different
		 * file system even after selecting some slices. Just check 
		 * before starting format.
		 */
		if (fstype.sliceable == 0) {
			PARTITIONED = 0;
			if (slice_file != NULL) {
				unlink (slice_file);
			}
		}

		if (PARTITIONED) {
			if (access(slice_file, R_OK) == 0) {
				unmount_and_partition(partition_media);
			} else {
				PARTITIONED = 0;
				unmount_and_do(format_media);
			}
		} else {
			unmount_and_do(format_media);
		}
	}

	g_signal_handlers_unblock_by_func(G_OBJECT(fApp->cancel_button),
					  button_delete_cb,fApp->dlg);

}

void help_cb(GtkWidget * widget, gpointer data)
{
	GError *error = NULL;
	gnome_help_display_desktop (NULL,
				    "user-guide",
				    "wgosnautilus.xml", "gosnautilus-451",
				    &error);

	if (error) {
		GtkWidget *dialog;
		dialog = gtk_message_dialog_new (GTK_WINDOW (data),
						 GTK_DIALOG_MODAL | GTK_DIALOG_DESTROY_WITH_PARENT,
						 GTK_MESSAGE_ERROR,
						 GTK_BUTTONS_CLOSE,
						 _("There was an error displaying help: %s"),
						 error->message);

		g_signal_connect (G_OBJECT (dialog),
				  "response", G_CALLBACK (gtk_widget_destroy),
				  NULL);
		gtk_window_set_resizable (GTK_WINDOW (dialog), FALSE);
		gtk_widget_show (dialog);
		g_error_free (error);
	}
	return;
}



void show_modal_message(int message_code)
{

	GtkWidget      *dialog; /* changed GtkWidget to GtkDialog */
	char           *dialog_title;
	char           *dialog_msg;

	switch (message_code) {
	case MSG_NOT_DETER_SIZE:
		dialog_title = _("Error determining media size");
		dialog_msg = _("gmedia_format cannot determine the size of the media, execution will be terminated.");
		break;
	case MSG_CMD_FAIL:
		dialog_title = _("Error formatting media");
		dialog_msg = _("gmedia_format has encountered an error trying to format this media. Execution will be terminated.");
		break;
	case MSG_OPEN_SLICE_FAIL:
		dialog_title = _("Error accessing slice information file");
		dialog_msg = _("gmedia_format is unable to open the slice file, execution will be terminated.");
		break;

	case MSG_NOT_PARSE_SLICE:
		dialog_title = _("Error parsing slice file");
		dialog_msg = _("gmedia_format cannot parse the information in the slice file, execution will be terminated.");
		break;
	case MSG_FORMAT_DONE:
		dialog_title = _("Format Completed");
		dialog_msg = _("gmedia_format has finished formatting the device.");
		break;

	case MSG_OPEN_DEVICE_FAIL:
		dialog_title = _("Error Opening the Device");
		dialog_msg = g_strdup_printf( _("gmedia_format is unable to open the device: %s, execution will be terminated now."), device_name);
		g_free(device_name);
		break;

	case MSG_NOT_GET_PROPS:
		dialog_title = _("Error Getting the Media Properties");
		dialog_msg = _("gmedia_format is unable to get the properties of the device, execution will be terminated.");
		break;

	}

	if (message_code == MSG_FORMAT_DONE)
		dialog = gtk_message_dialog_new (GTK_WINDOW (fApp->dlg), 							 GTK_DIALOG_MODAL | GTK_DIALOG_DESTROY_WITH_PARENT,
						 GTK_MESSAGE_INFO,
						 GTK_BUTTONS_OK,
						 "%s", dialog_msg);
	else
		dialog = gtk_message_dialog_new (GTK_WINDOW (fApp->dlg),
						 GTK_DIALOG_MODAL | GTK_DIALOG_DESTROY_WITH_PARENT,
						 GTK_MESSAGE_ERROR,
						 GTK_BUTTONS_OK,
						 "%s", dialog_msg);

	gtk_dialog_set_default_response (GTK_DIALOG (dialog), GTK_RESPONSE_OK);
	gtk_window_set_modal(GTK_WINDOW(dialog), TRUE);
	gtk_window_set_title(GTK_WINDOW(dialog), dialog_title);
	gtk_widget_show(GTK_WIDGET(dialog));
	gtk_dialog_run(GTK_DIALOG(dialog));
	gtk_widget_destroy(dialog);

	/* freeing the string only for MSG_OPEN_DEVICE_FAIL as the string 
	 * dialog_msg is malloced only in this case.For rest of the cases 
	 * it's a const char . 
 	 */

	if ( message_code == MSG_OPEN_DEVICE_FAIL)
		g_free(dialog_msg);

	sigignore (SIGCHLD);
}

void log_error(int error_code)
{
	char     usage[] =_("Usage: gmedia_format -d raw_device [ -m mount_point ]");
	const char     *msg;

	switch (error_code) {
	case ERR_USAGE:
		fprintf(stderr, usage);
		break;
	case ERR_OPEN_DEVICE:
		msg = _("Cannot open device %s");
		fprintf(stderr, msg, usable_device);
		break;
	case ERR_OPEN_FILE:
		msg = _("Could not open file %s.");
		fprintf(stderr, msg, slice_file);
		break;
	case ERR_REMOUNT:
		msg = _("Error remounting media please eject and re-insert.");
		fprintf(stderr, msg);
		break;
	case ERR_PARSING_FILE:
		msg = _("Error parsing file %s");
		fprintf(stderr, msg, slice_file);
		break;
	}
}

