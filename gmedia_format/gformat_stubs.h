/*
 * Copyright 2002 Sun Microsystems Inc.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Street #330, Boston, MA 02111-1307, USA.
 *
 */

#include <gnome.h>

/* Callbacks */
void fs_button_cb(GtkWidget *widget, gpointer data);
void quick_radio_cb(GtkWidget *widget, gpointer data);
void long_radio_cb(GtkWidget *widget, gpointer data);
void volumename_entry_cb(GtkWidget *widget, gpointer data);
void format_cb(GtkWidget *widget, gpointer data);
void cancel_cb(GtkWidget *widget, gpointer data);
void help_cb(GtkWidget *widget, gpointer data);
void slice_cb(GtkWidget *widget, gpointer data);
