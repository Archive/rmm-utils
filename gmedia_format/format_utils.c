/*
 * Copyright 2002 Sun Microsystems Inc.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Street #330, Boston, MA 02111-1307, USA.
 *
 */

/* This source file along with format_utils.h is to be shared with CDE-Motif and Gnome
   workspaces as it contains code common to both versions of format utility */

#ifdef HAVE_CONFIG_H
#  include <config.h>
#endif

#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <sys/param.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <dirent.h>
#include "gmedia_format.h"


static int             available[16];

char *slice_file;
int media_block_size;
char usable_device[MAXPATHLEN];	/* args must have global scope */
char usable_device_alias[MAXPATHLEN];
char media_size_str[20];	/* (ARG) # blocks on media */
char pc_device[MAXPATHLEN], pcfs_device[MAXPATHLEN];
slice_ptr start, curr, unused;
char u_dev[MAXPATHLEN];
char u_dev_alias[MAXPATHLEN];
int PARTITIONED = False;
int32_t low_level_format_percent;
int format_mode = RM_FORMAT_QUICK;
FormatBool path_is_slice = False;

unsigned long   calculate_media_size();
unsigned long   ioctl_calculate_media_size();
int             read_slice_info();
void            remove_backup_slice();
sys_ptr         get_file_sys(char sys[256]);
int             parse_slice(char *line);
slice_ptr       slice_create();
void 			setup_slice_names();
void 			report_system_error(char *);


void report_system_error(char *message)
{
	perror(message);
	format_in_progress = FALSE;
	enable_ui_ok_cancel(False, True);
}

int find_alias (char *arg_path, char *dev_path)
{
	struct dirent *dip;
	DIR *dirp;
	char buf[MAXPATHLEN+1];
	char path[MAXPATHLEN+1];

	if ((dirp = opendir ("/vol/dev/aliases")) == NULL)
	{
		return 0;
	}

	while (dip = readdir (dirp)) {

		memset (buf, 0, MAXPATHLEN+1);
		sprintf (path, "/vol/dev/aliases/%s", dip->d_name);

		if (readlink (path, buf, MAXPATHLEN) == -1)
		{
			continue;
		}

		if (strstr (arg_path, buf))
		{
			strcpy (dev_path, path);
			closedir (dirp);
			return 1;
		}
	}

	closedir (dirp);
	return 0;
}

int parse_args(int argc, char** argv){
	
	int	arg, i_count, param_error, j_set;

	i_count = 1;
	param_error = j_set = 0;



	/* brianc
	   following code amended to fix 4331578 */

	if (argc < 3) {
		param_error = 1;
	}else
	{
	    while (i_count < argc) {
		arg = argv[i_count][1];
		switch (arg) {
		case 'd':
	  	    if(argv[i_count][0] == '-')
		    {
			strcpy(usable_device, argv[i_count + 1]);
			if (!find_alias(argv[i_count + 1], usable_device_alias)) {
				strcpy (usable_device_alias, usable_device);
				strcpy (u_dev_alias, "");
			}
			else
				strcpy (u_dev_alias, usable_device_alias);

			argv[i_count][1] = 'D';
			j_set++;
		    }
		    break;

		case '?':
			break;
		}
		i_count = i_count + 2;
	    }
	}

	/* end of bugfix 4331578 */
	if ((param_error > 0) || (j_set < 1)) {
		return(ERR_USAGE);
	}
return ERR_OK;
}


int determine_media_type(){

	MediumInfoHandle medium_info;
	MediumHandle mfd;

	if((mfd = rmedia_device_open_medium_handle(usable_device)) == NULL)
    	{
    	    		perror(usable_device);
					return(MSG_OPEN_DEVICE_FAIL);
    	}	

    	medium_info = rmedia_device_get_medium_property(mfd);
    	if(medium_info == NULL) 
    	{	
               		rmedia_device_close_medium_handle(mfd);
					perror("rmedia_device_get_medium_property");
					return(MSG_NOT_GET_PROPS);
	}

	rmedia_device_close_medium_handle(mfd);	

	media_type = rmedia_medium_get_media_type(medium_info);
	return ERR_OK;

}

void
create_progress_warn_file() {

	pid_t my_id;
    int my_fd;

	my_id = getpid();
	snprintf(progress_warn, MAXPATHLEN, "/tmp/fmt_in_progress_%d", my_id);
	
	if ((my_fd = open(progress_warn, O_CREAT|O_WRONLY)) < 0)
		report_system_error("fopen");
	else {
		write(my_fd, "\n", 1);
		close(my_fd);
	}


}

/* Functions directly ripped out of workprocs.c */

/********************************************************************

GET_MEDIA_TYPE_FROM_ID :
	Simply convert the file system type into a string, ie.
	"floppy", "zip", "jaz"
	
	May need to be expanded for DVD etc, but all these  end
	up being defined as rmdisk anyway, so it should be OK

********************************************************************/

char *get_media_type_from_id(int mid)
{
	switch (mid) {
		case RM_FLOPPY:
			return "floppy";
		case RM_DK_ZIP:
			return "zip";
		case RM_DK_JAZ:
			return "jaz";
		default:
			return "rmdisk";/* unknown! */
	}
}


/********************************************************************

WAIT_FOR_VOLMGT_RENAME:
	This is a HACK, that waits a number of seconds for volmgt to
	update the name of the medium. This is achieved by sleeping
	for 10 seconds.

********************************************************************/

static void wait_for_volmgt_rename()
{

	DEBUGPRINT("waiting for volmgt...\n");
	sleep(5);
	
}


/********************************************************************

RECALC_DEVICENAME:
	This determines the name of the medium after a change has
	been applied that could change its name, 
	ie. rmformat -b label, or an fdisk
	
	Basically it determines the type of media, zip, jaz etc.
	then scrolls thru the nicknames ie jaz0 to jaz15, to see 
	if the path to that device matches the current device we
	are working on. 
	This works approx 90% of the time, when it fails its due 
	to volmgt problems, so we re make the device path with the
	current vol label and try again. This should work, the only
	time it will fail is if volmgt has appended a #N at the end
	of the label. So we check for that. Currently we just look
	for #1, this should be expanded to check from #1 to #16.
	

********************************************************************/

/* Modify the name of the device stored in usable_device */
int recalc_devicename()
{
	char nickname[20];
	char *nnroot;
	char *media_name;
	char device_path[MAXPATHLEN];
	char *tmp;
	int idx, jdx;
	unsigned long media_size;
	int try_count, found;

	try_count = 0;
	found = 0;

	nnroot = get_media_type_from_id(media_type);

	/*
	 * The path is of the form $VOLMGTROOT/dev/rdsk/device/nickname. The
	 * new path simply has a new nickname, so strip off the old one and
	 * look for the new one.
	 */

	strncpy(device_path, u_dev, sizeof(device_path));
	tmp = (char *)strrchr(device_path, PATH_SEP);
	if (tmp == NULL)
		return False;

	*tmp = '\0';
	
	while( (try_count < 10) && (found == 0) )
	{
	    wait_for_volmgt_rename();
	    for (idx = 0; idx < 16; idx++) {
		(void) sprintf(nickname, "%s%d", nnroot, idx);
		media_name = rmedia_findname(nickname);
		if (media_name != NULL) {
			if ((tmp = (char *)strstr(media_name, device_path)) != NULL) {
				(void) strcpy(usable_device, media_name);
				(void) strcpy(u_dev, media_name);
				(void) strcpy(pc_device, usable_device);

				if (strlen(u_dev_alias) <= 0)
					strcpy (u_dev_alias, u_dev);

				found++;
				if(!PARTITIONED)
					media_size = calculate_media_size();
				if(found > 0)
					break;
			}
		}
	    }
	    try_count++;
	}
	
	if (found > 0)
		return True;
	else	
		return False;
}





/********************************************************************

CALCULATE_MEDIA_SIZE :

	A simple call to determine the size of the media. The size
	is determined in sectors and is calculated by NC * NH * NS
	
********************************************************************/
unsigned long calculate_media_size()
{
	unsigned long media_size;
	MediumHandle  media_handle;
	MediumInfoHandle medium_info;

	media_size = 0;
	media_size = (unsigned long)ioctl_calculate_media_size();
	if(media_size == 0) {
		media_handle = rmedia_device_open_medium_handle(usable_device);
        	if (media_handle != NULL) {
               		medium_info = (void *) rmedia_device_get_medium_property(media_handle);
               		if (medium_info != NULL) 
                        	media_size =(unsigned long)rmedia_medium_calculate_media_size(medium_info);
        	}
        	rmedia_device_close_medium_handle(media_handle);
	}

	(void) snprintf(media_size_str, sizeof(media_size_str), "%lu", media_size);
	return media_size;
}



unsigned long ioctl_calculate_media_size()
{
    unsigned long ts;
    int ifd, nc, x;
    struct dk_geom ioc_struct;


    if((ifd = open(usable_device, O_RDONLY)) < 0)
    {
        close(ifd);
	return 0;
    }
	
    x = ioctl(ifd, DKIOCGGEOM, &ioc_struct);
    if( x < 0)
    {
        close(ifd);
	return 0;
    }
    

    ts = ioc_struct.dkg_nhead * ioc_struct.dkg_nsect * ioc_struct.dkg_ncyl;
	
    close(ifd);
    return ts;
}


/**************************************************************************

GET_CYLINDER_INFO
	replaces the two passed parameters with the number of cylinders
	and the number of blocks.

	Function was updated to use libsmedia call if ioctl failed
	or returned ZERO.
	
	Returns 1 if successful, else 0 if operation failed.
**************************************************************************/	

int get_cylinder_info(int *a, unsigned long *b)
{
    int ifd, x, found;
    struct dk_geom ioc_struct;
    MediumHandle mfd; 
    MediumInfoHandle medium_info;

    found = 0;

    if ((mfd = rmedia_device_open_medium_handle(usable_device)) != NULL)
    {
	if  ((medium_info = rmedia_device_get_medium_property(mfd)) != NULL)
	{
		if (rmedia_medium_get_cylinders (medium_info) > 0) {
 			*a = rmedia_medium_get_cylinders(medium_info);
           		*b = (*a - 1) * rmedia_medium_get_heads(medium_info) * rmedia_medium_get_sectors(medium_info);
           		found++;        
		}
         }
    }
    rmedia_device_close_medium_handle(mfd);
    

    if(found == 0)
    {
       if((ifd = open(usable_device, O_RDONLY)) < 0)
       {
           close(ifd);
       }else {

           x = ioctl(ifd, DKIOCGGEOM, &ioc_struct);
           if( x < 0)
           {
    	       close(ifd);
           }else {
               if (ioc_struct.dkg_ncyl > 0) {
                   *a = ioc_struct.dkg_ncyl;
                   *b = (*a - 1) *ioc_struct.dkg_nhead * ioc_struct.dkg_nsect;
                   found++;
               }
    	   }
       } 
    }
    
	return(found);
}

void run_slice()
{
    if (slice_file == NULL)
        slice_file = tmpnam(NULL);

    format_in_progress = TRUE;
    enable_ui_ok_cancel(False, True);
    enable_ui_low_format(False);
    enable_ui_filesystem(False);
    enable_ui_slice_button(False);
    enable_ui_vol_name_entry(False);

    launch_slice_editor();
}

/*****************************************************************

		   Imported from Slice Editor
		   
	These functions parse thru the slice file, and build up 
	a linked list of all the slices, containing name, size 
	file system etc.
		   
		   
*****************************************************************/



int read_slice_info( )
{

FILE *filp;
char ast[256];
int fst_line, x;
sys_ptr fs;


   fst_line = 0;
   x = 0;

   if ((filp = fopen(slice_file, "rw")) == NULL) {
	if ((filp = fopen(slice_file, "w")) == NULL) {
		return(ERR_OPEN_FILE);
	}
   }
   else {
	/*readwrite = 1;*/

	/* read any information in slice file */

	fgets(ast, 256, filp);
	/*nslice = 0;*/

	while (!feof(filp)) {

		if ((ast[0] == '#') && (fst_line == 0)) {
			/* see if the file contains filesys info */

			fs = get_file_sys(ast);
			
			fst_line++;
		}

		if ((ast[0] != '#') && (ast[0] != '\n')) {
			/* this line contains info, parse thru it */

			x = parse_slice(ast);
			/*nslice++;*/
		}

		if (x != 0) {
			/*
			 * an error occured during parsing, notify
			 * user and exit the program
			 */
			return(ERR_PARSING_FILE);
		}
		else {
			fgets(ast, 256, filp);
		}
	}
   }
   
   fclose(filp);
   curr = start;
   while(curr != NULL)
   {
   
   	strcpy(curr->slice_filesys, fs->filesys_name);
   	curr = curr->nextr;
   	fs = fs->nxt;
   
   }
 
   remove_backup_slice();
   return(0);


}

void remove_backup_slice()
{
   slice_ptr tmp_s;

   curr = start;
   tmp_s = start;

   while(curr != NULL)
   {
   	if(strcmp(curr->slice_tag, "backup") == 0)
   	{
   		if(curr == start)
   		{
   			start = curr->nextr;
   			free(curr);
   			curr = NULL;
   		} else {
   			tmp_s->nextr = curr->nextr;
   			free(curr);
   			curr = NULL;
   		}
   
   	} else {
   		tmp_s = curr;
   		curr = curr->nextr;
	}
 
   }


}

sys_ptr get_file_sys(char sys[256])
{
	sys_ptr s, c, t;
	char *tokn;
	int isbackup; /* 0 if no backup slice */
	
	isbackup = 0;

	
	s = (sys_ptr) malloc(sizeof(sys_rec));
	s->nxt = NULL;
	c = s;


	tokn = (char *)strtok(sys, "# :,=\t\"\\\n");
	while (tokn != NULL) {
		strcpy(c->filesys_name, tokn);
		tokn = (char *)strtok(NULL, " :,=\"\t\\\n");
		if (tokn != NULL) {
			c->nxt = (sys_ptr) malloc(sizeof(sys_rec));
			c = c->nxt;
			c->nxt = NULL;
		}
	}

	c = s;
	while (c != NULL) {
		if(strcmp(c->filesys_name, "backup"))
			isbackup = 1;
		c = c->nxt;
	}
	
	if(isbackup == 0)


 		available[2] = 1;
	


	return (s);

}

int parse_slice(char *line)
{
	char *tokn, *tmpt;
	int pos, num_var, i, n, x;


	if (start == NULL) {
		start = slice_create();
		curr = start;
		curr->nextr = NULL;
	}
	else {
		curr->nextr = slice_create();
		curr = curr->nextr;
		curr->nextr = NULL;

	}



	pos = 0;
	tokn = (char *)strtok(line, " :,=\t\"\\\n");
	while (tokn != NULL) {
		if (strcmp(tokn, "slices") != 0) {

			if (pos < 3) {
				n = 0;
				for (i = 0; tokn[i] >= '0' && tokn[i] <= '9'; i++)
					n = (10 * n) + (tokn[i] - '0');

			}

			switch (pos) {
			case 0:
				curr->slice_num = n;
				break;
			case 1:
				curr->slice_start = n;

				break;
			case 2:
				curr->size = n;
				break;
			case 3:
				if (strlen(tokn) == 0)
					strcpy(curr->slice_flag, "-");
				else
					strcpy(curr->slice_flag, tokn);
				break;
			case 4:
				if (strlen(tokn) == 0)
					strcpy(curr->slice_tag, "-");
				else
					strcpy(curr->slice_tag, tokn);
				break;
			case 5:
				if (strlen(tokn) == 0)
					strcpy(curr->slice_filesys, "-");
				else
					strcpy(curr->slice_filesys, tokn);
				break;
			default:
				return (1);
			}



			pos++;
		}

		tokn = (char *)strtok(NULL, " :,=\"\t\\\n");
		n = 0;
	}

	available[curr->slice_num] = 1;
	return (0);


}


/********************************************************************

SETUP_SLICE_NAMES :
	This is simply appending "/s" and the slice number to the
	end of the volmgt path of the medium.

********************************************************************/
void setup_slice_names()
{
	char *tmp;
	char slice_number[20];
	


	strcpy(pc_device, usable_device);
	sprintf(slice_number, "/s%d", curr->slice_num);
	strcat(pc_device, slice_number);



}

/********************************************************************

START_MAKING_SLICE_FILESYSTEMS :

	We have read in the list of slices to be created. Set a pointer
	to the start of the list. Set slice Id to 'c',  which will be
	used for PCFS partitions. Determine the Slice name ie. s0, s1..s15
	and then make the file system for this slice.

********************************************************************/
void start_making_slice_filesystems()
{

	char slice_id;

	curr = start;
	slice_id = 'c';
	setup_slice_names();
	

	
	make_sliced_filesystems();
	
}



slice_ptr slice_create()
{
	slice_ptr s;

	s = (slice_ptr) malloc(sizeof(slice_rec));

	s->slice_num = -1;
	s->slice_start = -1;
	s->size = -1;
	sprintf(s->slice_tag, "-");
	sprintf(s->slice_flag, "-");
	sprintf(s->slice_filesys, "-");

	return s;
}

static int device_is_slice(char *devpath)
{
	char *t=(char *)strrchr(devpath, PATH_SEP);

	t++;
	if ((*t == 's') && ((t[1] >= '0') && t[1] <= '9'))
		return True;
	return False;
}
void init_ui_values()
{
	char *tmp_p;
	int actual_prot;
	FileSystem *fs_list;
	int ntypes;
	MediumHandle media_fd;
	MediumInfoHandle medium_info;	

	strcpy(u_dev, usable_device);

	if (strlen (u_dev_alias) <= 0)
		strcpy (u_dev_alias, u_dev);

	if ((media_fd = rmedia_device_open_medium_handle(usable_device)) == NULL) {
		report_system_error(usable_device);
		return;
	}

	/* Get device info and medium info so it is available */

	if ((medium_info = rmedia_device_get_medium_property(media_fd)) == NULL) {
		report_system_error(usable_device);
		rmedia_device_close_medium_handle(media_fd);
		return;
	}

	media_block_size = rmedia_medium_get_block_size(medium_info);
	
	if(rmedia_medium_get_media_type(medium_info) != RM_FLOPPY) {
		low_level_format_percent = rmedia_device_check_format_status(media_fd);
		if (low_level_format_percent < 0) {
			report_system_error("Error on media!");
			rmedia_device_close_medium_handle(media_fd);
			return;
		}
		create_progress_bar();

	} else {
		enable_ui_low_format(False);
	}

	fs_list = rmedia_medium_get_supported_filesystems(medium_info, &ntypes);
	init_fs_buttons(fs_list, ntypes);

	actual_prot = rmedia_device_get_protection_status(media_fd);
	if(actual_prot != RM_UNPROTECTED)
	{
		enable_ui_low_format(False);
		format_in_progress = FALSE;
		enable_ui_ok_cancel(False, True);
		enable_ui_slice_button(False);
		rmedia_device_close_medium_handle(media_fd);
		return;
	}

	path_is_slice = False;
	if (low_level_format_percent != 100) {
		format_mode = RM_FORMAT_FORCE;
		/* update radio box */
		toggle_ui_long_format(True);
	} else if (device_is_slice(usable_device)) {
		/* can't low-level format a slice - disable radio items */
		/* prevent other incompatible options by setting this flag */
		path_is_slice = True;
		enable_ui_low_format(False);
		format_mode = RM_FORMAT_FORCE;
		tmp_p = strrchr(u_dev, PATH_SEP);
		tmp_p = NULL;
		low_level_format_percent = 0;
	} else {
		low_level_format_percent = 0;
	}

	rmedia_device_close_medium_handle(media_fd);
}




