/*
 * Copyright 2002 Sun Microsystems Inc.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Street #330, Boston, MA 02111-1307, USA.
 *
 */

#include <gnome.h>
#include <libgnomeui/gnome-about.h>
#include "format_utils.h"

typedef struct {
	GtkWidget      *dlg;
	GtkWidget      *table;
	GtkWidget      *table1;
	GtkWidget	   *table2;
	GtkWidget	   *table3;
	GtkWidget      *intro_label;
	GtkWidget      *lowlevel_label;
	GtkWidget      *radio_label;
	GtkWidget      *fs_label;
	GtkWidget      *fs_type_label;
	GtkWidget      *volume_label;
	GtkWidget      *separator1;
	GtkWidget      *separator2;
	GtkWidget      *separator3;
	GtkWidget      *separator4;
	GtkWidget      *separator5;
	GtkWidget      *separator6;
	GtkWidget      *separator7;
	GtkWidget      *vbox;
	GtkWidget      *vbox1;
	GtkWidget      *menu;
	GtkWidget      *option_menu;
	GtkWidget      *quick_radio_button;
	GtkWidget      *long_radio_button;
	GSList         *group;
	GtkWidget      *progress_bar;
	GtkWidget      *hbox;
	GtkWidget      *hbox1;
	GtkWidget     **fs_buttons;
	GtkWidget      *entry;
	GtkWidget      *format_button;
	GtkWidget      *cancel_button;
	GtkWidget      *help_button;
	GtkWidget      *slice_button;
	GtkWidget      *progress_bar_label;
	GtkWidget      *focused_widget;
}               FormatApp;

FormatApp      *fApp;

FileSystem      fstype;
gchar          *vol_label;
GtkWidget      *dialog_vbox;
GtkWidget      *hbox2;
gchar          *device_name;
gboolean       format_in_progress;
int            media_type; /* stores the media type we are formatting */

void enable_ui_options(int enabled);

void log_error(int error_code);
