/*
 * Copyright 2002 Sun Microsystems Inc.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Street #330, Boston, MA 02111-1307, USA.
 *
 */

#ifdef HAVE_CONFIG_H
#  include <config.h>
#endif
#include <stdio.h>
#include <string.h>


/*****************************************************************************/

#include "gutils_fmt.h"
#include "gmedia_format.h"
#include "gcommand.h"
#include <sys/types.h>
#include <unistd.h>
#include <sys/wait.h>
#include <sys/errno.h>
#include <errno.h>
#include <setjmp.h>
#include <unistd.h>
#include <stropts.h>

FileSystem fstype;
char progress_warn[MAXPATHLEN];
CommandInfo external_cmd;
MediumHandle media_fd;
char *vol_label;
int old_low_level_format_percent = 0;

static char *fdisk_tmpfile;
static FILE *fdisk_file;
static char tmpstr[MAXPATHLEN];
static char tmpstr3[MAXPATHLEN];

gboolean make_other_slice_filesystems();
gboolean resync_after_insert();
gboolean do_floppy();
gboolean format_floppy();
void remove_files(gboolean ismounted);



/******************/


static gboolean resync_to_volmgt_path(gpointer cd);
static gboolean make_filesystem(gpointer client_data);
static gboolean continue_makeing_filesystem(gpointer client_data);
static gboolean tidy_after_cmd(gpointer client_data);
static gboolean tidy_after_slice(gpointer client_data);
static gboolean floppy_low_level_format();
static void unmount_slices();


/********************/



/********************************************************************

RESYNC_TO_VOLMGT_PATH:
	This routine should only be called after executing a command
	that may change the volmgt path to the media.
	
	Fistly it checks is the command executed properly.
	
	If it did then it calls recalc_devicename to determine the
	current path to the medium.
	
	Finally it executes the next command in the sequence
	

********************************************************************/

gboolean resync_to_volmgt_path(gpointer client_data)
{
	int continue_commands = TRUE;
	

	if ((external_cmd.exit_status >> 8) && external_cmd.cmd_spec[external_cmd.ix - 1].cs_end_if_nonzero) {
		continue_commands = FALSE;
		DEBUGPRINT("Non zero status, not continuing\n");
		report_system_error("Failure during format");
	}
	if (continue_commands) {
		/*
		 * We need to work out what is the new name for the medium
		 */

		if (recalc_devicename()) {
/* The problem with using strings such as tmpstr and tmpstr3 to store commands
   that are stacked in a list is that the device name may have changed by
   preceeding commands in the list before they get executed, and so the format 
   will fail. We can prevent this by reforming the command string every time
   the volmgt path to a device changes.
   NOTE: these strings are only used for PCFS formats
 */		
		    if (!strcmp(fstype.fs_type, FS_PCFS)) {
			if(tmpstr3 != NULL)
			{
			    char *quoted_label = NULL;

			    if (vol_label)
			       quoted_label = g_shell_quote(vol_label);

			    snprintf(tmpstr3, MAXPATHLEN, "/usr/bin/echo y | %s -F pcfs -o b=%s %s:c > /dev/null 2>&1",
			    	     CMD_MKFS, vol_label ? quoted_label : get_media_type_from_id(media_type),
			    	     usable_device_alias);

			    g_free (quoted_label);
			}
			if(tmpstr != NULL)
			{
			    snprintf(tmpstr, MAXPATHLEN, "/usr/bin/echo y | %s -n -v -F %s %s > /dev/null 2>&1", 
		  		 CMD_FDISK, fdisk_tmpfile, u_dev_alias );	     
			    
			}
		    }
			/* Execute the next command in the sequence */
			external_cmd.ix++;
			ExecuteCurrentCommand();
		} else {
			report_system_error("Failure during format");
		}
	}

	return FALSE;		/* remove the work procedure from the list */
}








/********************************************************************

TIDY_UP:
	This is a cut down version of resync_to_volmgt_path. It is
	used when the command we just executed cannot change the
	volmgt path. So we just check that it completed OK, and 
	execute the next command in the sequence.

********************************************************************/
static gboolean tidy_after_cmd(gpointer client_data)
{
	

	if ((external_cmd.exit_status >> 8) && external_cmd.cmd_spec[external_cmd.ix].cs_end_if_nonzero) {
		show_modal_message(MSG_CMD_FAIL);
		DEBUGPRINT("Non zero status, not continuing\n");
		remove_files(FALSE);
		format_in_progress = FALSE;
		enable_ui_ok_cancel(TRUE, TRUE);
		return FALSE;
	}
	external_cmd.ix++;
	if (external_cmd.ix == external_cmd.ncmds) {
		show_modal_message(MSG_FORMAT_DONE);
		rmedia_device_close_medium_handle(media_fd);
		remove_files(TRUE);
		format_in_progress = FALSE;
		enable_ui_ok_cancel(FALSE, TRUE);
		return FALSE;
	}
	/* Reset as u_dev will have changed since last used */
	
	
	if (path_is_slice)
	{
		snprintf(tmpstr, MAXPATHLEN, "/usr/bin/echo y | %s -F quick %s", CMD_RMFORMAT, u_dev_alias);
		(void)strcpy(pcfs_device, u_dev_alias);
		(void) strcat(pcfs_device, ":c");
	}
	/* Execute the next one in the sequence */
	ExecuteCurrentCommand();

	return FALSE;		/* remove the work procedure from the list */
}



/********************************************************************

UNMOUNT_AND_PARTITION : MERGE

	Simply unmounts the current medium, and calls the function 
	partition_media, which is responsible for slicing the medium.
	
	
	No difference between this, unmount_and_do and do_floppy.
	They should all be merged into one.
	
********************************************************************/

void unmount_and_partition(gboolean (*follow_func)())
{
	/*
	 * First, unmount the device. Then execute the appropriate series of
	 * commands . unmount_and_partition(partition_media)
	 *
	 * NOTE: for x86 may need to do an fdisk after we unmount
	 */

	
	INITCMDS(external_cmd);
	NEWCMD(external_cmd, CMD_VOLRMMOUNT);
	AddArg(external_cmd, "-e");
	AddArg(external_cmd, usable_device_alias);
	SetFollowFunc(external_cmd, follow_func);
	SetStopOnFailure(external_cmd, TRUE);
	BeginCommandSequence();
}



/********************************************************************

UNMOUNT_AND_DO : MERGE

	Simply unmounts the current medium, and calls the function 
	format_media, which is responsible for formatting the medium.
	
	
	No difference between this, unmount_and_partition and do_floppy.
	They should all be merged into one.
	
********************************************************************/
void unmount_and_do(gboolean (*follow_func)())
{
	/*
	 * First, unmount the device. Then execute the appropriate series of
	 * commands
	 */
	 

	INITCMDS(external_cmd);
	NEWCMD(external_cmd, CMD_VOLRMMOUNT);
	AddArg(external_cmd, "-e");
	AddArg(external_cmd, u_dev_alias);
	SetFollowFunc(external_cmd, follow_func);
	SetStopOnFailure(external_cmd, TRUE);
	BeginCommandSequence();
}



/********************************************************************

MAKE_PARTITIONS :

	this routine slices the medium using the slice file created
	by sdtmedia_slice. It then labels the medium and then 
	remounts the medium. Remounting will be carried out by 
	volrmmount -e, which currently doesnt work, so
	CMD_MYVOLRMMOUNT is used as a HACK. Basically execution is
	halted until the user does an eject and insert of the medium.
	
	The final command has a follow function called resync_after_insert,
	which starts laying down new filesystems on the slices.

********************************************************************/

static gboolean make_partitions(gpointer client_data)
{
	char *label_opt = NULL;	/* not same across all fs-types! */
	unsigned long media_size;
	char *tmp;
	int status = 0;
	
	
	INITCMDS(external_cmd);




    if((  media_size = calculate_media_size()) == 0) {
		show_modal_message(MSG_NOT_DETER_SIZE);
		log_error(ERR_OPEN_DEVICE);
		remove_files(FALSE);
		exit(-1);
	}
     
	status = read_slice_info();
	switch(status) {
		case 0:
			break;
		case ERR_OPEN_FILE:
			log_error(status);
			show_modal_message(MSG_OPEN_SLICE_FAIL);
			remove_files(FALSE);
			exit(1);
			break;
		case ERR_PARSING_FILE:
			log_error(status);
			show_modal_message(MSG_NOT_PARSE_SLICE);
			remove_files(FALSE);
			exit(-1);
			break;
	}


	if (!path_is_slice) {
	    /* Command: rmformat */
		/*
	     * This command is a hack to repair VTOC which incorrectly reports
	     * no. of cylinders after format using smedia_format()
	     */
	    NEWCMD(external_cmd, CMD_RMFORMAT);
	    AddArg(external_cmd, "-b");
	    AddArg(external_cmd, vol_label ? vol_label : get_media_type_from_id(media_type));
	    AddArg(external_cmd, usable_device_alias);
	    SetFollowFunc(external_cmd, resync_to_volmgt_path);
	    SetStopOnFailure(external_cmd, TRUE);
		
	
		
	} else 
	{
		NEWCMD(external_cmd, CMD_VOLRMMOUNT);
		AddArg(external_cmd, "-i");
		AddArg(external_cmd, u_dev_alias);
		SetFollowFunc(external_cmd, resync_to_volmgt_path);
		SetStopOnFailure(external_cmd, TRUE);
		
		NEWCMD(external_cmd, CMD_VOLRMMOUNT);
		AddArg(external_cmd, "-e");
		AddArg(external_cmd, u_dev_alias);
		SetFollowFunc(external_cmd, resync_to_volmgt_path);
		SetStopOnFailure(external_cmd, TRUE);
		
		NEWCMD(external_cmd, CMD_RMFORMAT);
		AddArg(external_cmd, "-b");
		AddArg(external_cmd, vol_label ? vol_label : get_media_type_from_id(media_type));
		AddArg(external_cmd, u_dev_alias);
		SetFollowFunc(external_cmd, tidy_after_cmd);
		SetStopOnFailure(external_cmd, TRUE);	
	
	
	
	}	
		
	NEWCMD(external_cmd, CMD_RMFORMAT);
	AddArg(external_cmd, "-s");
	AddArg(external_cmd, slice_file);
	AddArg(external_cmd, usable_device_alias);
	SetFollowFunc(external_cmd, resync_to_volmgt_path);
	SetStopOnFailure(external_cmd, TRUE);
	
	NEWCMD(external_cmd, CMD_RMFORMAT);
	AddArg(external_cmd, "-b");
	AddArg(external_cmd, vol_label ? vol_label : get_media_type_from_id(media_type));
	AddArg(external_cmd, usable_device_alias);
	SetFollowFunc(external_cmd, resync_to_volmgt_path);
	SetStopOnFailure(external_cmd, TRUE);
	
	
	strcpy(pcfs_device, usable_device_alias);

	

	
	NEWCMD(external_cmd, CMD_VOLRMMOUNT);
	AddArg(external_cmd, "-i");
	AddArg(external_cmd, usable_device_alias); /* was pcfs_device*/
	SetFollowFunc(external_cmd, resync_to_volmgt_path);
	SetStopOnFailure(external_cmd, TRUE);
	
	NEWCMD(external_cmd, CMD_VOLRMMOUNT);
	AddArg(external_cmd, "-e");
	AddArg(external_cmd, usable_device_alias);
	SetFollowFunc(external_cmd, resync_after_insert);
	SetStopOnFailure(external_cmd, TRUE);

	BeginCommandSequence();
	return FALSE;
	
	
}


/********************************************************************

RESYNC_AFTER_INSERT :

	First thing to do is check if the last command was sucessful.
	
	then determine the volmgt path to the medium (recalc_devicename)
	
	finally start making the slice filesystems (start_making_slice_filesystems)

********************************************************************/	
gboolean resync_after_insert()
{

	int continue_commands = TRUE;


	if ((external_cmd.exit_status >> 8) && external_cmd.cmd_spec[external_cmd.ix - 1].cs_end_if_nonzero) {
		continue_commands = FALSE;
		DEBUGPRINT("Non zero status, not continuing\n");
		report_system_error("Failure during format");
	}
	if (continue_commands) {
		/*
		 * We need to work out what is the new name for the medium
		 */

		if (recalc_devicename()) {
			
			start_making_slice_filesystems();
		} else {
			report_system_error("Failure during format");
		}
	}

	return FALSE;		/* remove the work procedure from the list */
}
	



/********************************************************************

MAKE_OTHER_SLICE_FILESYSTEMS :

	This is called after we have created a slice file system, it
	simply increments the pointer to the next slice, and sets 
	about creating that file system. 
	When there  are no slices left, it ejects the medium.

********************************************************************/
gboolean make_other_slice_filesystems(gpointer data)
{
	char *tmpp;

	curr = curr->nextr;
	if(curr != NULL)
	{
		setup_slice_names();
		make_sliced_filesystems();
		
	}else {
	
		tmpp = strrchr(pc_device, PATH_SEP);
		*tmpp = '\0';
		
		
		remove_files(TRUE);
	
		INITCMDS(external_cmd);
		NEWCMD(external_cmd, CMD_VOLRMMOUNT);
		AddArg(external_cmd, "-i");
		AddArg(external_cmd, pc_device);
		SetFollowFunc(external_cmd, tidy_after_cmd);
		SetStopOnFailure(external_cmd, TRUE);
		BeginCommandSequence();
	}
	
	

	return FALSE;
}


/********************************************************************

MAKE_SLICED_FILESYSTEMS :

	This routine puts a filesystem on a slice. If the filesystem 
	is PCFS, it needs to perform an FDISK on that partition first,
	and then apply the filesystem using MKFS.
	Upon completion, it calls make_other_slice_filesystems to
	make the filesystem on the next slice.
	
********************************************************************/
void make_sliced_filesystems()
{	
	char slice_number[20];
	char extra_args[100];
	char id_as_str[1];
	int media_size;


	
	INITCMDS(external_cmd);	

	/* 	Now we apply the file  system using mkfs
		mkfs -f filesys pathto slice [size]
		we only add size for ufs and udfs filesystems.
	*/
	
	NEWCMD(external_cmd, CMD_MKFS);
	AddArg(external_cmd, "-F");
	AddArg(external_cmd, curr->slice_filesys);
	
	sprintf(media_size_str, "%d", (int)(curr->size / media_block_size));
	
	if(strcmp(curr->slice_filesys, "pcfs") == 0)
	{
		AddArg(external_cmd, "-o");
		strcpy(extra_args, "size=");
		strcat(extra_args, media_size_str);
		strcat(extra_args, ",nofdisk");
		AddArg(external_cmd, extra_args );
		AddArg(external_cmd, pc_device);
	} else {

		AddArg(external_cmd, pc_device);
		AddArg(external_cmd, media_size_str  );
	}
		

	SetFollowFunc(external_cmd, make_other_slice_filesystems);
	SetStopOnFailure(external_cmd, TRUE);
		
	
	BeginCommandSequence();
}



/********************************************************************

FORMAT_MEDIA :
	Performs a lowlevel format of a medium, and sets up the 
	update_format_bar callback, to show the status of the
	format.
	This is not used on floppies, as no mechanism for getting
	the current status exists for floppies.
	
********************************************************************/

 
gboolean format_media(gpointer client_data)
{

	gboolean	RetVal = TRUE;
	if ((external_cmd.exit_status >> 8) && external_cmd.cmd_spec[external_cmd.ix - 1].cs_end_if_nonzero) {
		DEBUGPRINT("Non zero status, not continuing\n");
		goto failed;
	}
	
	

	/* Now check the format status of the device */

	if (low_level_format_percent != 100) {
		/* not yet formatted */
		DEBUGPRINT("About to begin formatting RM_FORMAT_IMMEDIATE\n");
		
		if ((media_fd = rmedia_device_open_medium_handle(usable_device)) == NULL) {
			report_system_error(u_dev_alias);
			return TRUE;
		}
	
		if (rmedia_device_format(media_fd, format_mode, RM_FORMAT_IMMEDIATE) < 0) {
			DEBUGPRINT("got an error from format call\n");
			external_cmd.exit_status = -1;
			goto failed;
		} else {
			gtk_timeout_add(1000, (GtkFunction)update_format_bar, NULL); 
					rmedia_device_close_medium_handle(media_fd);
	
			
			}
		return FALSE;
	} else {
		external_cmd.exit_status = 0;

		RetVal = make_filesystem(NULL);

		return RetVal;
	}

failed:
	format_in_progress = FALSE;
	enable_ui_ok_cancel(FALSE, TRUE);
	external_cmd.exit_status = -1;
	return FALSE;
}

/********************************************************************

PARTITION_MEDIA :


********************************************************************/

gboolean partition_media(gpointer client_data)
{
	if ((external_cmd.exit_status >> 8) && external_cmd.cmd_spec[external_cmd.ix - 1].cs_end_if_nonzero) {
		DEBUGPRINT("Non zero status, not continuing\n");
		goto failed;
	}

	if ((media_fd = rmedia_device_open_medium_handle(usable_device)) == NULL) {
		report_system_error(usable_device);
		return TRUE;
	}

	/* Now check the format status of the device */

	if (low_level_format_percent != 100) {
		/* not yet formatted */
		DEBUGPRINT("About to begin formatting RM_FORMAT_IMMEDIATE\n");
		if (rmedia_device_format(media_fd, format_mode, RM_FORMAT_IMMEDIATE) < 0) {
			external_cmd.exit_status = -1;
		} else {
			gtk_timeout_add(1000, (GtkFunction)update_format_bar,NULL);
			}
		return FALSE;
	} else {
		external_cmd.exit_status = 0;
		rmedia_device_close_medium_handle(media_fd);		
		make_partitions(NULL);
		return FALSE;
	
	}
	
failed:
	format_in_progress = FALSE;
	enable_ui_ok_cancel(FALSE, TRUE);
 	external_cmd.exit_status = -1;
	return FALSE;
}

/********************************************************************

UPDATE_FORMAT_BAR :

	
********************************************************************/
void update_format_bar(gpointer client_data)
{

	static int time_interval = 1000;	/* 1 sec */
	float percent_value = 0.0;

	if ((media_fd = rmedia_device_open_medium_handle(usable_device)) == NULL) {
		report_system_error(usable_device);
		return;
	}
	low_level_format_percent = rmedia_device_check_format_status(media_fd);
	rmedia_device_close_medium_handle(media_fd);

	/*
	 * Be slightly clever about the timer interval. If the format is
	 * slow, stretch the interval
	 */
	if (old_low_level_format_percent > low_level_format_percent) {
		old_low_level_format_percent = low_level_format_percent;	/* indicates starting format again,
					 * reset */
		time_interval = 1000;
	}
	else {
		if (old_low_level_format_percent > 0) {
			int diff = low_level_format_percent - old_low_level_format_percent;

			if (diff < MIN_PERCENT_INCR) {
				/*
				 * Less than MIN_PERCENT_INCR% increase since
				 * last timeout
				 */
				time_interval *= 2;
				if (time_interval < 500) {
					time_interval = 500;	/* Keep it above 1/2
								 * second */
				}
			}
			else if (diff > MAX_PERCENT_INCR) {	/* Its too high, shorten
								 * the interval */
				time_interval /= 2;
				if (time_interval > 10000) {
					time_interval = 10000;	/* Cap at 10 seconds */
				}
			}
		}
	}
	old_low_level_format_percent = low_level_format_percent;

	/* Since gtk_progress_bar_set_fraction  expects a value between 0.0 and 1.0, we divide
		low_level_format_percent by 100 */
	if(low_level_format_percent > 0) {
		percent_value = low_level_format_percent;
		percent_value /=100;
	}
	/*printf("percent_value = %f\n",percent_value); */
	gtk_progress_bar_set_fraction (GTK_PROGRESS_BAR(fApp->progress_bar), percent_value);

	if (low_level_format_percent < 100) {
		gtk_timeout_add(time_interval, (GtkFunction)update_format_bar, NULL);
	} else {
		external_cmd.exit_status = 0;
		make_filesystem(NULL);
	}
}



/********************************************************************

MAKE_FILESYSTEM:

	This function prepairs the media for a filesystem. For UFS
	and UDFS this simply involves performing an rmformat, and
	simulating and insert and eject. For PCFS we need an extra 
	step of placing an fdisk partition on the medium, followed
	by a simulated insert and eject.

********************************************************************/

static gboolean make_filesystem(gpointer client_data)
{

    char tmpstr2[MAXPATHLEN];

	INITCMDS(external_cmd);
	

		snprintf(tmpstr2, MAXPATHLEN, "/usr/bin/echo y | %s -F quick %s > /dev/null 2>&1", CMD_RMFORMAT, usable_device_alias);
		
		NEWCMD(external_cmd, "/bin/sh");
		AddArg(external_cmd, "-c");
		AddArg(external_cmd, tmpstr2);
		SetFollowFunc(external_cmd, resync_to_volmgt_path);
		SetStopOnFailure(external_cmd, TRUE);
		
		
		
		NEWCMD(external_cmd, CMD_VOLRMMOUNT);
		AddArg(external_cmd, "-i");
		AddArg(external_cmd, u_dev_alias);
		SetFollowFunc(external_cmd, resync_to_volmgt_path);
		SetStopOnFailure(external_cmd, TRUE);
		
		if (!strcmp(fstype.fs_type, FS_PCFS)) {
		    /* When creating the fdisk_file, we dont know the size of the media
		       so we need to calculate it here, 
		     */
		    int nc, info_found=0;
		    unsigned long ts;
		    
		    NEWCMD(external_cmd, CMD_VOLRMMOUNT);
		    AddArg(external_cmd, "-e");
		    AddArg(external_cmd, u_dev_alias);
		    SetFollowFunc(external_cmd, resync_to_volmgt_path);
		    SetStopOnFailure(external_cmd, TRUE);

		    if((info_found = get_cylinder_info(&nc, &ts)) == 0){
				show_modal_message(MSG_NOT_DETER_SIZE);
				log_error(ERR_OPEN_DEVICE);
				remove_files(FALSE);
				exit(-1);
			}
 		
		    fdisk_tmpfile = tmpnam(NULL);
		    if ((fdisk_file = fopen(fdisk_tmpfile, "w")) == NULL) {
			report_system_error("fopen");
			return TRUE;
		    }
		    fprintf(fdisk_file, "6:0:0:0:0:0:0:0:%d:%lu\n",
			nc, ts);
		    fclose(fdisk_file);
		

		    snprintf(tmpstr, MAXPATHLEN, "/usr/bin/echo y | %s -n -v -F %s %s > /dev/null 2>&1", 
		  		 CMD_FDISK, fdisk_tmpfile, u_dev_alias );

		  		 
				
		    NEWCMD(external_cmd, "/bin/sh");
		    AddArg(external_cmd, "-c");
		    AddArg(external_cmd, tmpstr);
		    SetFollowFunc(external_cmd, resync_to_volmgt_path);
		    SetStopOnFailure(external_cmd, TRUE);
		    
		    NEWCMD(external_cmd, CMD_VOLRMMOUNT);
		    AddArg(external_cmd, "-i");
		    AddArg(external_cmd, u_dev_alias);
		    SetFollowFunc(external_cmd, resync_to_volmgt_path);
		    SetStopOnFailure(external_cmd, TRUE);
		
		

	}
		
		NEWCMD(external_cmd, CMD_VOLRMMOUNT);
		AddArg(external_cmd, "-e");
		AddArg(external_cmd, u_dev_alias);	
		if(PARTITIONED)
			SetFollowFunc(external_cmd, make_partitions);
		else
			SetFollowFunc(external_cmd, continue_makeing_filesystem);		
		SetStopOnFailure(external_cmd, TRUE);

	BeginCommandSequence();
	
	return FALSE;

}



/********************************************************************

CONTINUE_MAKEING_FILESYSTEM:

	This contains the rest of the commands required to
	place a filesystem on the media. This is basically
	the MKFS and labelit commands followed by a 
	simulated insert.

********************************************************************/

static gboolean continue_makeing_filesystem(gpointer client_data)
{
	char *label_opt = NULL;	/* not same across all fs-types! */
	unsigned long media_size;

	
	
	

	/* Setup structures to call the correct series of commands */

	INITCMDS(external_cmd);
	

	

		 

	if (strcmp(fstype.fs_type, FS_PCFS)) {
		NEWCMD(external_cmd, CMD_RMFORMAT);
		AddArg(external_cmd, "-b");
		AddArg(external_cmd, vol_label ? vol_label : get_media_type_from_id(media_type));
		AddArg(external_cmd, usable_device_alias);
		SetFollowFunc(external_cmd, resync_to_volmgt_path);
		SetStopOnFailure(external_cmd, TRUE);
	}


		


	if( !strcmp(fstype.fs_type, FS_UDFS) || !strcmp(fstype.fs_type, FS_UFS) )
	{

	   /* Command: mkfs */
	   NEWCMD(external_cmd, CMD_MKFS);
	   AddArg(external_cmd, "-F");
	   AddArg(external_cmd, fstype.fs_type);
	   AddArg(external_cmd, usable_device_alias);
	   AddArg(external_cmd, media_size_str);
	   SetStopOnFailure(external_cmd, TRUE);
	   SetFollowFunc(external_cmd, resync_to_volmgt_path);

	}else {		/* using PCFS format */
	   char *quoted_label = NULL;

	   if (vol_label)
	      quoted_label = g_shell_quote(vol_label);
	
	   snprintf(tmpstr3, MAXPATHLEN, "/usr/bin/echo y | %s -F pcfs -o b=%s %s:c > /dev/null 2>&1", 
	   		CMD_MKFS, vol_label ? quoted_label : get_media_type_from_id(media_type), usable_device_alias);

	   g_free (quoted_label);
	
	   NEWCMD(external_cmd, "/bin/sh");
	   AddArg(external_cmd, "-c");
	   AddArg(external_cmd, tmpstr3);
	   SetFollowFunc(external_cmd, tidy_after_cmd);
	   SetStopOnFailure(external_cmd, TRUE);
	   
	  



	}
	
	if( !strcmp(fstype.fs_type, FS_UDFS) || !strcmp(fstype.fs_type, FS_UFS) ) {
	    NEWCMD(external_cmd, CMD_RMFORMAT);
	    AddArg(external_cmd, "-b");
	    AddArg(external_cmd, vol_label ? vol_label : get_media_type_from_id(media_type));
	    AddArg(external_cmd, usable_device_alias);
	    SetStopOnFailure(external_cmd, TRUE);
	    SetFollowFunc(external_cmd, tidy_after_cmd); /* definately dodgy */
	}

	/* Command: eject */
	NEWCMD(external_cmd, CMD_VOLRMMOUNT);
	AddArg(external_cmd, "-i");

	if (!strcmp(fstype.fs_type, FS_PCFS))
		AddArg(external_cmd, u_dev_alias);
	else
		AddArg(external_cmd, usable_device_alias);
	
	SetFollowFunc(external_cmd, tidy_after_cmd);
	SetStopOnFailure(external_cmd, TRUE);

	/*
	 * initiate first command, triggering subsequent commands when each
	 * has been succcesful
	 */
	 
	rmedia_device_close_medium_handle(media_fd);
	BeginCommandSequence();
	return FALSE;
}


void unmount_slices()
{
	
	
	INITCMDS(external_cmd);
	NEWCMD(external_cmd, CMD_MYVOLRMMOUNT);
	AddArg(external_cmd, "-e");
	AddArg(external_cmd, pc_device);
	SetFollowFunc(external_cmd, resync_to_volmgt_path);
	SetStopOnFailure(external_cmd, TRUE);
	BeginCommandSequence();
	
	


}

void remove_files(gboolean ismounted)
{
	pid_t pid;
	int res;
	char *env_init[] = {"USER=unknown", "PATH=/usr/bin", NULL};

    if(!ismounted)
    {
    	if( (pid = fork() ) < 0)
    	{
		log_error(ERR_REMOUNT);
	    exit(-1);
    	}
    
        if(pid == 0) {
	    	if(execle("/usr/bin/volrmmount",
				"volrmmount", "-i", usable_device_alias, (char *) 0, env_init) < 0)
	    	{
				log_error(ERR_REMOUNT);
				exit(-1);
	    	}
        }
    



        while ((waitpid(pid, &res, 0) < 0) && (errno == EINTR));
        
        
        if(WEXITSTATUS(res) != 0) {
       		fprintf(stderr, "status %d\n", res);
			log_error(ERR_REMOUNT);
	    	exit(-1);
        }
    }



	if(progress_warn != NULL)
		unlink(progress_warn);

	if(fdisk_tmpfile != NULL)
		unlink(fdisk_tmpfile);
		
	if(slice_file != NULL)
		unlink(slice_file);


}


 
/****************************************************************/
/*********		FORMAT  FLOPPY DEVICES		*********/
/****************************************************************/	






/***************************************************************

DO_FLOPPY : MERGE
	This function unmounts the medium, and sets the low
	level format to be performed when the unmount is 
	completed.
			
				
****************************************************************/


gboolean do_floppy()
{


	INITCMDS(external_cmd);
	
	NEWCMD(external_cmd, CMD_VOLRMMOUNT);
	AddArg(external_cmd, "-e");
	AddArg(external_cmd, usable_device_alias);
	SetFollowFunc(external_cmd, floppy_low_level_format);
	SetStopOnFailure(external_cmd, TRUE);

	
	BeginCommandSequence();
	return TRUE;

}




/***************************************************************

FLOPPY_LOW_LEVEL_FORMAT :

	Performs a low level format on the floppy. Floppies
	only support LONG formats, and the status bar will not
	be updated. On completion it calls the function to lay
	down the file system.
	
****************************************************************/
gboolean floppy_low_level_format()
{
	gboolean res = FALSE;
	int media_size, x, density;
	
	/* calculate media size, we can determine what DENSITY
	   of floppy we are dealing with */
	media_size = calculate_media_size();
	
	if(media_size == 0) { 	/* Error , exit program */
			show_modal_message(MSG_NOT_DETER_SIZE);
			log_error(ERR_OPEN_DEVICE);
 			remove_files(FALSE);
   	    	exit(-1);
	}
	switch(media_size) {
		case 1440 :
			density = RM_FORMAT_DD;	/* Double Density 720K*/
			break;
		case 2400 :
			density = RM_FORMAT_MD; /* Medium Density 1.2m */
			break;
		case 2880 :	
			density = RM_FORMAT_HD; /* High Density 1.44m */
			break;
		case 5760 :	
			density = RM_FORMAT_ED; /* Extended Density 2.88m */
			break;
		default :
			density = RM_FORMAT_HD;
			break;
		}
		

		
	if ((external_cmd.exit_status >> 8) && 
	     external_cmd.cmd_spec[external_cmd.ix - 1].cs_end_if_nonzero) {
		DEBUGPRINT("Non zero status, not continuing\n");
		goto failed;
	}

	/* Perform the low level format */

	if ((media_fd = rmedia_device_open_medium_handle(usable_device)) == NULL) {
		report_system_error(usable_device);
		return TRUE;
	}
	x = rmedia_device_format(media_fd, density, RM_FORMAT_BLOCKED);
	rmedia_device_close_medium_handle(media_fd);
	
	if (x < 0) {
		external_cmd.exit_status = -1;
	}else {
		res = format_floppy();
		return(res);
	}
failed:
	format_in_progress = FALSE;
	enable_ui_ok_cancel(FALSE, TRUE);
	external_cmd.exit_status = -1;
	return FALSE;
}





/***************************************************************

FORMAT_FLOPPY :
	Actually lays down the file system on the formatted floppy 
	and ejects it.
	
****************************************************************/

gboolean format_floppy()
{
	unsigned long drive_size;
	static char label_string[80];
	unsigned long med_size;
	char *label_opt = NULL;
	char *tmps;
	

	
	/* for NECDOS file system size is 1.2m  for every other
	   file system it depends on the medium size 
	*/
	   
	if (!strcmp(fstype.fs_type, FS_NECDOS))
	{
		med_size = 2400;
	} else {
		med_size = calculate_media_size();
	}
	if(med_size == 0) {  /* Error, exit */
		show_modal_message(MSG_NOT_DETER_SIZE);
		log_error(ERR_OPEN_DEVICE);
		remove_files(FALSE);
   	    exit(-1);
	}
	/* Setup structures to call the correct series of commands */

	INITCMDS(external_cmd);
	
	/* If the medium was previously formatted, it can appear mounted at 
	   this stage. Simply unmounting it remedies the situation. We set
	   the SetStopOnFailure flag to false, in case the media was still
	   unmounted.
	*/
	
 	NEWCMD(external_cmd, CMD_VOLRMMOUNT);
	AddArg(external_cmd, "-e");
	AddArg(external_cmd, usable_device_alias);
	SetFollowFunc(external_cmd, tidy_after_cmd);
	SetStopOnFailure(external_cmd, FALSE);
 	
	
	
	/* If we are not using a DOS type format, label the disk */ 
	
	if ( strcmp(fstype.fs_type, FS_PCFS) && strcmp(fstype.fs_type, FS_NECDOS) )
	{
		NEWCMD(external_cmd, CMD_RMFORMAT);
		AddArg(external_cmd, "-b");
		AddArg(external_cmd, vol_label ? vol_label : get_media_type_from_id(media_type));
		AddArg(external_cmd, usable_device_alias);
		SetFollowFunc(external_cmd, tidy_after_cmd);
		SetStopOnFailure(external_cmd, TRUE);	
	} else {
	
	/* otherwise set the label flag for DOS systems, and force
	   the label to be floppy if the user didnt enter a label
	*/
		label_opt = "b";
		if( vol_label == NULL) 
		{
			vol_label = g_malloc(strlen("floppy") +1);
			(void)strcpy(vol_label, "floppy");
			
		}
	}

	/* Construct the MKFS command */
	
	NEWCMD(external_cmd, CMD_MKFS);
	AddArg(external_cmd, "-F");

	if(!strcmp(fstype.fs_type, FS_NECDOS))
                AddArg(external_cmd, FS_PCFS);
        else
                AddArg(external_cmd, fstype.fs_type);


	if (!strcmp(fstype.fs_type, FS_PCFS))  {
		(void) snprintf(label_string, sizeof(label_string),
				"%s=%s", label_opt, vol_label);
		AddArg(external_cmd, "-o");
		AddArg(external_cmd, label_string);
	}	
		
	
	if (!strcmp(fstype.fs_type, FS_NECDOS))  { /* need additional parameters */
		(void) snprintf(label_string, sizeof(label_string),
				"%s=%s,nofdisk,size=%d", label_opt, vol_label, med_size);
		AddArg(external_cmd, "-o");
		AddArg(external_cmd, label_string);
	}			
	AddArg(external_cmd, usable_device_alias);
	
	if( !strcmp(fstype.fs_type, FS_UDFS) || !strcmp(fstype.fs_type, FS_UFS) ) {
	
		snprintf(media_size_str, sizeof(media_size_str), "%lu", med_size);
		AddArg(external_cmd, media_size_str);
	}
	
	SetFollowFunc(external_cmd, tidy_after_cmd);
	SetStopOnFailure(external_cmd, TRUE);
	
	

	/* Command: eject */
	NEWCMD(external_cmd, CMD_VOLRMMOUNT);
	AddArg(external_cmd, "-i");
	AddArg(external_cmd, usable_device_alias);
	SetFollowFunc(external_cmd, tidy_after_cmd);
	SetStopOnFailure(external_cmd, TRUE);

	/*
	 * initiate first command, triggering subsequent commands when each
	 * has been succcesful
	 */
	
	
	BeginCommandSequence();
	return FALSE;
}

static gboolean tidy_after_slice(gpointer client_data)
{
 	int continue_commands = TRUE;

	if ((external_cmd.exit_status >> 8) && external_cmd.cmd_spec[external_cmd.ix].cs_end_if_nonzero) {
		continue_commands = FALSE;
		DEBUGPRINT("Non zero status, not continuing\n");
		format_in_progress = FALSE;
		enable_ui_ok_cancel(FALSE, TRUE);
	}

        if ( continue_commands ) {
		format_in_progress = FALSE;
		enable_ui_ok_cancel(TRUE, TRUE);
		enable_ui_low_format(TRUE);
		enable_ui_filesystem(TRUE);
		enable_ui_vol_name_entry(TRUE);
	}

	enable_ui_slice_button(TRUE);
	format_set_focused_widget();

        return FALSE;		/* remove the work procedure from the list */
}


void launch_slice_editor()
{
	char *CMD_SLICE_ED;

	INITCMDS(external_cmd);
	CMD_SLICE_ED = g_find_program_in_path("gmedia_slice");
	NEWCMD(external_cmd, CMD_SLICE_ED);
	AddArg(external_cmd, "-d");
	AddArg(external_cmd, usable_device);
	AddArg(external_cmd, "-W");
	AddArg(external_cmd, slice_file);
	SetFollowFunc(external_cmd, tidy_after_slice);
	
	SetStopOnFailure(external_cmd, TRUE);

	BeginCommandSequence();
}
