/*
 * Copyright 2002 Sun Microsystems Inc.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Street #330, Boston, MA 02111-1307, USA.
 *
 */

#ifdef HAVE_CONFIG_H
#  include <config.h>
#endif
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <signal.h>
#include <sys/errno.h>
#include <errno.h>
#include "gcommand.h"
#include "format_utils.h"
#include "gutils_fmt.h"

static pid_t *childpid = NULL;
static int maxfd;
static FILE *fp;
static int sig_fds[2];

static gboolean process_done(GIOChannel *source, GIOCondition condition, gpointer data);
static void sigchild_handler(int signo);
static gboolean ProcessCmdOutput();
static void debug_external_cmd();

gint result;

/********************************************************************

ExecuteCommand :
	Executes the given command, passing the given arguments, 
	returning the process id or -1 if failed.

********************************************************************/

 
int ExecuteCommand(
		       char *command,
		       char *arguments[])
{

	pid_t pid;
	int pipe_fd;
	int sig_pipe_fd;
	int fds[2];
	int i;
	GIOChannel *sig_gio_ch, *sig_xid;

	if (childpid == NULL) {	/* First time through */
		/* Work out how many open file descriptors can there be */
		if ((maxfd = sysconf(_SC_OPEN_MAX)) < 0) {
			if (errno == 0)
				maxfd = 0;	/* Don't try and close
						 * anything */
			else {
				report_system_error("_SC_OPEN_MAX");
				return 0;
			}
		}
		if ((childpid = calloc(maxfd, sizeof(pid_t))) == NULL)
			return 0;

                /* Setup a handler for the SIGCHLD */

				if((sig_pipe_fd = pipe(sig_fds)) == 0) {
					sig_gio_ch = g_io_channel_unix_new(sig_fds[0]);
					g_io_add_watch(sig_gio_ch, G_IO_IN, process_done, NULL);
				}
                (void) sigset(SIGCHLD, sigchild_handler );
	}


	/*
	 * We want to exec a command to accomplish a task.
	 * 
	 * From "Advanced Programming in the UNIX environment", Stevens:
	 * 
	 * Before calling fork we create a pipe. After the fork the parent
	 * closes its read end and the child closes its write end. The child
	 * then calls dup2 to have its standard input be the read end of the
	 * pipe. When the spawned program is executed its standard input will
	 * be the read end of the pipe. fork Note, that this is essentially
	 * popen(), so it might be possible to replace it in the future. Its
	 * done like this for the moment to allow inclusing of GUI specific
	 * features (e.g. work procs etc).
	 * 
	 */

	DEBUG_CMD_SEQUENCE();

	if ((pipe_fd = pipe(fds)) == 0) {
		pid = fork();
		if (pid < 0) {
			report_system_error("pipe");
			return 0;
		}
		else if (pid > 0) {	/* parent */
			close(fds[1]);	/* Close write end of pipe */
			if ((fp = fdopen(fds[0], "r")) == NULL) {
				report_system_error("fdopen");
				return 0;
			}

			/*
			 * Setup a callback for when output happens on the
			 * pipe
			 */
			sig_xid = g_io_channel_unix_new (fileno(fp));
			result = g_io_add_watch (sig_xid, G_IO_IN, ProcessCmdOutput, NULL);
			g_io_channel_unref (sig_xid);
		}
		else {		/* child */
			close(fds[0]);	/* Close read end of pipe */
			if (fds[1] != STDOUT_FILENO) {
				dup2(fds[1], STDOUT_FILENO);
				close(fds[1]);	/* Don't need this after dup2 */
				close(STDIN_FILENO);

				/* Close all descriptors in childpid[] */
				for (i = 0; i < maxfd; i++) {
					if (childpid[i] > 0)
						close(i);
				}

				DEBUGPRINT("command");
				for (i = 0; arguments[i]; i++) {
					DEBUGPRINT(arguments[i]);
				}	
				if (execv(command, arguments) < 0) {
					show_modal_message(MSG_CMD_FAIL); 

					report_system_error("execv");
					remove_files(FALSE);
					exit(1);
				}
			}
		}
	}
	else {
		report_system_error("pipe");
		return 0;
	}
	return pid;
}

/********************************************************************

EXECUTECURRENTCOMMAND :

********************************************************************/
void ExecuteCurrentCommand()
{
	int n = external_cmd.cmd_spec[external_cmd.ix].cs_nargs;
	if (n < 0)
		n = 0;

/*	while (access(usable_device, 0) != 0)
		recalc_devicename();	/* hack */

	external_cmd.cmd_spec[external_cmd.ix].cs_arguments[n] = NULL;
	external_cmd.pid = ExecuteCommand(
			  external_cmd.cmd_spec[external_cmd.ix].cs_command,
		       external_cmd.cmd_spec[external_cmd.ix].cs_arguments);
}

/********************************************************************

PROCESS_DONE :


********************************************************************/

static void dummy_timer(gpointer client_data)
{
    return;
}

/********************************************************************

BEGINCOMMANDSEQUENCE :

	Sets the current command to be the first command in the sequence,
	and executes the current command.

********************************************************************/
void BeginCommandSequence()
{
	external_cmd.ix = 0;
	ExecuteCurrentCommand();
}

static gboolean process_done(GIOChannel *source, GIOCondition condition, gpointer data) 
{

        char dummy[1024];
	int status;

	if (result) {
		g_source_remove (result);
		result = 0;
	}

        read( sig_fds[0], dummy, 1024 );
 
        while ((waitpid(external_cmd.pid, &status, 0) == -1)  && (errno == EINTR) ){
                /* Error of some kind. Ignore it */
        }
	if (errno != ECHILD) {
		external_cmd.exit_status = status;
		gtk_idle_add(external_cmd.cmd_spec[external_cmd.ix].cs_func , NULL);
	}
	return TRUE;
}

static void sigchild_handler(int signo)
{
    if ( signo == SIGCHLD ) {
		write(sig_fds[1], (const void *)"asdf", (size_t)strlen("asdf"));
    }
}

/********************************************************************

ProcessCmdOutput :


********************************************************************/
static gboolean ProcessCmdOutput( GIOChannel *source, GIOCondition condition, gpointer data)
{
	char output[MAXLINE];
	int nbytes;
	int i;
	int fd;

	fd = g_io_channel_unix_get_fd (source);

	if ((nbytes = read(fd, output, MAXLINE)) == -1) {
		report_system_error("ProcessCmdOutput");
	}
#ifdef DEBUG
	else {
		if (nbytes > 0) {
			for (i = 0; i < nbytes; i++)
				putchar(output[i]);
			fflush(stdout);
		}else {
		}
	}
#endif
	return TRUE;
}



/********************************************************************

DEBUG_EXTERNAL_CMD :
	Only used when DEBUG is switched on. It prints the command
	we are trying to exec.

********************************************************************/

#ifdef DEBUG
static void debug_external_cmd()
{

	int i;

	for (i = 0; i < 10; i++) {
		if (external_cmd.cmd_spec[external_cmd.ix].cs_arguments[i] == NULL)
			break;
		else {
			printf("%s ", external_cmd.cmd_spec[external_cmd.ix].cs_arguments[i]);
		}
	}
	printf("\n");
	fflush(stdout);
}
#endif				/* DEBUG */

