/*
 * Copyright 2002 Sun Microsystems Inc.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Street #330, Boston, MA 02111-1307, USA.
 *
 */

/**************************************************

This header file was created to share functionality between
sdtmedia_format and sdtmedia_slice. 


**************************************************/
#include <gnome.h>

#define MAXLINE  1024
#define MAXPATHLEN  1024
#define MIN_PERCENT_INCR 2
#define MAX_PERCENT_INCR 6

#ifdef DEBUG
#define DEBUG_CMD_SEQUENCE() debug_external_cmd()
#else
#define DEBUG_CMD_SEQUENCE()
#endif


/* the character between elements of a directory path */
#define PATH_SEP '/'


/**********************************************************************************

	External FUNCTIONS that need to be visible to either
	sdtmedia_format, sdtmedia_slice or both
	
**********************************************************************************/
extern void unmount_and_partition(
	gboolean (*follow_func)());			/* unmount device and start to partition it */	       
extern void unmount_and_do(gboolean (*follow_func)());	/* unmount device and start to format it */
extern gboolean format_media(gpointer client_data);	/* issue format commands */
extern void update_format_bar(
	gpointer client_data); 	/* update the low level format status bar */
extern gboolean partition_media(gpointer client_data); 	/* apply the partitioning commands */
extern gboolean do_floppy(); 					/* format floppy disks */
extern void remove_files(gboolean ismounted); 				/* tidy up tmp files and remount media */

void launch_slice_editor(void);
void make_sliced_filesystems(void);
