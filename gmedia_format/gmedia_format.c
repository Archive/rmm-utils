/*
 * Copyright 2002 Sun Microsystems Inc.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Street #330, Boston, MA 02111-1307, USA.
 *
 */

#ifdef HAVE_CONFIG_H
#  include <config.h>
#endif

#include <locale.h>
#include <gnome.h>
#include <libgnomeui/gnome-about.h>
#include <stdio.h>
#include <string.h>
#include "gmedia_format.h"
#include "gformat_stubs.h"

void			make_gui();
static void		set_relation(GtkWidget *widget, GtkLabel *label);
gboolean		delete_cb(GtkDialog *dialog, GdkEventAny *event, gpointer data);
static GtkWidget*	rmm_dialog_add_buttons (GtkDialog *dialog, const gchar* text,
						const gchar* stock_id, gint response_id);


int main(int argc, char *argv[])
{

	int             status;
	static char     *mountpointarg, *devpatharg = NULL;
	char            *title;
	poptContext     ctx;
	struct poptOption options[] = {
		{
			NULL,
			'd',
			POPT_ARG_STRING,
			&devpatharg,
			0,
			"Raw device path",
			"PATH"

		}, {
			NULL,
			'M',
			POPT_ARG_STRING,
			&mountpointarg,
			0,
			"Mount point",
			"PATH"
		}, {
			NULL,
			0,
			0,
			NULL,
			0,
			NULL,
			NULL
		}
	};

	setlocale (LC_ALL, "");
#ifdef ENABLE_NLS
	bindtextdomain (GETTEXT_PACKAGE, GETTEXT_LOCALEDIR);
	bind_textdomain_codeset (GETTEXT_PACKAGE, "UTF-8");
	textdomain (GETTEXT_PACKAGE);
#endif

	gnome_program_init ("gmedia_format", VERSION,
			 LIBGNOMEUI_MODULE,
			 argc, argv,
			 GNOME_PARAM_POPT_TABLE, options,
			 0, &ctx,
			 NULL);

	status = parse_args(argc, argv);

	if (status != ERR_OK) {
		g_print(_("Usage error"));
		exit(1);
	}
	fApp = (FormatApp *) g_malloc(sizeof(FormatApp));

	/* Copying the device name to a variable which can be used Later */ 
	device_name = g_strdup (argv[2]);

	title = g_malloc (strlen ("Gnome Media Format - ") + strlen (argv[2]) + 1);
	strcpy (title, "Gnome Media Format - ");
	title = strcat (title, (const char *)argv[2]);
	format_in_progress = FALSE;

	fApp->dlg = gtk_dialog_new_with_buttons(title,
						 NULL,
						 GTK_DIALOG_NO_SEPARATOR, 
						 NULL); 

	g_signal_connect(G_OBJECT(fApp->dlg), "delete_event",
			   G_CALLBACK(delete_cb), NULL);

	make_gui();
	init_ui_values();

	gtk_main();
	free(title);

	return 0;
}


void make_gui()
{
	GtkTooltips *tooltips;

	/*
	 * Try setting a table as the main container for all the seperate
	 * parts
	 */
	fApp->table = gtk_table_new(5, 1, FALSE);

	gtk_box_pack_start(GTK_BOX(GTK_DIALOG (fApp->dlg)->vbox), fApp->table,
		 FALSE, FALSE,0);

	/*
	 * Define the label for the warning and attach to the table what are
	 * the 5's for??
	 */
	fApp->intro_label = gtk_label_new(_("Formatting is a two stage process involving a Low-level Format which prepares the medium for a file system, and then construction of the file system.\n\nWARNING:  THIS PROCESS IS NOT REVERSIBLE."));

	gtk_label_set_line_wrap (GTK_LABEL (fApp->intro_label), TRUE);
	gtk_table_attach(GTK_TABLE(fApp->table), fApp->intro_label, 0, 1, 0, 1,
		       GTK_FILL | GTK_EXPAND | GTK_SHRINK, GTK_FILL, 5, 20);


	fApp->vbox = gtk_vbox_new(FALSE, 10);	/* check params */
	fApp->vbox1 = gtk_vbox_new(FALSE, 10);	/* check what the parameters*/
	fApp->table1 = gtk_table_new(5, 7, FALSE);


	
	fApp->lowlevel_label = gtk_label_new(_("Low level formatting"));
	gtk_label_set_justify(GTK_LABEL(fApp->lowlevel_label), GTK_JUSTIFY_LEFT);
	gtk_table_attach(GTK_TABLE(fApp->table1), fApp->lowlevel_label, 1, 2, 0, 1,
			 GTK_FILL | GTK_EXPAND | GTK_SHRINK, GTK_FILL, 5, 10);

	fApp->quick_radio_button = gtk_radio_button_new_with_mnemonic(NULL, _("_Quick Format"));

	gtk_table_attach(GTK_TABLE(fApp->table1), fApp->quick_radio_button, 1, 2, 1, 2,
			 GTK_FILL | GTK_EXPAND | GTK_SHRINK, GTK_FILL, 5, 10);

	g_signal_connect(G_OBJECT(fApp->quick_radio_button), "toggled",
			   G_CALLBACK(quick_radio_cb), NULL);

	fApp->long_radio_button = gtk_radio_button_new_with_mnemonic(gtk_radio_button_get_group(GTK_RADIO_BUTTON(fApp->quick_radio_button)), _("_Long Format"));

	gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(fApp->quick_radio_button) , TRUE);
	gtk_table_attach(GTK_TABLE(fApp->table1), fApp->long_radio_button, 1, 2, 2, 3,
			 GTK_FILL | GTK_EXPAND | GTK_SHRINK, GTK_FILL, 5, 10);
	g_signal_connect(G_OBJECT(fApp->long_radio_button), "toggled",
			 G_CALLBACK(long_radio_cb), NULL);



	/*
	 * define a vbox to put the label, button and text area into then add
	 * vbox to table
	 */

	fApp->fs_label = gtk_label_new(_("Construct File System"));
	gtk_table_attach(GTK_TABLE(fApp->table1), fApp->fs_label, 4, 5, 0, 1,
			 GTK_FILL | GTK_EXPAND | GTK_SHRINK, GTK_FILL, 5, 10);
	
	fApp->separator1 = gtk_vseparator_new();
	gtk_table_attach(GTK_TABLE(fApp->table1), fApp->separator1, 0, 1, 0, 5,
			 GTK_FILL | GTK_EXPAND | GTK_SHRINK, GTK_FILL, 5, 10);

	fApp->separator2 = gtk_vseparator_new();
	gtk_table_attach(GTK_TABLE(fApp->table1), fApp->separator2, 2, 3, 0, 5,
			 GTK_FILL | GTK_EXPAND | GTK_SHRINK, GTK_FILL, 5, 10);

	fApp->separator3 = gtk_vseparator_new();
	gtk_table_attach(GTK_TABLE(fApp->table1), fApp->separator3, 3, 4, 0, 5,
			 GTK_FILL | GTK_EXPAND | GTK_SHRINK, GTK_FILL, 5, 10);

	fApp->separator4 = gtk_vseparator_new();
	gtk_table_attach(GTK_TABLE(fApp->table1), fApp->separator4, 6, 7, 0, 5,
			 GTK_FILL | GTK_EXPAND | GTK_SHRINK, GTK_FILL, 5, 10);

	fApp->separator5 = gtk_hseparator_new();
	gtk_table_attach(GTK_TABLE(fApp->table1), fApp->separator5, 0, 3, 4, 5,
			 GTK_EXPAND | GTK_FILL | GTK_SHRINK, GTK_FILL, 5, 10);

	fApp->separator6 = gtk_hseparator_new();
	gtk_table_attach(GTK_TABLE(fApp->table1), fApp->separator6, 3, 7, 4, 5,
			 GTK_FILL | GTK_EXPAND | GTK_SHRINK, GTK_FILL, 5, 10);

	fApp->separator7 = gtk_hseparator_new();
	gtk_table_attach(GTK_TABLE(fApp->table1), fApp->separator7, 5, 7, 0, 1,
			 GTK_FILL | GTK_EXPAND | GTK_SHRINK, GTK_FILL, 5, 10);


	fApp->fs_type_label = gtk_label_new_with_mnemonic(_("_File System Type:"));
	gtk_table_attach(GTK_TABLE(fApp->table1), fApp->fs_type_label, 4, 5, 1, 2,
			 GTK_FILL | GTK_EXPAND | GTK_SHRINK, GTK_FILL, 5, 10);

	/*
	 * Create the option menu here, but create the menu items dynamically
	 * when init_ui_values calls init_fs_buttons
	 */

	fApp->option_menu = gtk_option_menu_new();
	gtk_table_attach(GTK_TABLE(fApp->table1), fApp->option_menu, 5, 6, 1, 2,
			 GTK_FILL | GTK_EXPAND | GTK_SHRINK, GTK_FILL, 5, 10);

	set_relation(fApp->option_menu, GTK_LABEL(fApp->fs_type_label));

	fApp->volume_label = gtk_label_new_with_mnemonic(_("_Volume Label:"));
	gtk_table_attach(GTK_TABLE(fApp->table1), fApp->volume_label, 4, 5, 2, 3,
			 GTK_FILL | GTK_EXPAND | GTK_SHRINK, GTK_FILL, 5, 10);

	/*
	 * Create the option menu here, but create the menu items dynamically
	 * when init_ui_values calls init_fs_buttons
	 */

	fApp->entry = gtk_entry_new();
	/*gtk_entry_set_max_length(GTK_ENTRY(fApp->entry),50);*/
	gtk_entry_set_max_length(GTK_ENTRY(fApp->entry),11);
	gtk_table_attach(GTK_TABLE(fApp->table1), fApp->entry, 5, 6, 2, 3,
			 GTK_FILL | GTK_EXPAND | GTK_SHRINK, GTK_FILL, 5, 10);

	set_relation(fApp->entry, GTK_LABEL(fApp->volume_label));

	gtk_box_pack_start(GTK_BOX(fApp->vbox1), fApp->table1, FALSE, FALSE, 0);

	fApp->slice_button = gtk_button_new_with_mnemonic(_("Slice _Editor..."));
	gtk_table_attach(GTK_TABLE(fApp->table1), fApp->slice_button, 5, 6, 3, 4,
				GTK_FILL | GTK_EXPAND | GTK_SHRINK, GTK_FILL, 5, 10);
	g_signal_connect(G_OBJECT(fApp->slice_button), "clicked",
			   G_CALLBACK(slice_cb), NULL);


	gtk_table_attach(GTK_TABLE(fApp->table), fApp->vbox1, 0, 1, 1, 2,
		       GTK_FILL | GTK_EXPAND | GTK_SHRINK, GTK_FILL, 5, 10);

  	gtk_button_box_set_layout (GTK_BUTTON_BOX 
		(GTK_DIALOG(fApp->dlg)->action_area), GTK_BUTTONBOX_SPREAD);

	/*
	 *   Adding Help, Format, Cancel buttons 
 	*/

	fApp->help_button =  gtk_dialog_add_button(GTK_DIALOG (fApp->dlg), GTK_STOCK_HELP, GTK_RESPONSE_YES);
	gtk_widget_ref (fApp->help_button);
	g_object_set_data_full (G_OBJECT(fApp->dlg),"fApp->help_button",
	fApp->help_button , (GtkDestroyNotify) gtk_widget_unref);

	fApp->cancel_button = rmm_dialog_add_buttons(GTK_DIALOG (fApp->dlg),_("_Cancel"),
						     GTK_STOCK_CANCEL, GTK_RESPONSE_CANCEL);
	gtk_widget_ref (fApp->cancel_button);
	g_object_set_data_full (G_OBJECT(fApp->dlg),"fApp->cancel_button",
				fApp->cancel_button , (GtkDestroyNotify) gtk_widget_unref);

	fApp->format_button =rmm_dialog_add_buttons(GTK_DIALOG (fApp->dlg),_("_Format"),
						    GTK_STOCK_APPLY,GTK_RESPONSE_APPLY);
	gtk_widget_ref (fApp->format_button);
	g_object_set_data_full (G_OBJECT(fApp->dlg),"fApp->format_button",
				fApp->format_button , (GtkDestroyNotify) gtk_widget_unref);
	gtk_dialog_set_default_response (GTK_DIALOG(fApp->dlg), GTK_RESPONSE_HELP);

	tooltips = gtk_tooltips_new();
	gtk_tooltips_set_tip(tooltips, fApp->format_button,
				_("Press button to format"), NULL);


	g_signal_connect(GTK_BUTTON(fApp->cancel_button),"clicked",G_CALLBACK(button_delete_cb),fApp->dlg);
	g_signal_connect(GTK_BUTTON(fApp->help_button),"clicked",G_CALLBACK(help_cb),fApp->dlg);
	g_signal_connect(GTK_BUTTON(fApp->format_button),"clicked",G_CALLBACK(format_cb),GTK_ENTRY(fApp->entry));


	gtk_widget_show_all(fApp->dlg);

	gtk_widget_set_sensitive(GTK_WIDGET(fApp->slice_button), 0);


}

void init_fs_buttons(FileSystem * fs_list, int ntypes)
{

	int             i;

	fApp->menu = gtk_menu_new();
	fApp->fs_buttons = (GtkWidget **) g_malloc(sizeof(GtkWidget *) * ntypes);

	fstype = fs_list[0];
	for (i = 0; i < ntypes; i++) {
		fApp->fs_buttons[i] = gtk_menu_item_new_with_label(fs_list[i].fs_name);
		gtk_menu_shell_append(GTK_MENU_SHELL(fApp->menu), fApp->fs_buttons[i]);

		g_signal_connect(G_OBJECT(fApp->fs_buttons[i]), "activate",
		    G_CALLBACK(fs_button_cb), (gpointer) & fs_list[i]);
		if(fs_list[i].formattable == 0)
			gtk_widget_set_sensitive(GTK_WIDGET(fApp->fs_buttons[i]), 0);
		gtk_widget_show(fApp->fs_buttons[i]);
	}
	gtk_option_menu_set_menu(GTK_OPTION_MENU(fApp->option_menu), fApp->menu);

}



void toggle_ui_long_format(int enabled)
{

	gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(fApp->long_radio_button), !enabled);

}

void enable_ui_low_format(int enabled)
{

	gtk_widget_set_sensitive(GTK_WIDGET(fApp->long_radio_button), enabled);
	gtk_widget_set_sensitive(GTK_WIDGET(fApp->quick_radio_button), enabled);
}

void enable_ui_ok_cancel(int ok_state, int cancel_state)
{
	GtkWidget *label;
	GtkStockItem item;
	gchar* text;
	gchar* stock_id;
	GtkWidget *image;
	GtkWidget *hbox;
	GtkWidget *align;

	gtk_widget_set_sensitive(GTK_WIDGET(fApp->format_button), ok_state);

	if (format_in_progress)
	{
		/* During formatting a  floppy the Cancel button will be 
		 * disabled. So we will not change  the lebel to "Stop" if 
		 * the media type is floppy.
		 */
		if ( media_type == RM_FLOPPY )
		{
			/* Keep  Cancel button as it is*/
			text = _("_Cancel");
			stock_id = GTK_STOCK_CANCEL;
		}
		else
		{
			/* Change Cancel button to  Stop*/
			text = _("_Stop");
			stock_id = GTK_STOCK_STOP;
		}
	}
	else
	{
		/* Change Cancel button to  Close*/
		text = _("_Close");
		stock_id = GTK_STOCK_CLOSE;
	}

	if (GTK_BIN (fApp->cancel_button)->child)
		gtk_container_remove (GTK_CONTAINER (fApp->cancel_button),
				      GTK_BIN (fApp->cancel_button)->child);

	if (gtk_stock_lookup (stock_id, &item)) {
		label = gtk_label_new_with_mnemonic (text);
		gtk_label_set_mnemonic_widget (GTK_LABEL (label),
					       GTK_WIDGET (fApp->cancel_button));

		image = gtk_image_new_from_stock (stock_id, GTK_ICON_SIZE_BUTTON);
		hbox = gtk_hbox_new (FALSE, 2);
		align = gtk_alignment_new (0.5, 0.5, 0.0, 0.0);
		gtk_box_pack_start (GTK_BOX (hbox), image, FALSE, FALSE, 0);
		gtk_box_pack_end (GTK_BOX (hbox), label, FALSE, FALSE, 0);
		gtk_container_add (GTK_CONTAINER (fApp->cancel_button), align);
		gtk_container_add (GTK_CONTAINER (align), hbox);
		gtk_widget_show_all (align);
	}
	else
	{
		label = gtk_label_new_with_mnemonic (text);
		gtk_label_set_mnemonic_widget (GTK_LABEL (label),
					       GTK_WIDGET(fApp->cancel_button));
		gtk_misc_set_alignment (GTK_MISC (label), 0.5, 0.5);
		gtk_widget_show (label);
		gtk_container_add (GTK_CONTAINER (fApp->cancel_button), label);
	}
	/* Floppy format being a blocking call the User will get
	 * the Warning message after the format is over. So disbale
	 * the "Stop" button ,  if it is floppy.
	 */

	if (media_type == RM_FLOPPY && format_in_progress )
	{
		gtk_widget_set_sensitive(GTK_WIDGET(fApp->cancel_button),FALSE);
	}
	else
	{
		gtk_widget_set_sensitive(GTK_WIDGET(fApp->cancel_button),TRUE);
	}
}

void enable_ui_slice_button(int enabled)
{

	gtk_widget_set_sensitive(GTK_WIDGET(fApp->slice_button), enabled);

}

void enable_ui_options(int enabled)
{

	gtk_widget_set_sensitive(GTK_WIDGET(fApp->option_menu), enabled);
}

void enable_ui_filesystem(int enabled)
{

	gtk_widget_set_sensitive(GTK_WIDGET(fApp->option_menu), enabled);
	gtk_widget_set_sensitive(GTK_WIDGET(fApp->menu), enabled);
	


}

void enable_ui_vol_name_entry (int enabled)
{
	gtk_widget_set_sensitive(GTK_WIDGET(fApp->entry), enabled);
}

void format_get_focused_widget ()
{
	fApp->focused_widget = gtk_window_get_focus (GTK_WINDOW (fApp->dlg));
}

void format_set_focused_widget ()
{
	gtk_window_set_focus (GTK_WINDOW (fApp->dlg), fApp->focused_widget);
}

/**
 * set_relation
 * @widget : The Gtk widget which is labelled by @label
 * @label : The label for the @widget.
 * Description : This function establishes atk relation
 * between a gtk widget and a label.
 */
static void set_relation(GtkWidget *widget, GtkLabel *label)
{
	AtkObject *aobject;
	AtkRelationSet *relation_set;
	AtkRelation *relation;
	AtkObject *targets[1];
 
	g_return_if_fail(GTK_IS_WIDGET(widget));
	g_return_if_fail(GTK_IS_LABEL(label));
 
	aobject = gtk_widget_get_accessible(widget);
 
	/* Set the ATK_RELATION_LABEL_FOR relation */
	gtk_label_set_mnemonic_widget(label, widget);

	/* Return if GAIL is not loaded */
	if (! GTK_IS_ACCESSIBLE(aobject))
		return;
 
	targets[0] = gtk_widget_get_accessible(GTK_WIDGET(label));
 
	relation_set = atk_object_ref_relation_set(aobject);
 
	relation = atk_relation_new(targets, 1, ATK_RELATION_LABELLED_BY);
	atk_relation_set_add(relation_set, relation);
	g_object_unref(G_OBJECT(relation));
}

void create_progress_bar()
{

 	fApp->progress_bar = gtk_progress_bar_new();


	fApp->progress_bar_label = gtk_label_new(_("Low Level Format Progress"));
	gtk_table_attach(GTK_TABLE(fApp->table), fApp->progress_bar_label, 0, 1, 2, 3,
			GTK_FILL | GTK_EXPAND | GTK_SHRINK, GTK_SHRINK, 0, 0);

	gtk_table_attach(GTK_TABLE(fApp->table), fApp->progress_bar, 0, 1, 3, 4,
			GTK_FILL | GTK_EXPAND | GTK_SHRINK, GTK_FILL, 5, 20);

	gtk_progress_set_show_text (GTK_PROGRESS_BAR (fApp->progress_bar), TRUE);
	gtk_widget_show (fApp->progress_bar_label);
	gtk_widget_show (fApp->progress_bar);
}

static GtkWidget*
rmm_button_new_with_stock_image (const gchar* text, const gchar* stock_id)
{
	GtkWidget *button;
	GtkStockItem item;
	GtkWidget *label;
	GtkWidget *image;
	GtkWidget *hbox;
	GtkWidget *align;

	button = gtk_button_new ();

	if (GTK_BIN (button)->child)
	gtk_container_remove (GTK_CONTAINER (button),
			      GTK_BIN (button)->child);

	if (gtk_stock_lookup (stock_id, &item)) {
		label = gtk_label_new_with_mnemonic (text);
		gtk_label_set_mnemonic_widget (GTK_LABEL (label), GTK_WIDGET (button));
		image = gtk_image_new_from_stock (stock_id, GTK_ICON_SIZE_BUTTON);
		hbox = gtk_hbox_new (FALSE, 2);
		align = gtk_alignment_new (0.5, 0.5, 0.0, 0.0);

		gtk_box_pack_start (GTK_BOX (hbox), image, FALSE, FALSE, 0);
		gtk_box_pack_end (GTK_BOX (hbox), label, FALSE, FALSE, 0);
		gtk_container_add (GTK_CONTAINER (button), align);
		gtk_container_add (GTK_CONTAINER (align), hbox);
		gtk_widget_show_all (align);

		return button;
	}

	label = gtk_label_new_with_mnemonic (text);
	gtk_label_set_mnemonic_widget (GTK_LABEL (label), GTK_WIDGET (button));
	gtk_misc_set_alignment (GTK_MISC (label), 0.5, 0.5);
	gtk_widget_show (label);
	gtk_container_add (GTK_CONTAINER (button), label);
	return button;
}


static GtkWidget*
rmm_dialog_add_buttons (GtkDialog *dialog, const gchar* text, const gchar* stock_id,
			gint response_id)

{
	GtkWidget *button;

	g_return_val_if_fail (GTK_IS_DIALOG (dialog), NULL);
	g_return_val_if_fail (text != NULL, NULL);
	g_return_val_if_fail (stock_id != NULL, NULL);
	button = rmm_button_new_with_stock_image (text, stock_id);
	g_return_val_if_fail (button != NULL, NULL);
	GTK_WIDGET_SET_FLAGS (button, GTK_CAN_DEFAULT);
	gtk_widget_show (button);
	gtk_dialog_add_action_widget (dialog, button, response_id);
	return button;
}


static
gboolean show_message(GtkWindow *window)
{
	GtkWidget *dialog;
	dialog = gtk_message_dialog_new(GTK_WINDOW(window),
					GTK_DIALOG_MODAL,
					GTK_MESSAGE_QUESTION,
					GTK_BUTTONS_NONE,
					_("Formatting is in progress. Stopping the format now may make the disk unusable. Do you really want to stop formatting?"));

	rmm_dialog_add_buttons (GTK_DIALOG (dialog),
				_("_Quit Format"),
				GTK_STOCK_STOP,
				GTK_RESPONSE_YES);

	rmm_dialog_add_buttons (GTK_DIALOG (dialog),
				_("Co_ntinue Format"),
				GTK_STOCK_YES,
				GTK_RESPONSE_NO);

	gtk_dialog_set_default_response (GTK_DIALOG (dialog), GTK_RESPONSE_NO);
	switch (gtk_dialog_run (GTK_DIALOG (dialog))) {

		case GTK_RESPONSE_YES:
		gtk_widget_destroy (dialog);
		gtk_main_quit();
		break;

		case GTK_RESPONSE_NO:
		default:
		gtk_widget_destroy (dialog);
		return TRUE;
		break;
	}
}


gboolean delete_cb(GtkDialog *w, GdkEventAny *e, gpointer data)
{
	if (format_in_progress) {

		/* Floppy format being a blocking call the User will get
		 * the Warning message after the format is over. So return
		 * from here if it is floppy.
		 */
		if (media_type == RM_FLOPPY) {
			return TRUE;
		}
		else {
			show_message(GTK_WINDOW(w));
		}
	}
	else {
		/* format is over */
		gtk_main_quit();
	}
}

gboolean button_delete_cb(GtkWidget *widget, gpointer data)
{
	if (format_in_progress) {
		show_message(GTK_WINDOW(data));
	}
	else {
		/* format is over */
		gtk_main_quit();
	}
}
