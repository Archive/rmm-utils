/*
 * Copyright 2002 Sun Microsystems Inc.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Street #330, Boston, MA 02111-1307, USA.
 *
 */

#include <gnome.h>
/* *** { Macros used to manage entries to cmd_spec */

#define MAXPATHLEN  1024

#define INITCMDS(c) \
	c.ncmds = 0;

#define AddArg(c,val) 	\
	c.cmd_spec[c.ix].cs_arguments[c.cmd_spec[c.ix].cs_nargs++] = (char*)(val)
#define SetCommandName(c,val) \
	strcpy(c.cmd_spec[c.ix].cs_command, val);

#define SetFollowFunc(c,func) 	\
	c.cmd_spec[c.ix].cs_func = func
#define SetStopOnFailure(c, val)	\
	c.cmd_spec[c.ix].cs_end_if_nonzero = val
#define NEWCMD(c, name) 		\
	c.ix = c.ncmds++; c.cmd_spec[c.ix].cs_nargs = 0; SetCommandName(c,name); AddArg(c,name);

/* *** } Macros used to manage entries to cmd_spec */

/* Full-paths to all external commands used */
static const char CMD_RMFORMAT[] = "/usr/bin/rmformat";
static const char CMD_VOLRMMOUNT[] = "/usr/bin/volrmmount";
static const char CMD_EJECT[] = "/usr/bin/eject";
static const char CMD_FDISK[] = "/usr/sbin/fdisk";
static const char CMD_MKFS[] = "/usr/sbin/mkfs";
static const char CMD_MYVOLRMMOUNT[] = "/usr/dt/bin/ownvolrmmount";
static const char CMD_FDFORMAT[] = "/usr/bin/fdformat";
static const char CMD_LABELIT[] = "/usr/sbin/labelit";

typedef struct _CommandSpec {
	char cs_command[MAXPATHLEN];
	char *cs_arguments[10];	/* Arbitrary number of arguments, should be
				 * ok */
	int cs_nargs;
	    gboolean (*cs_func) ();
	gboolean  cs_end_if_nonzero;	/* Whether to exit if the command
					 * returns nonzero */
}            CommandSpec;

typedef struct _CommandInfo {
	CommandSpec cmd_spec[100];
	int ix;
	int ncmds;
	pid_t pid;
	int exit_status;
} CommandInfo;

extern CommandInfo external_cmd;

extern void ExecuteCurrentCommand();			/* these functoons carry out */
extern void BeginCommandSequence();			/* begin the sequence to format the media */
