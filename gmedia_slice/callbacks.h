/*
 * Copyright 2002 Sun Microsystems Inc.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Street #330, Boston, MA 02111-1307, USA.
 *
 */

#include <gnome.h>


void
on_slicesautoslice_clicked             (GtkButton       *button, gpointer         user_data);

void
on_slicesautoslice_clicked             (GtkButton       *button, gpointer         user_data);

void slice_list_selection_cb                (GtkTreeView        *treeview, GtkTreePath            *path, GtkTreeViewColumn            *iter, gpointer        data);

void
setup_auto_slice                       (GtkButton       *button, gpointer         user_data);

void
remove_slice                           (GtkButton       *button, gpointer         user_data);

void
start_changed                          (GtkEditable     *editable, gpointer         user_data);

void
switch_editable                        (GtkToggleButton *togglebutton, gpointer         user_data);

void
size_change                            (GtkEditable     *editable, gpointer         user_data);

void
on_slicedetailsflagsread_toggled       (GtkToggleButton *togglebutton, gpointer         user_data);

void
on_slicedetailsflagswrite_toggled      (GtkToggleButton *togglebutton, gpointer         user_data);

void
on_slicedetailsflagsmnt_toggled        (GtkToggleButton *togglebutton, gpointer         user_data);

void
on_autoslicecancelbtn_clicked          (GtkButton       *button, gpointer         user_data);

void
on_autoslicehelpbtn_clicked            (GtkButton       *button, gpointer         user_data);

void
on_cancel_clicked                      (GtkButton       *button, gpointer         user_data);

void
on_help_clicked                        (GtkButton       *button, gpointer         user_data);

void 
add_new_slice                          (GtkButton       *button, gpointer         user_data);

void
wrap_up                                (GtkButton       *button, gpointer         user_data);

void
apply_changes_to_slice                 (GtkButton       *button, gpointer         user_data);

void 
change_to_bytes                        (GtkMenuItem     *menuitem, gpointer         user_data);


void 
change_to_kilobytes                    (GtkMenuItem     *menuitem, gpointer         user_data);


void
change_to_megabytes                    (GtkMenuItem     *menuitem, gpointer         user_data);


void 
change_to_cylinders                    (GtkMenuItem     *menuitem, gpointer         user_data);

void
alternates_menuitem_cb                 (GtkMenuItem     *menuitem, gpointer         user_data);

void
boot_menuitem_cb                       (GtkMenuItem     *menuitem, gpointer         user_data);

void
home_menuitem_cb                       (GtkMenuItem     *menuitem, gpointer         user_data);

void
root_menuitem_cb                       (GtkMenuItem     *menuitem, gpointer         user_data);

void
stand_menuitem_cb                      (GtkMenuItem     *menuitem, gpointer         user_data);

void
swap_menuitem_cb                       (GtkMenuItem     *menuitem, gpointer         user_data);

void
unassigned_menuitem_cb                 (GtkMenuItem     *menuitem, gpointer         user_data);

void
usr_menuitem_cb                        (GtkMenuItem     *menuitem, gpointer         user_data);

void
var_menuitem_cb                        (GtkMenuItem     *menuitem, gpointer         user_data);

void
ufs_FS_menuitem_cb                     (GtkMenuItem     *menuitem, gpointer         user_data);

void
udfs_FS_menuitem_cb                    (GtkMenuItem     *menuitem, gpointer         user_data);

void
pcfs_FS_menuitem_cb                    (GtkMenuItem     *menuitem, gpointer         user_data);
void
delete_event_cb 		       (GtkWidget 	*widget, GdkEvent 	*event, gpointer 	data);
