/*
 * Copyright 2002 Sun Microsystems Inc.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Street #330, Boston, MA 02111-1307, USA.
 *
 */

/* This is the Solaris version of rmedia.h.
 * On other platforms (e.g. Linux, FreeBSD), the same functions and
 * macros are required * but would be replaced with OS-specific values.
 * sys/smedia.h is an include for the Solaris smedia API.
 */



#include <sys/smedia.h>


/* So that RMM utilites do not need any smedia.h or dkio.h includes
 */
/* Mapping defines from smedia.h */

/* Format type passed into rmedia_device_format */
#define RM_FORMAT_LONG		SM_FORMAT_LONG	/* Full format of the whole media */ 
#define RM_FORMAT_QUICK		SM_FORMAT_QUICK /* Formats only selected tracks so quicker */
#define RM_FORMAT_FORCE		SM_FORMAT_FORCE /* Full format erasing any passwords associated with the media*/
#define RM_FORMAT_DD		SM_FORMAT_DD	/* Format Double Density floppy */
#define RM_FORMAT_MD		SM_FORMAT_MD	/* Format medium Density floppy */
#define RM_FORMAT_HD		SM_FORMAT_HD	/* Format High Density floppy */
#define RM_FORMAT_ED		SM_FORMAT_ED	/* Format Extended Density floppy */

/* Format modes passed in rmedia_device_format */
#define RM_FORMAT_IMMEDIATE		SM_FORMAT_IMMEDIATE	/* Do not block until format completion */
#define RM_FORMAT_BLOCKED		SM_FORMAT_BLOCKED	/* Block until the requested format is complete */

/* Media types returned by rmedia_medium_get_media_type*/
#define RM_FIXED_DISK				SM_FIXED_DISK
#define RM_FLOPPY					SM_FLOPPY
#define RM_REMOVABLE_DISK			SM_REMOVABLE_DISK
#define RM_PCMCIA_MEM				SM_PCMCIA_MEM

/* Protection values returned by rmedia_device_get_protection. */
#define RM_WRITE_PROTECT_DISABLE	SM_WRITE_PROTECT_DISABLE
#define RM_WRITE_PROTECT_NOPASSWD	SM_WRITE_PROTECT_NOPASSWD
#define RM_WRITE_PROTECT_PASSWD		SM_WRITE_PROTECT_PASSWD
#define RM_READ_WRITE_PROTECT		SM_READ_WRITE_PROTECT

#define RM_UNPROTECTED				SM_UNPROTECTED
#define RM_WRITE_PROTECTED			SM_WRITE_PROTECTED
#define RM_WRITE_PROTECTED_WP		SM_WRITE_PROTECTED_WP
#define RM_READ_WRITE_PROTECTED		SM_READ_WRITE_PROTECTED

/* Mapping defines from dkio.h */

#define RM_DKIOCGGEOM	DKIOCGGEOM      /* Get geometry */
#define RM_DKIOCINFO	DKIOCINFO       /* Get info */
#define RM_DKIOCEJECT	DKIOCEJECT      /* Generic 'eject' */
#define RM_DKIOCGVTOC	DKIOCGVTOC      /* Get VTOC */
#define RM_DKIOCSVTOC	DKIOCSVTOC      /* Set VTOC & Write to Disk */

#define RM_DK_MO_ERASABLE	DK_MO_ERASABLE	/* MO Erasable */
#define RM_DK_MO_WRITEONCE	DK_MO_WRITEONCE	/* MO Write once */
#define RM_DK_AS_MO			DK_AS_MO		/* AS MO */
#define RM_DK_CDROM			DK_CDROM		/* CDROM */
#define RM_DK_CDR			DK_CDR			/* CD-R */
#define RM_DK_CDRW			DK_CDRW			/* CD-RW */
#define RM_DK_DVDROM		DK_DVDROM		/* DVD-ROM */
#define RM_DK_DVDR			DK_DVDR			/* DVD-R */
#define RM_DK_DVDRAM		DK_DVDRAM		/* DVD_RAM or DVD-RW */

/*
 * Media types for other rewritable magnetic media
 */
#define RM_DK_FIXED_DISK	DK_FIXED_DISK	/* Fixed disk SCSI or otherwise */
#define RM_DK_FLOPPY		DK_FLOPPY          /* Floppy media */
#define RM_DK_ZIP			DK_ZIP          /* IOMEGA ZIP media */
#define RM_DK_JAZ			DK_JAZ          /* IOMEGA JAZ media */

#define PCFS			1
#define NEC				2
#define UDFS			3
#define UFS				4

typedef struct s_FileSystem {
	char	*fs_name;
	char	*fs_type;
	size_t	max_volume_label; /* maximum length volume label can be */ 
	int	sliceable;	/* supports UFS-style slicing (currently UFS only) */
	int	formattable;	/* other operations possible on media, but not formatting */
} FileSystem;

typedef void *MediumHandle;
typedef void *MediumInfoHandle;
typedef void *DeviceInfoHandle;

/*  Returns a handle (file descriptor) to the device. NULL is returned if device is not
    found or cannot be opened */
MediumHandle rmedia_device_open_medium_handle(const char * media_device_name);

/* Closes the handle (file descriptor) to the device */
void rmedia_device_close_medium_handle(MediumHandle);

/* Takes in a handle to the medium device and returns a void* to the structure
   containing the devices properties. The structure is malloc'd by this function, but
   it is up to user to free it. NULL is returned if operation failed */
MediumInfoHandle rmedia_device_get_medium_property(MediumHandle medium_handle);

/* Takes in a handle to the device and returns a void* to the structure containing device
   info The structure is malloc'd by this function, but
   it is up to user to free it. NULL is returned if operation failed */
DeviceInfoHandle rmedia_device_get_info(MediumHandle medium_handle);

/* The following functions return the individual members of struct 
   returned by rmedia_get_medium_property */

/* Returns the number of physical cylinders of the device */
int32_t rmedia_medium_get_cylinders(MediumInfoHandle medium_info);

/* Returns the media type of the device. The media_types are can be one
   of the #defines above */
int32_t rmedia_medium_get_media_type(MediumInfoHandle medium_info);

/* Returns the block size in bytes of the device */
int32_t rmedia_medium_get_block_size(MediumInfoHandle medium_info);

/* Returns the version of the device */
int32_t rmedia_medium_get_version(MediumInfoHandle medium_info);

/* Returns the capacity in blocks of the device */
int32_t rmedia_medium_get_capacity(MediumInfoHandle medium_info);

/* Returns the number of heads on the device */
int32_t rmedia_medium_get_heads(MediumInfoHandle medium_info);

/* Returns the number of sectors per cylinder on the device */
int32_t rmedia_medium_get_sectors(MediumInfoHandle medium_info);



/* This function calculates the size of the media in sectors by multipling the 
   number of cylinders by the number of heads by the number of sectors */
unsigned long rmedia_medium_calculate_media_size(MediumInfoHandle medium_info);


/* Returns an int that is the percentage of formatting so far
   completed on the device. Returns -1 if operation failed.
 */
int rmedia_device_check_format_status(MediumHandle media_handle);

/* Returns the protection status of the medium. Either SM_UNPROTECTED, SM_WRITE_PROTECTED,
   SM_WRITE_PROTECTED_WP or SM_READ_WRITE_PROTECTED. -1 is returned if operation fails. */
int rmedia_device_get_protection_status(MediumHandle media_handle) ;

/* Takes in a pathname to the media device, the new protection state (see #defines above), 
   a password , the length of the password and a version number. 
   It then set the new protection protecties on the medium.
   Returns 0  if successful else -1 if operation failed.  
 */
int rmedia_device_set_protection_status(char* media_pathname, int32_t state, const char *passwd, int32_t len,int32_t version );

/* Takes a handle to the device, the format type, (see #defines above) and 
   format mode(RM_FORMAT_IMMEDIATE or RM_FORMAT_BLOCKED) and then formats the medium.
   Returns 0 if successful else -1 if operation failed.   
 */
int rmedia_device_format(MediumHandle media_fd, int format_type, int format_mode);


/* wraps media_findname(VOLMGT) - caller owns return value */
char * rmedia_findname( char *start);

/* Returns an array of structures containing information about the supported filesystems
   for this medium. If the formattable field is set in this structure, this means that
   the medium supported formating of that filesystem. */
FileSystem *rmedia_medium_get_supported_filesystems(MediumInfoHandle medium_info, int *ntypes );

/* Sets variable free and used to be the number of bytes free and bytes used on the device. Returns
   0 if successful or -1 if operation failed. */
int rmedia_device_get_block_info(char *media_device_name, unsigned long *free, unsigned long *used);



