/*
 * Copyright 2002 Sun Microsystems Inc.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Street #330, Boston, MA 02111-1307, USA.
 *
 */

#include <stdio.h>
#include <sys/types.h>
#include <sys/statvfs.h>
#include <sys/smedia.h>
#include <volmgt.h>
#include <sys/wait.h>
#include <sys/errno.h>
#include <dlfcn.h>
#include <dirent.h>
#include <errno.h>
#include <sys/dkio.h>
#include <sys/vtoc.h>
#include <unistd.h>
#include <stdlib.h>
#include <nl_types.h>
#include "rmedia_utils.h"


typedef enum {
	PC_FS,
	NEC_FS,
	U_FS,
	UD_FS
} fsys_types;

/* OS-SPECIFIC!
 *
 * This is the default set of Solaris filesystems supported; eventually this will
 * be replaced with code that can determine this dynamically
 */
static FileSystem default_filesystems[] = {
	{"PCFS", "pcfs", 11,0,  1},
	{"NEC", "nec", 11,0,  1},
	{"UFS", "ufs", 8,1,  1},
	{"UDFS", "udfs", 8,0,  1},
};

#define NUM_FSTYPES	(sizeof(default_filesystems)/sizeof(default_filesystems[0]))

/*  Returns a handle (file descriptor) to the device */
MediumHandle rmedia_device_open_medium_handle(const char *media_device_name) {

	
	int mfd;

	mfd = open(media_device_name, O_RDONLY);

	if(mfd < 0) {
		mfd = NULL;
		}

	return((void *)mfd);
}

/* Closes the handle (file descriptor) to the device */
void rmedia_device_close_medium_handle(MediumHandle media_fd) {

	close((int)media_fd);

}

/* Takes in a handle to the medium device and returns a void* to the structure
    containing the devices properties */

MediumInfoHandle rmedia_device_get_medium_property(MediumHandle medium_handle) {

	int res;
	smmedium_prop_t *medium_info;

	medium_info = malloc(sizeof(smmedium_prop_t));

	res = smedia_get_medium_property((int)medium_handle, medium_info);

	if(res < 0) {
 
		free(medium_info);
		medium_info = NULL;
	}

	return ((void *)medium_info);

}
/* Takes in a handle to the device and returns a void* to the structure containing device
   info */
DeviceInfoHandle rmedia_device_get_info(MediumHandle medium_handle){

	int res;
	smdevice_info_t *device_info;

	device_info = malloc(sizeof(smdevice_info_t));
		
	res = smedia_get_device_info((int)medium_handle, device_info);

	if(res < 0) {
		free(device_info);
		device_info = NULL;
	}

	return(device_info);
}
/* The following functions return the individual members of struct 
   returned by rmedia_get_medium_property */

/* Returns the number of physical cylinders of the device */
int32_t rmedia_medium_get_cylinders(MediumInfoHandle medium_info) {
 
	return(((smmedium_prop_t*)medium_info)->sm_pcyl);
}
/* Returns the media type of the device */
int32_t rmedia_medium_get_media_type(MediumInfoHandle medium_info){

	return(((smmedium_prop_t*)medium_info)->sm_media_type);

}
/* Returns the block size in bytes of the device */
int32_t rmedia_medium_get_block_size(MediumInfoHandle medium_info) {

	return(((smmedium_prop_t*)medium_info)->sm_blocksize);

}
/* returns the version of the device */
int32_t rmedia_medium_get_version(MediumInfoHandle medium_info) {


	return(((smmedium_prop_t*)medium_info)->sm_version);
}

/* Returns the capacity in blocks of the device */
int32_t rmedia_medium_get_capacity(MediumInfoHandle medium_info) {


	return(((smmedium_prop_t*)medium_info)->sm_capacity);

}
/* Returns the number of heads on the device */
int32_t rmedia_medium_get_heads(MediumInfoHandle medium_info) {

	return(((smmedium_prop_t*)medium_info)->sm_nhead);

}
/* Returns the number of sectors on the device */
int32_t rmedia_medium_get_sectors(MediumInfoHandle medium_info) {

	return(((smmedium_prop_t*)medium_info)->sm_nsect);

}

/* This function calculates the size of the media by multiply the number of cylinders
   by the number of heads by the number of sectors */

unsigned long rmedia_medium_calculate_media_size(MediumInfoHandle medium_info) {

	unsigned long media_size;

	smmedium_prop_t *mi = (smmedium_prop_t*)medium_info;

	media_size = mi->sm_nhead * mi->sm_pcyl  * mi->sm_nsect;

	return(media_size);

}


/* Returns an int that is the percentage of formatting so far
   completed on the device 
 */

int rmedia_device_check_format_status(MediumHandle media_handle) {

	return(smedia_check_format_status((int)media_handle));

}

/* Returns the protection status of the medium */
int rmedia_device_get_protection_status(MediumHandle media_handle) {

	int prot;

	prot = smedia_get_protection_status((int)media_handle);

	return(prot);

}

/* Takes in a pathname to the media device, a  state, a password , a length 
   and a version . It then set the new protection protecties on the medium.
   Returns the result of this operation.
 */
int rmedia_device_set_protection_status(char* media_pathname, int32_t state, const char *passwd, int32_t len , int32_t version) {

        MediumHandle fd;
	char *env_init[] = {"USER=unknown", "PATH=/usr/bin", NULL};
	pid_t pid;
	smwp_state_t protect_info;
	int result = 0;

        fd = rmedia_device_open_medium_handle(media_pathname);

	/* unmount the medium  volrmmount -e (soft eject)
	 * setup the new properties,
	 * apply new properties
	 * mount the medium    volrmmount -i (soft insert)
	 * notifies vold, and so updates /tmp/.removable and media manager */

	if( (pid = fork() ) < 0)
	{
		result = -1;
	}
	else if(pid == 0)
	{
		if(execle("/usr/bin/volrmmount", "volrmmount", "-e",
			media_pathname, (char *) 0, env_init) < 0)
		{
			result = -1;
		}
	}

	if(waitpid(pid, NULL, 0) < 0)
		result = -1;

	protect_info.sm_new_state = state;
	if(passwd) 
		strncpy(protect_info.sm_passwd, passwd, len);
	else 
		protect_info.sm_passwd[0] = '\0';

	protect_info.sm_passwd_len = len;
	protect_info.sm_version = version;

	if (result != -1)
		result = smedia_set_protection_status((int)fd, &protect_info);

	if( (pid = fork() ) < 0)
	{
		result = -1;
	}
	else if(pid == 0)
	{
		if(execle("/usr/bin/volrmmount", "volrmmount", "-i",
			media_pathname, (char *) 0, env_init) < 0)
		{
			result = -1;
		}
	}

	if(waitpid(pid, NULL, 0) < 0)
		result = -1;

        rmedia_device_close_medium_handle(fd);

	return result;
}



/* Take a handle to the device, the format_mode and type and Calls smedia_format */

int rmedia_device_format(MediumHandle media_fd,int format_mode,int type ) {

	int res = smedia_format((int)media_fd, format_mode, type);

	return (res);
}




/* volmgt.h wrapper */

char * rmedia_findname( char *start){

	char *media_name = media_findname(start);

	return(media_name);

}

FileSystem *rmedia_medium_get_supported_filesystems(MediumInfoHandle medium_info, int *ntypes )
{
	int32_t		media_type;
	FileSystem	*fs_list;
	int		i;

	*ntypes = NUM_FSTYPES;
	fs_list = (FileSystem *) malloc(NUM_FSTYPES * sizeof(FileSystem));

	for (i = 0; i < NUM_FSTYPES; i++) {
		fs_list[i].fs_name = default_filesystems[i].fs_name;
		fs_list[i].fs_type = default_filesystems[i].fs_type;
		fs_list[i].sliceable = default_filesystems[i].sliceable;
		fs_list[i].max_volume_label = default_filesystems[i].max_volume_label;
		fs_list[i].formattable = default_filesystems[i].formattable;
	}

	media_type = rmedia_medium_get_media_type(medium_info);
	switch (media_type) {
		case RM_FLOPPY :
			fs_list[U_FS].sliceable = 0;
			break;
		default :
			fs_list[NEC_FS].formattable = 0;
			break;

	}

	return((void *)fs_list);
}

/****************************************************

Calculate the capacities of the medium. How many bytes used
and how many bytes free. We use statvfs to determine this,
and may have some rounding errors.
statvfs uses blocks and block size, so we need to multiply
these values to get the sizes in bytes.

**************************************************/
int rmedia_device_get_block_info(char *media_device_name, unsigned long *free, unsigned long *used)
{
	struct statvfs  buf;
	int             ret_val;

	ret_val = statvfs(media_device_name, &buf);

	if (ret_val == 0)
	{
		*free = buf.f_bfree * buf.f_frsize;
		*used = ((buf.f_blocks - buf.f_bfree) * buf.f_frsize);
	}

	return ret_val;
}
