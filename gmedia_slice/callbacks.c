/*
 * Copyright 2002 Sun Microsystems Inc.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Street #330, Boston, MA 02111-1307, USA.
 *
 */

#ifdef HAVE_CONFIG_H
#  include <config.h>
#endif

#include <ctype.h>
#include <string.h>
#include <gnome.h>

#include "callbacks.h"
#include "interface.h"
#include "support.h"
#include "media_slice.h"

extern char filename [MAXPATHLEN];
void remove_all();
void verify_numeric(gint from, GtkEditable *ed);
void redefine_slices();
int check_overlap();
static void exit_gracefully(void);

void
slice_list_selection_cb                (GtkTreeView        *treeview,GtkTreePath *path, GtkTreeViewColumn            *iter, gpointer        data)
{
  gint count = 1;
  /* 
   * Determine which slice has been selected in the list box,
   * scroll thru the list of slices to set curr pointing to
   * the selected slice.
   */
  int row;
  GValue val = {0, };
  GtkTreeSelection *selection;
  GtkTreeIter newiter;
  GtkTreePath *newpath;
  GtkTreeModel *model1 = NULL;
  GtkTreeIter *selected_node = NULL;
  gint selected;

  selection = gtk_tree_view_get_selection (GTK_TREE_VIEW (treeview));
  selected = gtk_tree_selection_get_selected (selection, &model1, &newiter);
  if (selected == FALSE)
          return;
  if (gtk_tree_selection_get_selected (selection, NULL, &newiter))
        {
        newpath = gtk_tree_model_get_path (gtk_tree_view_get_model(GTK_TREE_VIEW(treeview)), &newiter);
        row = gtk_tree_path_get_indices (newpath)[0];
        }

  selected = row;
  /*
   * Apparently, the first slice on a disk has to be UFS, so
   * we need to grey out "UDFS" to prevent the user trying to
   * create the first slice on the disk with a UDFS filesystem.
   * This could change at a future date.
   */
  if(selected == 0)
  	gtk_widget_hide(GTK_WIDGET(udfs_menuitem));
  	/*gtk_widget_set_sensitive(GTK_WIDGET(udfs_menuitem), FALSE);*/
  else
  	gtk_widget_show(GTK_WIDGET(udfs_menuitem));
  	/*gtk_widget_set_sensitive(GTK_WIDGET(udfs_menuitem), TRUE);*/
	
  curr = start;
  while(count <= row) {
  	count++;
	curr = curr->nextr;
  }
  setup_unused();
}

void
setup_auto_slice                       (GtkButton       *button,
                                        gpointer        user_data)
{
  gint slice_size, space_over, start_at, i, x;
  div_t rec;
  slice_ptr tmp_p, tmp_s;
  
  /*
   * Clear any existing slice information from data structures, and
   * make all slices available
   */
  remove_all();
  for(i = 0; i < MAX_SLICES; i++)
  	available[i] = 0;

  /* Determine how many slices we need to create */
  nslice = gtk_spin_button_get_value_as_int(GTK_SPIN_BUTTON(autoslicespinbox));
  
  /*
   * Calculate how many cylinders per slice,
   * and the number of cylinders remaining.
   */
  rec = div(nc, nslice);
  slice_size = rec.quot;
  space_over = rec.rem;
  start_at = 0;
  
  /* Build up the data structure for the slices */
  start = NULL;
  x = 0;
  for (i = 0; i < (nslice + 1); i++) {
 	if (start == NULL) {		/* start of data struct */
        start = slice_create ();
 		curr = start;
 		curr->nextr = NULL;
 	} else {					/* append to data struct */
 		curr->nextr = slice_create ();	
 		curr = curr->nextr;
 		curr->nextr = NULL;
 	}  
 	
 	if( nslice < BACKUP_SLICE)		/* add backup slice  if only 1 or 2 slices */
 		if( i == nslice )
 			i = BACKUP_SLICE;
 	
 	if (i == BACKUP_SLICE) {		/* add backup slice */
    	curr->slice_num = i;
    	curr->size = nc * nh * ns * bs;
    	curr->slice_start = 0;
    	sprintf (curr->slice_tag, "backup");
    	sprintf (curr->slice_filesys, "-"); 
    	sprintf (curr->slice_flag, "ru");
 	} else {					/* otherwise add a slice */
 		curr->slice_num = i;
 		sprintf (curr->slice_flag, "wm");
 		curr->nextr = NULL;

 		curr->slice_start = start_at;
 		if (space_over > 0) { /* add extra cylinder if any odd cylinders remain */
 			curr->size = (slice_size + 1)* (nh * ns * bs);
 			space_over--;
 		} else
 			curr->size = slice_size * (nh * ns * bs);
 		
 		sprintf (curr->slice_tag, "usr");
 		sprintf (curr->slice_filesys, "ufs"); 	
 		start_at = start_at + curr->size;
    }	
    available[i] = 1; /* mark this slice as used */
  }
  
  nslice++; /* extra slice for backup slice */
  curr = start;
  /* add in info for unused RHS of GUI*/
  unused->size = start->size;
  unused->size = first_free;
  sprintf (unused->slice_tag, "usr");
  sprintf (unused->slice_filesys, "ufs"); 	
  sprintf (unused->slice_flag, "wm");
  
  update_list_display();  /* display new data structure */
  selected = 0;
  
  /* grey out New button, set Delete and Change to visible */
  gtk_widget_set_sensitive(GTK_WIDGET(slicedetailnewbtn), FALSE);
  gtk_widget_set_sensitive(GTK_WIDGET(slicesdelete), TRUE);
  gtk_widget_set_sensitive(GTK_WIDGET(slicedetailschangebtn), TRUE);
   
  /* This is the OK button of the slice editor. */
  gtk_widget_set_sensitive(GTK_WIDGET(button1), TRUE);

  /* Hide AutoSlice dialog */
  gtk_widget_hide(GTK_WIDGET(dialog2));
  
  curr = start;
  setup_unused();
  
}

void
remove_slice                           (GtkButton       *button,
                                        gpointer         user_data)
{
  /* Remove a Slice, called buy clicking on "Delete" */
  gint active_slice, x, i, n, tmp;
  slice_ptr prev, tmp_p;
  GtkTreeSelection *selection;
  GtkTreeIter newiter;
  GtkTreePath *newpath;
  gint selected, row = 0;

  selection = gtk_tree_view_get_selection (GTK_TREE_VIEW (treeview));

  if (gtk_tree_selection_get_selected (selection, NULL, &newiter))
    {
         newpath = gtk_tree_model_get_path 
			(gtk_tree_view_get_model(GTK_TREE_VIEW (treeview)), &newiter);
         row = gtk_tree_path_get_indices (newpath)[0];
         gtk_tree_path_free (newpath);
    } else {
         return;
    }

  selected = row;

  x=0;
  curr = start;
  /*
   * If we selected the first slice, set the start of the list to point
   * to the second element in the list and remove the first. If the slice
   * to be removed is the backup slice, we need to reserve slice 2. If 
   * slice two is in use move it to an available position, (the number of
   * the slice we just deleted.
   */
  if(selected == 0) {
  	start = curr->nextr;
    if(strcmp(curr->slice_tag, "backup") == 0) {
    	if(curr->slice_num != BACKUP_SLICE) {
    		if(available[BACKUP_SLICE] == 0) {
    			available[BACKUP_SLICE] = 1;
    			available[curr->slice_num] = 0;
    		} else {
				tmp_p = start;
				while((tmp_p->slice_num != BACKUP_SLICE) && tmp_p != NULL)
    				tmp_p = tmp_p->nextr;
    			if(tmp_p != NULL)
    				tmp_p->slice_num = curr->slice_num;
			}
		}
	} else
		available[curr->slice_num] = 0;
	
	size_free = size_free + curr->size;
	free(curr);
	curr = start;
  } else {	/* otherwise */
  	while(x != selected) { /* locate the data struct for the required slice */
		prev = curr;
    	curr = curr->nextr;
    	x++;
	}
	if(curr != NULL)
    		prev->nextr = curr->nextr;
	
	if(strcmp(curr->slice_tag, "backup") == 0) { /* if its tha backup slice */
    	if(curr->slice_num != BACKUP_SLICE) {
    		if(available[BACKUP_SLICE] == 0) {
    			available[BACKUP_SLICE] = 1;
    			available[curr->slice_num] = 0;
    		} else {
				tmp_p =  start;
				while((tmp_p->slice_num != BACKUP_SLICE) && tmp_p != NULL)
					tmp_p = tmp_p->nextr;
				if(tmp_p != NULL)
    				tmp_p->slice_num = curr->slice_num;
			}
		}
	} else
		available[curr->slice_num] = 0;
	
	size_free = size_free + curr->size;
    free(curr);
    curr = prev;
  }
  
  nslice--;  
  curr = start;
  while(curr != NULL)
	curr = curr->nextr;
   
  if(mode == PREVENT_OVERLAP) {
    first_free = 0;
    size_free = 0;
    remove_gaps();	
  }
  
  update_list_display();
  selected = 0;
  if(nslice == 0) {
  	/* Grey out the delete button and the change button */
  	gtk_widget_set_sensitive(GTK_WIDGET(slicesdelete), FALSE);
	gtk_widget_set_sensitive(GTK_WIDGET(slicedetailschangebtn), FALSE);

        /* This is the Ok button of the slice editor. Grey it out 
           when there are no slices. 
        */
        gtk_widget_set_sensitive(GTK_WIDGET(button1),FALSE);
	size_free = capacity;
	first_free = 0;
  }
  
  /*
   * If there is space free and we haven't used up all possible
   * slices the set the new button to be visible
   */
  if(size_free > 0) {
  	for(i = 0; i < MAX_SLICES; i++) {
		if(available[i] == ISFREE)
			gtk_widget_set_sensitive(GTK_WIDGET(slicedetailnewbtn), TRUE);
	}
  }
  
  curr = start;
  setup_unused();
}


void
start_changed                          (GtkEditable     *editable,
                                        gpointer         user_data)
{
	verify_numeric(0,  editable);
}


void
switch_editable                        (GtkToggleButton *togglebutton,
                                        gpointer         user_data)
{
  /* Toggle between Prevent and Allow Overlaps */
  if(mode == ALLOW_OVERLAP) {
  	mode = PREVENT_OVERLAP;
	gtk_editable_set_editable(GTK_EDITABLE(GTK_ENTRY(entry4)), FALSE);
	redefine_slices();
  } else {
  	mode = ALLOW_OVERLAP;
	gtk_editable_set_editable(GTK_EDITABLE(GTK_ENTRY(entry4)), TRUE);
	first_free = 0;
	size_free = 0;
  }
}


void
size_change                            (GtkEditable     *editable,
                                        gpointer         user_data)
{
  verify_numeric(1,  editable);
}

void
on_slicedetailsflagsread_toggled       (GtkToggleButton *togglebutton,
                                        gpointer         user_data)
{
  /* 
   * The READ item has been toggled, we either set it to readable
   * or un-readable depending on what it was before.
   */     
  protection = convert_flag();

  switch (protection) {
	case 0: case 2: case 4: case 6:
		protection++;
		break;
	case 1: case 3: case 5: case 7:
		protection--;
		break;
	default:
		break;
  }


  /* change the value of protection into text */
 
  switch (protection) {
  	case 0:
   		strcpy(unused->slice_flag, "-");
   		break;
  	case 1:
   		strcpy(unused->slice_flag, "ru");
   		break;
 	case 2:
   		strcpy(unused->slice_flag, "wu");
   		break;
 	case 3:
   		strcpy(unused->slice_flag, "rw");
   		break;
 	case 4:
   		strcpy(unused->slice_flag, "wm");
   		break;
 	case 5:
   		strcpy(unused->slice_flag, "rm");
   		break;
 	case 6:
   		strcpy(unused->slice_flag, "wm");
   		break;
 	case 7:
   		strcpy(unused->slice_flag, "wm");
   		break;
 	default:
   		strcpy(unused->slice_flag, "wm");
   		break;
  }   

  /* The flag "rw" is not supported by rmforat, so the format fails
   * if user starts formatting with one of the slices having "rw" flag
   * set. So don't let user select the "rw" flag by insensitizing the
   * Change button
   */ 
  if (strcmp (unused->slice_flag, "rw") == 0) {
        gtk_widget_set_sensitive(GTK_WIDGET(slicedetailschangebtn), FALSE);
  }
  else if (strcmp (unused->slice_tag, "backup") != 0) {
        gtk_widget_set_sensitive(GTK_WIDGET(slicedetailschangebtn), TRUE);
  }
}


void
on_slicedetailsflagswrite_toggled      (GtkToggleButton *togglebutton,
                                        gpointer         user_data)
{
/* Toggle writeable */
    
    protection = convert_flag();
    

switch (protection) {
	case 0: case 1: case 4: case 5:
		protection = protection + 2;
		break;
	case 2: case 3: case 6: case 7:
		protection = protection - 2;
		break;
	default:
		break;
}

   switch (protection) {
   case 0:
   	strcpy(unused->slice_flag, "-");
   	break;
   case 1:
   	strcpy(unused->slice_flag, "ru");
   	break;
   case 2:
   	strcpy(unused->slice_flag, "wu");
   	break;
   case 3:
   	strcpy(unused->slice_flag, "rw");
   	break;
   case 4:
   	strcpy(unused->slice_flag, "wm");
   	break;
   case 5:
   	strcpy(unused->slice_flag, "rm");
   	break;
   case 6:
   	strcpy(unused->slice_flag, "wm");
   	break;
   case 7:
   	strcpy(unused->slice_flag, "wm");
   	break;
   default:
   	strcpy(unused->slice_flag, "wm");
   	break;
   } 

  /* The flag "rw" is not supported by rmforat, so the format fails
   * if user starts formatting with one of the slices having "rw" flag
   * set. So don't let user select the "rw" flag by insensitizing the
   * Change button
   */ 
  if (strcmp (unused->slice_flag, "rw") == 0) {
        gtk_widget_set_sensitive(GTK_WIDGET(slicedetailschangebtn), FALSE);
  }
  else if (strcmp (unused->slice_tag, "backup") != 0) {
        gtk_widget_set_sensitive(GTK_WIDGET(slicedetailschangebtn), TRUE);
  }
}


void
on_slicedetailsflagsmnt_toggled        (GtkToggleButton *togglebutton,
                                        gpointer         user_data)
{
/* Toggle mountable */
    
    protection = convert_flag();

    switch (protection) {
	case 0: case 1: case 2: case 3:
		protection = protection + 4;
		break;
	case 4: case 5: case 6: case 7:
		protection = protection - 4;
		break;
	default:
		break;
	}

   switch (protection) {
   case 0:
   	strcpy(unused->slice_flag, "-");
   	break;
   case 1:
   	strcpy(unused->slice_flag, "ru");
   	break;
   case 2:
   	strcpy(unused->slice_flag, "wu");
   	break;
   case 3:
   	strcpy(unused->slice_flag, "rw");
   	break;
   case 4:
   	strcpy(unused->slice_flag, "wm");
   	break;
   case 5:
   	strcpy(unused->slice_flag, "rm");
   	break;
   case 6:
   	strcpy(unused->slice_flag, "wm");
   	break;
   case 7:
   	strcpy(unused->slice_flag, "wm");
   	break;
   default:
   	strcpy(unused->slice_flag, "wm");
   	break;
   }

  /* The flag "rw" is not supported by rmforat, so the format fails
   * if user starts formatting with one of the slices having "rw" flag
   * set. So don't let user select the "rw" flag by insensitizing the
   * Change button
   */ 
  if (strcmp (unused->slice_flag, "rw") == 0) {
        gtk_widget_set_sensitive(GTK_WIDGET(slicedetailschangebtn), FALSE);
  }
  else if (strcmp (unused->slice_tag, "backup") != 0) {
        gtk_widget_set_sensitive(GTK_WIDGET(slicedetailschangebtn), TRUE);
  }

}


void
on_slicesautoslice_clicked             (GtkButton       *button,
                                        gpointer         user_data)
{
	/* 
	 * Only create the dialog if it has not already been 
	 * created or if the window manager destroyed it.
	 * Otherwise, just unhide the existing dialog
	 */
	if((dialog2 == NULL) || (GTK_IS_WIDGET(dialog2) == FALSE)) {
		dialog2 = create_dialog2();
		gtk_widget_show(dialog2);
	} else 
		gtk_widget_show(dialog2);
}

void
on_autoslicecancelbtn_clicked          (GtkButton       *button,
                                        gpointer         user_data)
{
	gtk_widget_hide(GTK_WIDGET(dialog2));
}

void
on_autoslicehelpbtn_clicked          (GtkButton       *button,
                                        gpointer         user_data)
{
	GError *error = NULL;
	gnome_help_display_desktop (NULL,
				    "user-guide",
				    "wgosnautilus.xml", "gosnautilus-456",
				    &error);

	if (error) {
		GtkWidget *dialog;
		dialog = gtk_message_dialog_new (GTK_WINDOW (user_data),
					         GTK_DIALOG_MODAL | GTK_DIALOG_DESTROY_WITH_PARENT,
					         GTK_MESSAGE_ERROR,
					         GTK_BUTTONS_CLOSE,
					         _("There was an error displaying help: %s"),
					         error->message);

		g_signal_connect (G_OBJECT (dialog),
				  "response", G_CALLBACK (gtk_widget_destroy),
				  NULL);

		gtk_window_set_resizable (GTK_WINDOW (dialog), FALSE);
		gtk_widget_show (dialog);
		g_error_free (error);
	}
	return;
}


void 
add_new_slice                          (GtkButton       *button,
                                        gpointer         user_data)
{
   /* 
    * Add a new slice to the list of slices, using the information 
    * displayed in the RHS of the GUI.
    */

    slice_ptr tmp, prev;
	gint i, found, n;
	gint isbackup;
	
	nslice++;
    found = 0;
    for(i = 0; i < MAX_SLICES, found < 1; i++) { /* find an available slice */
  		if (available[i] == 0)
  			found = 1;
    }
	
	i--;
	available[i] = 1;
    if(found == 0)
    	return;
	
	tmp = slice_create();
	if(unused->size < 1)
    	unused->size = 1;
		
	/*
	 * If we are preventing overlaps calculate the start and amend the
	 * size if necessary.
	 * Otherwise, just add the slice and make sure it doesn't run off
	 * the end of the media.
	 */
	if(mode == PREVENT_OVERLAP) {
		if(size_free > 0) {
			tmp->slice_num = i;
    		tmp->slice_start = first_free;
			if((unused->size * units) > size_free)
				tmp->size = size_free;
			else
				tmp->size = (unused->size * units);
			
			first_free = first_free + tmp->size;
			size_free = capacity - first_free;
		}
	} else {
		tmp->slice_num = i;
		tmp->slice_start = (unused->slice_start * units);
    	if(((unused->size + unused->slice_start) * units)  > capacity)
    		tmp->size = capacity - (unused->slice_start * units);
    	else
    		tmp->size = (unused->size * units);
	}
	
	strcpy(tmp->slice_flag, "wm");
    strcpy(tmp->slice_filesys, unused->slice_filesys);
    strcpy(tmp->slice_tag, unused->slice_tag);
    strcpy(tmp->slice_flag, unused->slice_flag);
   
	/*
	 * Position the new slice in the list of slices
	 */
	prev = NULL;
    curr = start;
    if (start == NULL) {
        start = tmp;
		curr = start;
  		curr->nextr = NULL;
    } else {
  		while((curr->nextr != NULL) && (tmp->slice_num > curr->slice_num)) {
  			prev = curr;
     		curr = curr->nextr;
     	}
		if(tmp->slice_num < curr->slice_num) {
			tmp->nextr = curr;
  			if(prev == NULL)
  				start = tmp;
  			else
  				prev->nextr = tmp;
  		
			curr = tmp;
		} else {
			curr->nextr = tmp;
  			curr = curr->nextr;
  			curr->nextr = NULL;
		}
	}
	
	re_compute_sizes();
	update_list_display();
	selected = 0;
	isbackup = 0;
	if(mode == PREVENT_OVERLAP) {
    	curr = start;
    	while(curr != NULL) {
    		if(strcmp(curr->slice_tag, "backup") == 0)
    			isbackup++;
    			
    		curr = curr->nextr;		
    	}
    	if(isbackup > 0) {
    		if( (size_free == 0) || (nslice == MAX_SLICES) )
				gtk_widget_set_sensitive(GTK_WIDGET(slicedetailnewbtn), FALSE);
    	} else {
    		if( (size_free == 0) || ((nslice + 1) == MAX_SLICES ) )
    			gtk_widget_set_sensitive(GTK_WIDGET(slicedetailnewbtn), FALSE);
    	}
    } else {
    	if( (size_free == 0) || (nslice == MAX_SLICES) ) {
    		/* grey out "New" button */
    		gtk_widget_set_sensitive(GTK_WIDGET(slicedetailnewbtn), FALSE);
    	}
    }
	gtk_widget_set_sensitive(GTK_WIDGET(slicesdelete), TRUE);
	gtk_widget_set_sensitive(GTK_WIDGET(slicedetailschangebtn), TRUE);

        /* This is the OK button of the slice editor. Enable it when a slice is added */
	gtk_widget_set_sensitive(GTK_WIDGET(button1), TRUE);
	curr = start;
	setup_unused();
}    	


void
wrap_up                                (GtkButton       *button,
                                        gpointer         user_data)
{
  gint isbackup, bkup_free;
  slice_ptr s, p;
  FILE *fp;
  gint overlap, x;
  GtkWidget *dialog;
  
  bkup_free = ISFREE;
  
  if((fp = fopen(filename, "w")) == NULL) {
	fprintf(stderr, _("Could not open file"));
	exit(1);
  }
  
  /* If there are no slices to be used, warn the user */
  if(nslice == 0)
  {
  	dialog = gtk_message_dialog_new(GTK_WINDOW(user_data),GTK_DIALOG_MODAL | GTK_DIALOG_DESTROY_WITH_PARENT | GTK_DIALOG_NO_SEPARATOR,
 GTK_MESSAGE_WARNING, GTK_BUTTONS_OK, _("Warning: No Slices detected. If you do not want to slice this medium, select Cancel in the Slice Editor Dialog"));
	gtk_window_set_transient_for(GTK_WINDOW(dialog), GTK_WINDOW(dialog1));
	gtk_window_set_modal(GTK_WINDOW(dialog), TRUE);
	return;
  }
  
  /* If prevent overlap was switched off, we must check to see if there are any overlaps
       and warn the user. We cannot create a usable slice file if there are any overlaps.
   */
  if(mode == ALLOW_OVERLAP)
  {
  	overlap = check_overlap();
	if(overlap > 0) {
    	dialog = gtk_message_dialog_new(GTK_WINDOW(user_data), GTK_DIALOG_MODAL | GTK_DIALOG_DESTROY_WITH_PARENT | GTK_DIALOG_NO_SEPARATOR,
 GTK_MESSAGE_WARNING, GTK_BUTTONS_OK, _("Warning: Overlaps detected. Please remove them before proceeding, or switch on Prevent Overlaps to remove them automatically."));
		gtk_window_set_transient_for(GTK_WINDOW(dialog), GTK_WINDOW(dialog1));
		gtk_window_set_modal(GTK_WINDOW(dialog), TRUE);
		return;
	}
  }
  
  /* check to see if there is a backup slice, and if
   *    slice 2 is being used
   */
  isbackup = 0;
  curr = start;
  while(curr != NULL)
  {
   	if(strcmp(curr->slice_tag, "backup") == 0)
		isbackup++;
	
	if(curr->slice_num == BACKUP_SLICE)
		bkup_free = NOTFREE;
	
	s = curr;
	curr = curr->nextr;
  }
  curr = s;
  
  if(isbackup == 0) /* if no backup slice */
  {
  	s = slice_create(); /* create a backup slice */
    s->slice_start = 0;
    s->size = capacity;
    strcpy(s->slice_filesys, "-");
    strcpy(s->slice_tag, "backup");
    strcpy(s->slice_flag, "wm");
	
	if(bkup_free == NOTFREE)  /* if slice 2 not available*/
    {
    	x = 0;
    	while((available[x] == NOTFREE) && ( x < MAX_SLICES))
    		x++;	
    		s->slice_num = x; /* append backup slice to list */
    		s->nextr = NULL;
    		curr->nextr = s;
    		s = NULL;
   	} else {    /* slice 2 is free use it for backup */
		s->slice_num = BACKUP_SLICE;
   		curr = start;
   		x = 0;
   		while(  (curr != NULL) && (curr->slice_num < BACKUP_SLICE)  )
   		{
   			p = curr;
   			curr = curr->nextr;
   		}
   		p->nextr = s;
   		s->nextr = curr;
	}
  }
  
  /* Write the slice info to the slice file */
  write_slices(fp, start);
  
  curr = start->nextr;
  fprintf(fp, "slices:\t%d = %0.f, %0.f",start->slice_num, start->slice_start, \
		  start->size);
  if( (strcmp(start->slice_tag,"-") != 0) && (strcmp(start->slice_flag,"-")) )
    	fprintf(fp, ", \"%s\", \"%s\"", start->slice_flag, start->slice_tag);
  
  fprintf(fp, " :\\\n");
  
  while(curr != NULL)
  {
    fprintf(fp, "\t%d = %0.f, %0.f",curr->slice_num, curr->slice_start, curr->size);
    if( (strcmp(curr->slice_tag,"-") != 0) && (strcmp(curr->slice_flag,"-")) )
    	fprintf(fp, ", \"%s\", \"%s\"", curr->slice_flag, curr->slice_tag);
    if(curr->nextr != NULL)
    	fprintf(fp, " :\\\n"); 
    else
    	fprintf(fp, "\n");  
    
	curr = curr->nextr; 
  }
  fprintf(fp, "\n");
  fclose(fp);
  /* 
   * ADD instructions to apply slices here if they are needed 
   * otherwise
   * exit gracefully 
   */

    exit_gracefully();
}
		  
void
on_cancel_clicked                 (GtkButton       *button,
                                   gpointer         user_data)
{
	remove (filename);
	exit_gracefully();
}
void
on_help_clicked                   (GtkButton       *button,
                                   gpointer         user_data)
{
	GError *error = NULL;
	gnome_help_display_desktop (NULL,
				    "user-guide",
				    "wgosnautilus.xml", "gosnautilus-456",
				    &error);

	if (error) {
		GtkWidget *dialog;
		dialog = gtk_message_dialog_new (GTK_WINDOW (user_data),
						 GTK_DIALOG_MODAL | GTK_DIALOG_DESTROY_WITH_PARENT,
						 GTK_MESSAGE_ERROR,
						 GTK_BUTTONS_CLOSE,
						 _("There was an error displaying help: %s"),
						 error->message);

		g_signal_connect (G_OBJECT (dialog),
				  "response", G_CALLBACK (gtk_widget_destroy),
				  NULL);
		gtk_window_set_resizable (GTK_WINDOW (dialog), FALSE);
		gtk_widget_show (dialog);
		g_error_free (error);
	}
	return;
}



  
static void 
exit_gracefully                        (void)
{
    
    /*
	 * We have completed our task, free up the data structures
	 * and exit with sucess 
     */

    curr = start;
    
    while(curr != NULL)
    {
    	curr = curr->nextr;
    	free(start);
    	start = curr;
    }
    
    free(unused);
    exit(0);
}


void
apply_changes_to_slice                 (GtkButton       *button,
                                        gpointer         user_data)
{
  gint i;
  gint row;
  GValue val = {0, };
  GtkTreeSelection *selection;
  GtkTreeIter newiter;
  GtkTreePath *newpath;
  GtkTreeModel *model1 = NULL;
  GtkTreeIter *selected_node = NULL;
  gint selected;

  selection = gtk_tree_view_get_selection (GTK_TREE_VIEW (treeview));
  selected = gtk_tree_selection_get_selected (selection, &model1, &newiter);

  if (selected == FALSE)
          return;

  if (gtk_tree_selection_get_selected (selection, NULL, &newiter))
        {
         newpath = gtk_tree_model_get_path 
			(gtk_tree_view_get_model(GTK_TREE_VIEW (treeview)), &newiter);
         row = gtk_tree_path_get_indices (newpath)[0];
        }

  curr = start;
  selected = row;
  
  for(i = 1; i <= selected; i++)
  	curr = curr->nextr;
  
  curr->slice_start = unused->slice_start * units;
  if(unused->size < 1)
  	unused->size = 1;
  curr->size =  unused->size * units;
  
  strcpy(curr->slice_flag, unused->slice_flag);
  strcpy(curr->slice_tag, unused->slice_tag);
  strcpy(curr->slice_filesys, unused->slice_filesys);
  
  re_compute_sizes();
  update_list_display();
}


void 
change_to_bytes                        (GtkMenuItem     *menuitem,
                                        gpointer         user_data)
{
  gint origu = units;
  units = BYTES;
  update_display(origu);
}


void 
change_to_kilobytes                    (GtkMenuItem     *menuitem,
                                        gpointer         user_data)
{
  gint origu = units;
  units = KBYTES;
  update_display(origu);
}


void
change_to_megabytes                    (GtkMenuItem     *menuitem,
                                        gpointer         user_data)
{
  gint origu = units;
  units = MBYTES;
  update_display(origu);
}


void 
change_to_cylinders                    (GtkMenuItem     *menuitem,
                                        gpointer         user_data)
{
  gint origu = units;
  units = nh * ns * bs;
  update_display(origu);
}

void
alternates_menuitem_cb                 (GtkMenuItem     *menuitem,
                                        gpointer         user_data)
{
  strcpy(unused->slice_tag, "alternates");
}

void
boot_menuitem_cb                       (GtkMenuItem     *menuitem,
                                        gpointer         user_data)
{
  strcpy(unused->slice_tag, "boot");
}

void
home_menuitem_cb                       (GtkMenuItem     *menuitem,
                                        gpointer         user_data)
{
  strcpy(unused->slice_tag, "home");
}

void
root_menuitem_cb                       (GtkMenuItem     *menuitem,
                                        gpointer         user_data)
{
  strcpy(unused->slice_tag, "root");
}

void
stand_menuitem_cb                      (GtkMenuItem     *menuitem,
                                        gpointer         user_data)
{
  strcpy(unused->slice_tag, "stand");
}

void
swap_menuitem_cb                       (GtkMenuItem     *menuitem,
                                        gpointer         user_data)
{
  strcpy(unused->slice_tag, "swap");
}

void
unassigned_menuitem_cb                 (GtkMenuItem     *menuitem,
                                        gpointer         user_data)
{
  strcpy(unused->slice_tag, "unassigned");
}

void
usr_menuitem_cb                        (GtkMenuItem     *menuitem,
                                        gpointer         user_data)
{
  strcpy(unused->slice_tag, "usr");
}

void
var_menuitem_cb                        (GtkMenuItem     *menuitem,
                                        gpointer         user_data)
{
  strcpy(unused->slice_tag, "var");
}


void
ufs_FS_menuitem_cb                     (GtkMenuItem     *menuitem,
                                        gpointer         user_data)
{
  strcpy(unused->slice_filesys, "ufs");
}

void
udfs_FS_menuitem_cb                    (GtkMenuItem     *menuitem,
                                        gpointer         user_data)
{
  strcpy(unused->slice_filesys, "udfs");
}

void
pcfs_FS_menuitem_cb                    (GtkMenuItem     *menuitem,
                                        gpointer         user_data)

{
  strcpy(unused->slice_filesys, "pcfs");
}

/*******************************************

This function removes all slices from
the data structure.

*******************************************/
void
remove_all()
{/* These variable are all unused so we could possibly delete them
  GtkLabel gstring;
  gchar** elements;
  gint n, i;
  */
  curr = start;
  while(curr != NULL)
  {
  	start = curr->nextr;
	available[curr->slice_num] = 0;
	free(curr);
	curr = start;
  }
  
  nslice = 0;
  update_list_display();
  
}


/* FIXME !!!!!The cursor positioning is a bit wonky here */
void
verify_numeric(gint from, GtkEditable *ed)
{
  gchar *text_label, real_label[516];
  gint i, j, x;
  double nn; /* NOTE !!! This could be dodgy for 32bit vs. 64 bit */
  
  nn = 0;
  
  /* Removed the check on variable my_fault which is not required as only
   * size_change manipulates this function. User can not anyway manipulate
   * slice start details. The check used to give some errors like not reading
   * all the text entered in slice size entry and taking only a portion of 
   * the numbers entered. That is why it is removed.
   */
  text_label = (gchar *)gtk_entry_get_text(GTK_ENTRY(ed));
	
  x = strlen(text_label);
  j = 0;
	
  for(i=0; i<x; i++)
  {
	if(isdigit(text_label[i]))
		real_label[j++] = text_label[i];
  }
  real_label[j++] = NULL;
	
  gtk_editable_set_position(GTK_EDITABLE(ed), j); /* pos = j*/
  /*gtk_entry_set_text(GTK_ENTRY(ed), real_label);*/

  i = 0;
	
  while(isdigit(real_label[i]))
  {
	nn = (nn * 10) + (real_label[i] - '0');
	i++;
  }
	
  if(from == 0)
	unused->slice_start = nn; /* Start textfield */
  else
	unused->size = nn; /* Size textfield */
}


/*******************************************************

This function updates the list of slices displayed in the list box

*******************************************************/

/*void update_display(gint old_units)	
{
  gchar *text_label;
  double nn = 0;
  div_t res;
  
  text_label = (gchar *)gtk_entry_get_text(GTK_ENTRY(entry3));

  nn = atof(text_label);
  res = div((nn * old_units),units);
  nn = res.quot;
  
  sprintf(text_label, "%.0f", nn);
  gtk_entry_set_text(GTK_ENTRY(entry3), text_label);
  g_free(text_label);
  
  text_label =(gchar *)gtk_entry_get_text(GTK_ENTRY(entry4));
  
  nn = 0;
  nn = atof(text_label);
  res = div((nn * old_units),units);
  nn = res.quot;
  
  sprintf(text_label, "%.0f", nn);
  gtk_entry_set_text(GTK_ENTRY(entry4), text_label);
  g_free(text_label);
}*/


void
delete_event_cb (GtkWidget *widget, GdkEvent *event, gpointer data)
{
  g_object_unref (G_OBJECT (tooltips));

  /* cleanup and exit */
  remove (filename);
  exit_gracefully();
}
