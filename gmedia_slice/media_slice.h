/*
 * Copyright 2002 Sun Microsystems Inc.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Street #330, Boston, MA 02111-1307, USA.
 *
 */

#include <sys/dkio.h>
#include <sys/vtoc.h>
#include <sys/param.h>


typedef struct _slice_rec {
	int slice_num;
	char slice_tag[15];
	char slice_flag[5];
	double slice_start, size;
	char slice_filesys[6];
	struct _slice_rec *nextr;
	}slice_rec, *slice_ptr;

typedef struct filesys_rec {
	char filesys_name[6];
	struct filesys_rec *nxt;
}           sys_rec, *sys_ptr;


/* Error codes returned by slice_utils.c */
#define ERR_OK			0
#define ERR_DEVPATH		1
#define ERR_BADDEV		2
#define ERR_FILEOPEN	3
#define ERR_PARSE		4
#define ERR_SLICE		6
#define ERR_USAGE		100


/* number of fields displayed in list box */
#define MAX_SLICE_FIELDS 7

/* used by mode, if prevent overlap is toggled on or off */
#define PREVENT_OVERLAP 0
#define ALLOW_OVERLAP 1

/* slice number to be reserved for backup slice (if not specified in slicefile) */
#define BACKUP_SLICE 2

/*  */
#define ISFREE 0
#define NOTFREE 1

/* UNITS used in slice editor */
#define BYTES 1
#define KBYTES 1024
#define MBYTES 1048576
#define GBYTES 1073741824

/* number of possible slices differs per platform */
#ifdef i386
#define MAX_SLICES 16
#else
#define MAX_SLICES 8
#endif


int nslice;
int32_t capacity, nc, nh, ns, bs;
int saveonly, selected, capacity, protection;
/*extern slice_ptr start, curr, unused;*/

/*extern struct dk_geom ioc_struct;*/
double first_free, size_free;

slice_ptr start, curr, unused;

extern char rawdev[MAXPATHLEN], filename[MAXPATHLEN];

extern int parse_args(int argc, char **argv);

extern slice_ptr slice_create ();
extern void slice_delete (slice_ptr );
extern void remove_gaps();

extern void parse_delete_elements (char** elements);

extern void parse_create_elements (slice_ptr slice, char*** elements);
extern void format_display();
extern void setup_unused();
extern void update_list_display();
extern void re_compute_sizes();

extern int available[MAX_SLICES];
extern int change_or_new;
extern int mode;
extern int activated_by_button;
extern int units, max_size, max_size_units;

/*
 * From Slice_Editor_Stubs.c
 * Moved to slice_utils.c
 */
int write_slices(FILE *fp, slice_rec *sliceDefList);

int convert_flag(void);

void update_display(int old_units);
