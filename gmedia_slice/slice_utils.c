/*
 * Copyright 2002 Sun Microsystems Inc.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Street #330, Boston, MA 02111-1307, USA.
 *
 */

#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <sys/param.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <nl_types.h>
#include <string.h>
#include <malloc.h>
#include <fcntl.h>
#include "SDtRmedia.h"

#include "media_slice.h"

int parse_slice(char *line);

char rawdev[MAXPATHLEN], filename[MAXPATHLEN];

FILE *fp1;

int available[MAX_SLICES], readwrite;
int selected;
int mode;


sys_ptr get_file_sys(char sys[256]);

#define False	0
#define True	1

int parse_args(int argc, char **argv)
{

	int arg, x = 0;
	int i_count = 1;		/* set default values */
	int param_error =0, j_set = 0;
	char ast[256];
	MediumHandle devhandle;
	MediumInfoHandle medinfo;
	int32_t media_type;
	
	sys_ptr fs = NULL, tp;
	int fst_line;
	
	i_count = 1;		/* set default values */
	protection = 7;
	units = BYTES;
	fst_line = 0;

	/* check arguments */
	if (argc < 3) {

		param_error = True;
	}
	if (argc > 5)
		param_error = True;
		
	if (argc == 4)
		param_error = True;
		
	
	if (param_error == False)
	{
	    while (i_count < argc) {
		arg = argv[i_count][1];
		switch (arg) {
		case 'd':
		    if(argv[i_count][0] == '-')
		    {
			strcpy(rawdev, argv[i_count + 1]);
			argv[i_count][1] = 'D';
			j_set = j_set + 2;
		    }
			break;
		case 'W':
		    if(argv[i_count][0] == '-')
		    {
			strcpy(filename, argv[i_count + 1]);
			j_set++;
		    }
			break;

		case '?':
			break;
		}
		i_count = i_count + 2;
	    }
	}

	if(j_set < 2)
		param_error = True;
	
	if ((param_error == True) || (j_set < 2)) {
		return ERR_USAGE;
	}

	if (j_set < 3) {	/* determine filename to store slice info */
		char *tmp_buf = tempnam(NULL, "slice");
		strncpy(filename, tmp_buf, strlen(tmp_buf));
		free(tmp_buf);
	}

	if((devhandle = rmedia_device_open_medium_handle(rawdev)) == NULL)
		return ERR_DEVPATH;
	if((medinfo = rmedia_device_get_medium_property(devhandle)) == NULL)
		return ERR_BADDEV;
	bs = rmedia_medium_get_block_size(medinfo);
	rmedia_device_close_medium_handle(devhandle);

	/* brianc Feb 01 2000, added a check for media type, if tho
	   media is not writable, then you wont be able to slice it
	*/
	media_type = rmedia_medium_get_media_type(medinfo);
	switch(media_type) {
		case RM_DK_MO_ERASABLE:
		case RM_DK_MO_WRITEONCE:
		case RM_DK_AS_MO:
		case RM_DK_CDROM:
		case RM_DK_CDR:
		case RM_DK_DVDROM:
		case RM_DK_DVDR:
		/*	return ERR_SLICE;*/
		default:
			break;
	}

	/* end fix */
	
	/* Bugid# 4405351
	 * The smedia libraries are returning capacities that are
	 * inconsistent with the values returned by directly passing
	 * ioctls to the device. The fix is to calculate the capacity
	 * ourselves (ncylinders*nheads*nsectors*block_size). This also
	 * means that we don't have to use native system calls directly,
	 * which was the point of the rmedia wrapper library in the 1st place
	 */
	/* capacity = bs * rmedia_medium_get_capacity(medinfo); */

	nc = rmedia_medium_get_cylinders(medinfo);
	nh = rmedia_medium_get_heads(medinfo);
	ns = rmedia_medium_get_sectors(medinfo);
	capacity = nc*nh*ns*bs;

	/* If capacity is less than or equal to zero,
	   there's no point going on
	*/
	if( (capacity == 0) || (capacity < 0) )
	{
	    return ERR_BADDEV;
	}

	first_free = 0;
	size_free = capacity;
	

	if ((fp1 = fopen(filename, "rw")) == NULL) {
		if ((fp1 = fopen(filename, "w")) == NULL) {
			return ERR_FILEOPEN;
		}
	}
	else {
		readwrite = 1;

		/* read any information in slice file */

		fgets(ast, 256, fp1);
		nslice = 0;

		while (!feof(fp1)) {

			if ((ast[0] == '#') && (fst_line == 0)) {
				/* see if the file contains filesys info */

				fs = get_file_sys(ast);
				fst_line++;
			}

			if ((ast[0] != '#') && (ast[0] != '\n')) {
				/* this line contains info, parse thru it */

				x = parse_slice(ast);
				nslice++;
			}

			if (x != 0) {
				/*
				 * an error occured during parsing, notify
				 * user and exit the program
				 */
				
				return ERR_PARSE;

			}
			else {
				fgets(ast, 256, fp1);
			}
		}
	}

	/* add any filesys info into the data structures */
	curr = start;
	tp = fs;
	while ((tp != NULL) && (curr != NULL)) {
		strcpy(curr->slice_filesys, tp->filesys_name);
		curr = curr->nextr;
		tp = tp->nxt;
		free(fs);
		fs = tp;
	}


	/*
	 * check the info read in from the slice file and remove any gaps
	 * present
	 */
	 
	 if(start == NULL)
	 {
	 	available[BACKUP_SLICE] = NOTFREE;
	 }
	 


	remove_gaps();
	fclose(fp1);
	return ERR_OK;

}


/**************************************************

create a new instance of the data structure for storing the slice information

***************************************************/

slice_ptr slice_create()
{
	slice_ptr s;

	s = (slice_ptr) malloc(sizeof(slice_rec));

	s->slice_num = -1;
	s->slice_start = -1;
	s->size = -1;
	sprintf(s->slice_tag, "-");
	sprintf(s->slice_flag, "-");
	sprintf(s->slice_filesys, "-");

	return s;
}

/*********************************

Delete a slice

***********************************/

void slice_delete(slice_ptr s)
{
	free(s);
}

/*********************************************

Take a line read in from the slice file and tokenise it
tokens are separated by
	SPACE		' '
	COLON		':'
	COMMA		','
	EQUALS SIGN	'='
	TAB		'\t'
	QUOTE		'\"'
	BACKSLASH	'\\'
	NEWLINE		'\n'

***********************************************/
int parse_slice(char *line)
{
	char *tokn;
	int pos, i, n;


	if (start == NULL) {
		start = slice_create();
		curr = start;
		curr->nextr = NULL;
	}
	else {
		curr->nextr = slice_create();
		curr = curr->nextr;
		curr->nextr = NULL;

	}


	pos = 0;
	n = 0;
	tokn = strtok(line, " :,=\t\"\\\n");
	while (tokn != NULL) {
		if (strcmp(tokn, "slices") != 0) {

			if (pos < 3) {
				for (i = 0; tokn[i] >= '0' && tokn[i] <= '9'; i++)
					n = (10 * n) + (tokn[i] - '0');

			}

			switch (pos) {
			case 0:
				curr->slice_num = n;
				break;
			case 1:
				curr->slice_start = n;
				break;
			case 2:
				curr->size = n;
				break;
			case 3:
				if (strlen(tokn) == 0)
					strcpy(curr->slice_flag, "-");
				else
					strcpy(curr->slice_flag, tokn);
				break;
			case 4:
				if (strlen(tokn) == 0)
					strcpy(curr->slice_tag, "-");
				else
					strcpy(curr->slice_tag, tokn);
				break;
			case 5:
				if (strlen(tokn) == 0)
					strcpy(curr->slice_filesys, "-");
				else
					strcpy(curr->slice_filesys, tokn);
				break;
			default:
				return (1);
			}



			pos++;
		}

		tokn = strtok(NULL, " :,=\"\t\\\n");
		n = 0;
	}

	available[curr->slice_num] = 1;
	return (0);


}


/******************************************************

Make sure that the slice information does not contain any
gaps, or overlaps.

*****************************************************/
void remove_gaps()
{

	if (start == NULL)
		return;

	curr = start;

	while (curr) {
		if (strcmp(curr->slice_tag, "backup") == 0)
			curr = curr->nextr;
		else {
			curr->slice_start = first_free;
			if(curr->slice_start == 0)
				strcpy(curr->slice_filesys, "ufs");
			first_free = first_free + curr->size;
			size_free = capacity - first_free;
			curr = curr->nextr;
		}


	}


}

/*********************************************************

This parses thru the first comment line in a slice file
to see if it contains any info on file systems. Returns a
pointer to the start of a list containing the filesys info

************************************************************/

sys_ptr get_file_sys(char sys[256])
{
	sys_ptr s, c;
	char *tokn;
	int isbackup; /* 0 if no backup slice */
	
	isbackup = 0;


	s = (sys_ptr) malloc(sizeof(sys_rec));
	s->nxt = NULL;
	c = s;


	tokn = strtok(sys, "# :,=\t\"\\\n");
	while (tokn != NULL) {
		strcpy(c->filesys_name, tokn);
		tokn = strtok(NULL, " :,=\"\t\\\n");
		if (tokn != NULL) {
			c->nxt = (sys_ptr) malloc(sizeof(sys_rec));
			c = c->nxt;
			c->nxt = NULL;
		}
	}

	c = s;
	while (c != NULL) {
		if(strcmp(c->filesys_name, "backup"))
			isbackup = 1;
		c = c->nxt;
	}
	
	if(isbackup == 0)
		available[2] = 1;
	


	return (s);

}



/************************************************

Free's up space used to setup listbox display

**************************************************/
void parse_delete_elements(char **elements)
{
	int i = 0;

	while (elements[i]) {
		free(elements[i]);
		i++;
	}

	free(elements);
}

int convert_flag()
{


	if (strcmp(unused->slice_flag, "wm") == 0)
		return 7;
	if (strcmp(unused->slice_flag, "rm") == 0)
		return 5;
	if (strcmp(unused->slice_flag, "rw") == 0)
		return 3;
	if (strcmp(unused->slice_flag, "wm") == 0)
		return 4;
	if (strcmp(unused->slice_flag, "wu") == 0)
		return 2;
	if (strcmp(unused->slice_flag, "ru") == 0)
		return 1;
	
	return 0;
}

/************************************************

Setup the elements used to determine lisbox display

**************************************************/
void parse_create_elements(slice_ptr slice, char ***elements)
{
	char s[64];
	char **_elements;
	int i;
	_elements = (char **) malloc(sizeof(char *) * (MAX_SLICE_FIELDS + 1));


	i = 0;
	sprintf(s, "%d", slice->slice_num);
	_elements[i] = strdup(s);
	i++;

	sprintf(s, "%.0f", slice->slice_start);
	_elements[i] = strdup(s);
	i++;

	sprintf(s, "%.0f", slice->size);
	_elements[i] = strdup(s);
	i++;

	sprintf(s, "%s", slice->slice_flag);
	_elements[i] = strdup(s);
	i++;

	sprintf(s, "%s", slice->slice_tag);
	_elements[i] = strdup(s);
	i++;

	sprintf(s, "%s", slice->slice_filesys);
	_elements[i] = strdup(s);
	i++;

	_elements[i] = 0;


	*elements = _elements;
}


/*********************************************************

This function eliminates gaps and overlaps

*********************************************************/

void redefine_slices()
{

   double in[MAX_SLICES][2], out[MAX_SLICES][2];
   int i, j,lowest, isbackup;
   slice_ptr prev = NULL;
   
   first_free = 0;
   size_free = capacity;
   isbackup = 0;
   
   /* The in array represents the current start and size of each slice
      The out array represents the same value after we've removed gaps and overlaps
   */

   for(i = 0; i < MAX_SLICES; i++)
   {
   	in[i][0] = -1; in[i][1] = -1;
   	out[i][0] = -1; out[i][1] = -1;
   }
   curr = start;
   i = 0;
   
   
   while(curr != NULL)   /* setup the in array values */
   {
   	in[i][0] = curr->slice_start;
   	in[i][1] = curr->size;
   	if(strcmp(curr->slice_tag, "backup") == 0)
   		isbackup = i;
   	curr = curr->nextr;
   	i++;
   }


   /* if there is a backup slice, write its values to the 
      out array
   */
   if(isbackup > 0) 
   {
   	out[isbackup][0] = in[isbackup][0];
   	out[isbackup][1] = in[isbackup][1];
   	in[isbackup][0] = -1;
   	in[isbackup][1] = -1;
   }


   /*	while there are still entries in the in array
   		find the slice with the lowest start sector.
   		write it to the out array, using the first available
   		start popsition as it start. Make sure that the start plus size 
   		dont over run the media size (force this if necessary)
   	
   		remove this slice from the in array.
   	
   */

   for(i = 0; i < nslice; i++)
   {
	lowest = -1;
   	for(j = 0; j < nslice; j++)
   	{
   		if( (lowest == -1) && (in[j][0] > -1) )
   			lowest = j;
   		else if( (lowest > in[j][0]) && (in[j][0] > -1) )
   			lowest = j;
   			 	
   	}
   
   	if( (lowest > -1) && (first_free < capacity) )
   	{
   		out[lowest][0] = first_free;
   		if( (first_free + in[lowest][1]) < capacity)
   			out[lowest][1] = in[lowest][1];
   		else
   			out[lowest][1] = (capacity - first_free);
   			
   		in[lowest][0] = -1; in[lowest][1] = -1;
   		first_free = first_free + out[lowest][1];
   	}
   		
   			
   		
   }
   
   

   /* it is possible that some slices needed to be removed. This can occur
      if the slices were created with prevent overlap switched OFF. Scan
      thru the list removing any slices whose out array value is -1.
   */
   
   curr = start;
   i = 0;
   while(curr != NULL)
   {
   	if(out[i][0] == -1)
   	{
   		if(curr == start)
   		{
   			start = curr->nextr;
   			free(curr);
   			curr = start;
   			
   		}else {
   			prev->nextr = curr->nextr;
   			free(curr);
   			curr = prev->nextr;
   		}
   		nslice--;
   	}else {
   	
   		curr->slice_start = out[i][0];
   		curr->size = out[i][1];
   		prev = curr;
   		curr = curr->nextr;
   	
   	}	
   	i++;
   
   }
   
   curr = start;


    update_list_display();
    selected = 1;
    
    if (nslice == 0)
    {
	/* unmanage the delete button */
    }


        
        

}

/***************************************************

Check the list for overlaps, and return the number of overlap
found, 0 if none found.

***************************************************/

int check_overlap()
{
   slice_ptr s;
   double x, y;
   int overlap;

   curr = start;
   overlap = 0;
   
   /* for each slice in the list
   	check that none of the slices that follow it 
   		start between its start and end points
   		or
   		end between its start and end points.
   */
   
   while(curr != NULL)
   {
   	s = curr->nextr;
   	x = curr->slice_start + curr->size;
   	while(s != NULL)
   	{
   		y = s->slice_start + s->size;
   		if( (s->slice_start > curr->slice_start) && (s->slice_start < x) )
   			overlap++;
   		if( (y > curr->slice_start) && ( y < x) )
   			overlap++;
   			
   		s = s->nextr;
   	
   	}
   	
   	curr = curr->nextr;
   
   
   }
   
   return(overlap);

}

int write_slice(FILE *fp, slice_rec *sliceDef)
{
	fprintf(fp, " %s ", curr->slice_filesys);
	return 0;
}

int write_slices(FILE *fp, slice_rec *sliceDefList)
{
	fprintf(fp, "#");
	curr = start;
	while(curr != NULL) {
		write_slice(fp, curr);
		curr = curr->nextr;
	}
	fprintf(fp, "\n");
	fprintf(fp, "#\n");
	return 0;
} 
