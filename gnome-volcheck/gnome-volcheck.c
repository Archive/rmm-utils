/*
 * Copyright (C) 2002 Sun Microsyatems Inc.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Cambridge, MA 02139, USA.
 *
 */

#include	<stdio.h>
#include 	<unistd.h>
#include	<sys/types.h>
#include	<sys/stat.h>
#include	<sys/types.h>
#include	<limits.h>
#include	<strings.h>
#include	<dirent.h>
#include	<stdlib.h>
#include	<sys/fcntl.h>
#include	<time.h>
#include	<errno.h>
#include	<glib.h>
#include	<wait.h>
#include	<fcntl.h>
#include        <syslog.h>

#define PACKAGE_BIN_DIR BINDIR

#define NOTIFY_DIR "/tmp/.removable"
#define NOTIFY_NAME "notify"
#define readBufferSize	256	
#define ALLRW (S_IRUSR | S_IWUSR | S_IRGRP | S_IWGRP | S_IROTH | S_IWOTH)
#define MAX_MEDIA  15

static time_t lastCheck = 0;
static gboolean is_sliced_cd = FALSE;
static gboolean is_sliced_zip = FALSE;

/* Default values. If user provides values to these arguments through 
 * commandline, then use them. Otherwise, use these default values only.
 */
char *specs[MAX_MEDIA] = {"cdrom", "floppy", "zip", "jaz", "dvdrom", "rmdisk", NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL};
int sleep_time = 3;
int initial_st = 30;
 
static void 	cleanup_notify_pipe (void);
static void 	monitor_notify_pipe (void);
static void 	process_file (char* volumeFile, size_t fileSize);
static char**   get_files (char** specs);
static void 	free_names (char** names);
static void 	open_media (void);
static void 	exec_in_background (char *command);

void monitor_notify_pipe ()
{
	char pipename[256];
	struct stat file_status;
	char readBuffer[readBufferSize];
	int notifyFd, num_bytes, i;
	gboolean insert_happend = FALSE;

	/* Got message on the pipe, what to do?
	 * 1. If insertion,
	 *    Read latest (how?) notify file in NOTIFY_DIR
	 *    How do one handle audio?
	 *        looks for audio_only in notification file
	 *        if found start gnome-cd
	 *    How do handle unformated media ?
	 *        look for unformatted_media in notification file
	 *        if found start gmedia_format
	 *    How do handle protected media?
	 *        look for password_protected in notification file
	 *        if found start gmedia_prot
	 *    if none of the above start up nautilus
	 *    [ nautilus handle multiple instances of its own process anyway ]
	 *
	 * 2. If eject, do we need to handle anything?
	 *    Not really. We ignore eject, unmount and remount.
	 *
	 */ 

	(void)snprintf (pipename, 255, "%s/%s%d", NOTIFY_DIR, NOTIFY_NAME, getpid());  
	lastCheck = time (NULL);
	memset (readBuffer, 0, sizeof (readBuffer));
	num_bytes = 1;
	while(1) {
		if (stat (pipename, &file_status) != 0) {
			mode_t umsk = umask(0);
			mode_t mode = ALLRW & (~umsk);

			/* Check if NOTIFY_DIR exist first. If it is not there,
			 * then create it and change the mode to 0777
			 */
			if (stat (NOTIFY_DIR, &file_status) != 0) {
				if (errno != ENOENT) { 
					openlog ("GNOME-VOLCHECK", LOG_PID, LOG_USER);
					syslog (LOG_ERR, "Error stating on the directory '%s' directory: %s\n", NOTIFY_DIR, strerror (errno));
					closelog ();
					exit (2);
				}	
				if (mkdir (NOTIFY_DIR, 0777) < 0) {
					openlog ("GNOME-VOLCHECK", LOG_PID, LOG_USER);
					syslog (LOG_ERR, "Failed to create '%s' directory: %s\n", NOTIFY_DIR, strerror (errno));
					closelog ();
					exit (2);
				}
				else {
					chmod (NOTIFY_DIR, 0777);
				}
			}

			if (mkfifo (pipename, mode) == -1 ) {
				openlog ("GNOME-VOLCHECK", LOG_PID, LOG_USER);
				syslog (LOG_ERR, "Failed to create notify pipe '%s'", pipename);
				closelog ();
				exit (3);
			}
		}
		lastCheck = time (NULL);
		insert_happend = FALSE;
		if ((notifyFd = open (pipename, O_RDONLY )) >= 0) {
			/* Read first from the pipe. If there is an 'i', then
			 * sleep for 'sleep_time' and then read once again 
			 * to clean-up the pipe. This should ideally be done 
			 * for a multi-sliced media only to avoid stacking of 
			 * events in the pipe, which is causing some problems. 
			 * But we do sleep for all media as we can't 
			 * distinguish between media at this stage. We need 
			 * not sleep for 'ejects/unmounts/remounts' because 
			 * they get read from pipe without stacking up.
			 */
 
			num_bytes = read (notifyFd, (void *)readBuffer, readBufferSize);
			for (i = 0; i < num_bytes; i++) {
				if (readBuffer[i] == 'i') {
					insert_happend = TRUE;
					break;
				}
			}
			if (insert_happend) {
				sleep (sleep_time);
				num_bytes = read (notifyFd, (void *)readBuffer, readBufferSize);
				(void) open_media ();	
			}
			memset (readBuffer, 0, sizeof (readBuffer));
			close (notifyFd);
		}
	}
}

void cleanup_notify_pipe ()
{
	int len;
	DIR *dirp;
	struct dirent *dir_entryp;
	char namebuf[256];

	len = strlen (NOTIFY_NAME);
	dirp = opendir (NOTIFY_DIR);
	if (dirp == NULL)
		return;
	while (dir_entryp = readdir (dirp)) {
		if (strncmp (dir_entryp->d_name, NOTIFY_NAME, len) != 0)
			continue;
		(void) sprintf (namebuf, "%s/%s", NOTIFY_DIR, dir_entryp->d_name);
		/* remove existing named pipe */
		remove (namebuf);
	}
}

/* Return true if the VolumeFile represents slice-2(cdrom-s2 or zip-s2 etc.)
 * for a multi-partitioned media
 */
static gboolean isslice2 (char *volumeFile)
{
	char *slice;
	if (slice = g_strrstr (volumeFile,"-s2")) {
		if (slice[strlen ("-s2")] == '\0') {
			return TRUE;
		}
	}
	return FALSE;
}

/* Return TRUE if the volumeFile represents a partioned CDROM or ZIP. For
 * non-partioned CD or ZIP, it returns FALSE.
 */
static gboolean media_is_partitioned (gchar *volumeFile)
{
	gchar *charp;

	if (charp = g_strrstr (volumeFile, "cdrom")) {
		charp = charp + strlen ("cdrom");
	}
	else if (charp = g_strrstr (volumeFile, "zip")) {
		charp = charp + strlen ("zip");
	}
	else {
		/* For media other than CDROM or ZIP */
		return FALSE;
	}

	if (charp == NULL) {
		return FALSE;
	}
	else {
		if (g_ascii_isdigit(charp[0]) == FALSE) {
			return FALSE;
		}
		else {
			if (charp[1] == '\0')
				return FALSE; /*  *cdrom[0-9] or *zip[0-9] */
			if (g_ascii_isdigit(charp[1]) && (charp[1] == '\0'))
				return FALSE; /*  *cdrom[0-9][0-9] or *zip[0-9][0-9]*/
			if ((charp[1] == '-') && g_ascii_isdigit(charp[2]) && (charp[3] == '\0'))
				return FALSE; /*  *cdrom[0-9]-[0-9] or *zip[0-9]-[0-9] */
			if (g_ascii_isdigit(charp[1]) && (charp[2] == '-') && g_ascii_isdigit(charp[3]) && (charp[4] == '\0'))
				return FALSE; /*  *cdrom[0-9][0-9]-[0-9] or *zip[0-9][0-9]-[0-9] */
		}
	}
	return TRUE;
}


/* Return true if the VolumeFile represents slice-0(cdrom-s0 or zip-s0 etc.)
 * for a multi-partitioned media
 */
static gboolean is_slice_zero (char *volumeFile)
{
	char *slice;
	if (slice = g_strrstr (volumeFile,"-s0")) {
		if (slice[strlen ("-s0")] == '\0') {
			return TRUE;
		}
	}
	return FALSE;
}

void open_media (void)
{
	char **volumeFiles = NULL, **names;
	int i = 0;    

	volumeFiles = get_files (specs);
	if (volumeFiles == NULL ) {
		return;
	}

	names = volumeFiles;

	while (names[i] != NULL) {
		char *volumeFile = names[i];
		struct stat file_status;
		char *tempFile = NULL;
		gboolean found_slice_zero = FALSE;

		i++;

		/* Check that the file exists and it is a regilar file */
		if (stat (volumeFile, &file_status) != 0) {
			continue;
		}
		if (!S_ISREG (file_status.st_mode)) {
			continue;
		}

		/* Checking time of last modification; is this correct? */
		if (file_status.st_ctim.tv_sec <= lastCheck) {
			continue;
		}

		/* Check if the mounted media is partitioned CDROM/ZIP.
		 * If it is partitioned, then show only one nautilus 
		 * window for all the slices, i.e one window with the 
		 * contents of all slices. User can navigate into the 
		 * contents of each of the slices from this window.
		 *
		 * Assumption is, if it is a CDROM/ZIP, and if it is sliced, 
		 * it should always have slice cdrom[0-9]-s2/zip[0-9]-s2.
		 */

		if (media_is_partitioned (volumeFile)) {
			if (g_strrstr (volumeFile, "cdrom")) {
				/* For sliced CDROM, process cdrom[0-9]-s2
				 * to show nautilus contents and ignore others
				 */
				if (isslice2 (volumeFile)) {
					is_sliced_cd = TRUE;
				}
				else {
					continue;
				}
			}
			if (g_strrstr (volumeFile, "zip")) {
				/* For sliced ZIP, make sure that there is 
				 * a zip[0-9]-s2 and then get volumeFile for
				 * slice zero and process it. This is because 
				 * slice2(backup) volumeFile will not give 
				 * mount  point.
				 */
				if (is_slice_zero (volumeFile)) {
					tempFile = volumeFile;
					found_slice_zero = TRUE;
				}	
				else if (isslice2 (volumeFile)) {
					is_sliced_zip = TRUE;
				}

				/* Till we find both slice0 and slice2 */
				if (is_sliced_zip && found_slice_zero) {
					volumeFile = tempFile;
				}
				else {
					continue;	
				}
			}
		}
		process_file (volumeFile, file_status.st_size );

		if (is_sliced_cd) {
			is_sliced_cd = FALSE;
		}
		if (is_sliced_zip == TRUE) {
			is_sliced_zip = FALSE;
		}
	}
	free_names (volumeFiles);
}

static void process_file (char* volumeFile, size_t fileSize)
{
	int fd;
	int retVal;
	char mountPoint[256];
	char *quoted_mountPoint;
	char devicePath[256]; 
	char *quoted_devicePath;
	int protected = 0;
	int audio_only = 0;
	int backup_slice = 0;
	int unformatted_media = 0;
	int unknown_media_type = 0;
	int uninitialized = 0;
	char * buffer;
	char cmd[1024];
	char command[1024];
	FILE *fp;
	uid_t uid;
	int status = FALSE;
	pid_t pid;
	GError *Error = NULL;

	fd = open (volumeFile, O_RDONLY);

	if (fd == -1)
		return;

	buffer = (char *)malloc (fileSize+1);
	if (read (fd, buffer, fileSize) != fileSize) {
		close (fd);
		free (buffer);
		return;
	}
	buffer[fileSize] = '\0';

	/* Read mount point and device path from the volume file. Ignome file
	 * system name.
	 */
	if (sscanf (buffer, "%s %s", mountPoint, devicePath) != 2) {
		free (buffer);
		return;
	}

	if (!g_file_test (devicePath, G_FILE_TEST_EXISTS)) {
		openlog ("GNOME-VOLCHECK", LOG_PID, LOG_USER);		
		syslog (LOG_ERR, "Device path %s does not exist.", devicePath);
		closelog ();
		return;
	}
	if (mountPoint[0] != '/') {
		quoted_devicePath = g_shell_quote (devicePath);
		if (strcmp(mountPoint, "password_protected") == 0) {
			protected = 1;
			snprintf (command, 1024, "%s/gmedia_prot -d %s", SERVERDIR, quoted_devicePath);
			if (!g_spawn_command_line_async (command, &Error)) {
				openlog ("GNOME-VOLCHECK", LOG_PID, LOG_USER);
				syslog (LOG_ERR, "Error spawning the command '%s': %s", command, Error->message);
				closelog ();
				g_error_free (Error);
			}

			/* mountPoint = "/floppy/password_protected"; */

		} else if (strcmp (mountPoint, "backup_slice") == 0) {
			backup_slice = 1;

		} else if (strcmp (mountPoint, "unformatted_media") == 0) {
			unformatted_media = 1;

			snprintf (command, 1024, "%s/gmedia_format -d %s", SERVERDIR, quoted_devicePath);
			if (!g_spawn_command_line_async (command, &Error)) {
				openlog ("GNOME-VOLCHECK", LOG_PID, LOG_USER);
				syslog (LOG_ERR, "Error spawning the command '%s': %s", command, Error->message);
				closelog ();
				g_error_free (Error);
			}

		} else if (strcmp (mountPoint, "audio_only") == 0) {
			audio_only = 1;

			/* Get the real user id */
			uid = getuid ();
			sprintf (cmd, "/usr/bin/pgrep -u %ld gnome-cd", uid);

			fp = popen (cmd, "r");
			if (fp) {
                         	if (fgets (cmd, sizeof (cmd), fp) != NULL)
                                        if (cmd[0] != '\n')
                                                status = TRUE;
                                pclose (fp);
                        }

			if (status == FALSE) {
				snprintf (command, 1024, "%s/gnome-cd", PACKAGE_BIN_DIR);
				if (!g_spawn_command_line_async (command, &Error)) {
					openlog ("GNOME-VOLCHECK", LOG_PID, LOG_USER);
					syslog (LOG_ERR, "Error spawning the command '%s': %s", command, Error->message);
					closelog ();
					g_error_free (Error);
				}
			}
		} else {
			unknown_media_type = 1;
		}
		g_free (quoted_devicePath);
	}
	else {
		quoted_mountPoint = g_shell_quote (mountPoint);

		if (is_sliced_cd || is_sliced_zip ) {
			gchar *tempmount = NULL;
			int i;
			g_free (quoted_mountPoint);
			if (is_sliced_zip) {
				tempmount = g_strrstr (mountPoint, "/s0");
			}
			if (is_sliced_cd) {
				tempmount = g_strrstr (mountPoint, "/s2");
			}
			if (tempmount != NULL) {
					tempmount[0] = '\0';
			}
			quoted_mountPoint = g_shell_quote (mountPoint);
		}

		snprintf(command, 1024, "%s/nautilus --no-desktop %s", PACKAGE_BIN_DIR, quoted_mountPoint);

		if (!g_spawn_command_line_async (command, &Error)) {
			openlog ("GNOME-VOLCHECK", LOG_PID, LOG_USER);
			syslog (LOG_ERR, "Error spawning the command '%s': %s", command, Error->message);
			closelog ();
			g_error_free (Error);
		}
		g_free (quoted_mountPoint);
	}
}

static char** get_files (char** specs)
{ 
	/*
	 * This function checks for the existence in /tmp/.removable
	 * of files which start with a name in specs
	 *
	 * If such files exist an array of their names is returned.
	 * If no such files exist NULL is returned.
	 */
	char** retVal = NULL;
	DIR *dp;
	struct dirent *dirp;
	int i;
	int numFiles = 0;
	int dirLength = strlen (NOTIFY_DIR);

	if ((dp = opendir (NOTIFY_DIR)) == NULL)
		return NULL;

	while ((dirp = readdir (dp)) != NULL) {
		if (strcmp (dirp->d_name, ".") == 0 ||
		    strcmp (dirp->d_name, "..") == 0 ||
		    strncmp (dirp->d_name, NOTIFY_NAME, strlen(NOTIFY_NAME)) == 0 ) 
		/* ignore dot and dot-dot */
			continue;

		/*
		 * Check that we want this file
		 */
		i = 0;	
		while(specs[i]) {
			if (strstr (dirp->d_name, specs[i])) {
				/* We need this file */
				retVal = realloc (retVal, (numFiles + 1) * 
					sizeof(char*));
				retVal[numFiles] = (char *) malloc (dirLength +
					2 + strlen(dirp->d_name));
				strcpy (retVal[numFiles], NOTIFY_DIR);
				strcat (retVal[numFiles], "/");
				strcat (retVal[numFiles++], dirp->d_name);
				break;
			}
			i++;	
		} 
	}
	closedir (dp);
	if (retVal != NULL) {
		retVal = realloc (retVal, (numFiles + 1) * sizeof(char*));
		retVal[numFiles] = NULL;
	}
	return retVal;
}

static void free_names(char** names)
{
	char** pnames = names;

	if (pnames == NULL)
		return;

	while (*pnames) {
		free (*pnames);
		pnames++;
	}
        /* free (names); */

	return;
}

static void
process_media_specs (char *media_str) 
{
	gint len, spec_num;
	gchar *pos;
	gchar *temp = NULL;

	spec_num = 0;

	while ((pos = strchr (media_str, ',')) != NULL && spec_num < MAX_MEDIA) {
		len = pos - media_str;
		if (len == 0) {
			media_str = media_str + len + 1;
			continue;
		}
		temp = (char *) malloc (len + 1);
		strncpy (temp, media_str, len);
		temp[len] = '\0';
		specs[spec_num] = temp;

		spec_num++;
		media_str = media_str + len + 1;
	}
	if ((media_str[0] != '\0') && (strchr (media_str, ',') == NULL) && spec_num < MAX_MEDIA) {
		len = strlen (media_str);
		temp = (char *) malloc (len + 1);
		strncpy (temp, media_str, len);
		temp[len] = '\0';
		specs[spec_num] = temp;
		spec_num++;
	}

	while (specs != NULL && spec_num < MAX_MEDIA) {
		specs[spec_num++] = NULL;
	}
}

static void 
usage (void) 
{
	g_print ("gnome-volcheck: check for media and open the media found\n");
	g_print ("Usage: gnome-volcheck [-i nsec] [-z nsec] [-m media_spec]\n");
	g_print ("-i nsec     sleep 'nsec' seconds initially to allow nautilus to come up\n");
	g_print ("-z nsec     sleep 'nsec' seconds before opening the media\n");
	g_print ("-m media to monitor: floppy,cdrom,zip,jaz etc.\n");
}

void main (int argc, char **argv)
{
	gchar *media_str;
	gint c;

	/* Open NOTIFY_DIR and look for any notify file. Remove any of 
	 * the existing notify pipes. How do do this if the pipe is owned 
	 * by someone else??. I guess this is not critical as action_filemgr 
	 * write to every notify$$ file anyway.
	 */

	while ((c = getopt (argc, argv, "z:i:m:h?")) != -1) {
		switch (c) {
			case 'h':
			case '?':
				usage ();
				exit (3);
			case 'z':
				/* Make sure that sleep_time is >0 */
				if (atoi (optarg) >= 0) {
					sleep_time = atoi (optarg);
				}
				break;
			case 'i':
				initial_st = atoi (optarg);
				break;
			case 'm': 
				media_str = g_strdup (optarg); 
				process_media_specs (media_str);
				g_free (media_str);
				break;
			default:
				usage ();	
				exit (3);
		}
	}

	/* Initial sleep to allow nautilus to start up first */
	sleep (initial_st);
	(void) cleanup_notify_pipe ();
	(void) monitor_notify_pipe ();
}
