/*
 * Copyright 2002 Sun Microsystems Inc.
 *
 *  This Library is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This Library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this Library; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Street #330, Boston, MA 02111-1307, USA.
 *
 */

struct dev_info {
        char *mount_path;
        char *device_path;
        char *file_system;
};

typedef enum {
        DEVICE_UNKNOWN,
        DEVICE_AUDIO_CD,
        DEVICE_CAMERA,
        DEVICE_CDROM_DRIVE,
        DEVICE_FLOPPY_DRIVE,
        DEVICE_JAZ_DRIVE,
        DEVICE_MEMORY_STICK,
        DEVICE_NFS,
        DEVICE_ZIP_DRIVE,
        DEVICE_SMB,
        DEVICE_APPLE,
        DEVICE_WINDOWS,
} DeviceType;

/* returns the device type or -1 for the given device path*/ 
int get_device_type (char *path);

/* returns the device protection status or -1 for the given device path*/ 
int get_device_prot (char *path);

/* returns the device information like device path, 
 * mount_path and file system or -1 for the given device path
 */ 
struct dev_info *get_dev_info (FILE *ptr);
