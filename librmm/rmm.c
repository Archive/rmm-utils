/*
 * Copyright 2002 Sun Microsystems Inc.
 *
 *  This Library is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This Library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this Library; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Street #330, Boston, MA 02111-1307, USA.
 *
 */

#include <stdio.h>
#include <strings.h>
#include "rmm.h"
#include "SDtRmedia.h"

int get_device_type (char *path)
{
	MediumHandle medium_handle;
	MediumInfoHandle medium_info;
	int dev_type;

	medium_handle = rmedia_device_open_medium_handle (path);

	if (medium_handle == NULL)
		return -1;

	medium_info = rmedia_device_get_medium_property (medium_handle);
	rmedia_device_close_medium_handle (medium_handle);

	if (medium_info == NULL)
        	return -1;
	switch (rmedia_medium_get_media_type (medium_info)) {
		case  RM_DK_CDROM:
	 		dev_type = DEVICE_CDROM_DRIVE;
			break;
		case  RM_DK_DVDROM:
	 		dev_type = DEVICE_CDROM_DRIVE;
			break;
		case  RM_DK_FLOPPY:
			dev_type = DEVICE_FLOPPY_DRIVE;
			break;
		case  RM_DK_ZIP:
	 		dev_type = DEVICE_ZIP_DRIVE;
			break;
		case  RM_DK_JAZ:
	 		dev_type = DEVICE_JAZ_DRIVE;
			break;
		default:
			dev_type = DEVICE_UNKNOWN;
	}
	return dev_type;
}
	
int get_device_prot (char *path)
{
	int dev_prot; 	
	MediumHandle medium_handle;
	medium_handle = rmedia_device_open_medium_handle (path);

	if (medium_handle == NULL)
		return -1;
	dev_prot = rmedia_device_get_protection_status (medium_handle);
	rmedia_device_close_medium_handle (medium_handle);

	return (dev_prot);
}

struct dev_info *get_dev_info (FILE *ptr)
{
	struct dev_info *info;
	char string[256], mnt_path[256], dev_path[256], fs_type[256];
 	int i;

	fgets (string, 256, ptr);	
	info = (struct dev_info *) malloc (sizeof (struct dev_info));
	if (string != NULL) {
		strcpy (mnt_path, strtok (string, " "));
		strcpy (dev_path, strtok (NULL, " "));
		if (mnt_path[0] == '/') {
			strcpy (fs_type, strtok (NULL, " "));
		} else {
			strcpy (fs_type, mnt_path);
		}

		info->device_path = (char *) malloc (strlen (dev_path) + 1);
		if (info->device_path == NULL) {
			return NULL;
		}	
		strcpy (info->device_path, dev_path);

		if (mnt_path[0] == '/') { 
			info->mount_path = (char *) malloc (strlen (mnt_path) + 1);
			if (info->mount_path == NULL) {
				return NULL;
			}	
			strcpy (info->mount_path, mnt_path);
		} else {
			info->mount_path = (char *) malloc (strlen (mnt_path) + 2);
			if (info->mount_path == NULL) {
				return NULL;
			}	
			sprintf (info->mount_path, "/%s", mnt_path);
		}

		info->file_system = (char *)malloc(strlen(fs_type));
		if (info->file_system == NULL) {
			return NULL;
		}
		strcpy (info->file_system, fs_type);
	}
	else {
		return NULL;
	}
	return info;
}

